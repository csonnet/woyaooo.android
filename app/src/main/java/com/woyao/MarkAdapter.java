package com.woyao;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.MarkSummary;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class MarkAdapter extends RecyclerView.Adapter<MarkAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private  List<MarkSummary> thedata = new ArrayList<MarkSummary>();
    private Changed onChanged;

    Context thecontext;

    public MarkAdapter(Context context, List<MarkSummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata= data;

    }


    public interface Changed{
        void view(MarkSummary r);
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.chance_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MarkSummary curData =  thedata.get(position);

        holder.theTitle.setText( curData.getTitle() );


        holder.theOrder.setText(  (position +1) +"") ;

        if (curData.getAccess_right().equals("private")){
            holder.theTitle.setTextColor( ContextCompat.getColor(thecontext, R.color.colorSubTitleText));
        }else{
            holder.theTitle.setTextColor( ContextCompat.getColor(thecontext, R.color.colorTitleText));
        }

        holder.theMsg.setVisibility(View.GONE);
        if (curData.getNum() >0 ) {
            holder.theMsg.setVisibility(View.VISIBLE);
            holder.theMsg.setText(curData.getNum() + "");
        }else{
            holder.theMsg.setVisibility(View.INVISIBLE);
        }

        holder.theSubtitle.setText( curData.getDescription() );



        holder.theImage.setImageResource(R.drawable.no_avartar);
        if (StringUtil.notNullOrEmpty(curData.getSnailview())) {
//            ImageLoader.load(holder.theImage, curData.getSnailview());
            Picasso.with(thecontext)
                    .load(curData.getSnailview())
                    .into(holder.theImage);
        }

        holder.theContent.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                MarkSummary curdata = thedata.get(holder.getAdapterPosition());
                onChanged.view(curdata  );
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theTitle;
        public TextView theSubtitle;
        public LinearLayout theContent;
        public CircleImageView theImage;
        public TextView theOrder;
        public TextView theMsg;

        public ViewHolder(View view) {
            super(view);
            theTitle = (TextView) view.findViewById(R.id.chance_title);
            theSubtitle = (TextView) view.findViewById(R.id.chance_subtext);
            theContent = (LinearLayout) view.findViewById(R.id.chance_content);
            theImage = (CircleImageView) view.findViewById(R.id.chance_image);
            theOrder  = (TextView)  view.findViewById(R.id.chance_order);
            theMsg  = (TextView)  view.findViewById(R.id.chance_msg);

        }

    }

}
