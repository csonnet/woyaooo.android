package com.woyao;

import android.arch.lifecycle.ViewModel;

import com.woyao.core.model.User;

public class UserViewModel extends ViewModel {

    private User the_user = new User();

    public void initUser(User user) {
        the_user = user;
    }

    public User getUser() {
        return the_user;
    }
}
