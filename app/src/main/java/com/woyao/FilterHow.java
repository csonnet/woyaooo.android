package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.woyao.core.model.GetChildHowResponse;
import com.woyao.core.model.How;
import com.woyao.core.model.KeyValueList;
import com.woyao.core.service.HowService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

public class FilterHow extends AppCompatActivity {
    TextView howtitle = null;
    HashMap<String,String> parents = new HashMap<String,String>() ;

    private  String curhow = "";
    private  String from = "filter";

    private RecyclerView popList = null;
    LinearLayoutManager mLayoutManager =null;
    private NewFilterAapter mAdapter;

    LinearLayoutManager allLayoutManager = null;
    private RecyclerView alllist = null;
    private SingleTreeAdapter allAdapter;

    ProgressDialog progressDialog;
    Boolean changed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter_how);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //       ab.setHomeAsUpIndicator(R.drawable.icon_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
//        ab.setDisplayShowTitleEnabled(false); // disable the default title element

        popList = (RecyclerView) findViewById(R.id.recyclerHow);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        popList.setLayoutManager(mLayoutManager);

        alllist = (RecyclerView) findViewById(R.id.id_how_list);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        TextView roottitle = (TextView) findViewById(R.id.how_roottitle);

        roottitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curhow ="";
                howtitle.setText(  curhow );
                loadAllData();
            }
        });

        howtitle = (TextView) findViewById(R.id.alltitle);

        TextView gobackbtn = (TextView) findViewById(R.id.goback);

        gobackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (parents.get(curhow ) != null) {

                    curhow = parents.get(curhow ) ;
                    howtitle.setText(  curhow );
                    loadAllData();
                }
            }
        });

        Button nextbtn = (Button) findViewById(R.id.filter_how_finish);

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result",mAdapter.getSelectionKeyValue() );
                setResult(666, intent);
                finish();
            }
        });
        setTitle("请选择合作领域");

        Intent intent = getIntent();
        from = intent.getStringExtra("from");
        KeyValueList initHowList = (KeyValueList)intent.getExtras().get("how_list");
        String typeno = intent.getStringExtra("type");
        mAdapter = new NewFilterAapter(this, initHowList.getContent(),initHowList.getContent());

        mAdapter.setChangedHandler( new NewFilterAapter.Changed(){
            @Override
            public void dataChanged() {
                changed = true;
            }
        });

        popList.setAdapter(mAdapter);

        if (from.equals("demand")){
            loadTypeHow(typeno );
        }else {
            loadAllData();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        intent.putExtra("changed", changed );
        setResult(0, intent);
        finish();

        return true;
    }


    private void loadTypeHow(final String typeno ){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("拼命加载中······");
        progressDialog.show();

        AsyncTask<Void,Void,GetChildHowResponse> task =
                new AsyncTask<Void, Void, GetChildHowResponse>() {
                    @Override
                    protected GetChildHowResponse doInBackground(Void... params) {
                        HowService svc = ServiceFactory.get(HowService.class);
                        Call<GetChildHowResponse> responseCall = svc.getTypeHow(typeno,WoyaoooApplication.userId);
                        try {
                            GetChildHowResponse response = responseCall.execute().body();
                            return response;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                    @Override
                    protected void onPostExecute(final GetChildHowResponse response) {
                        progressDialog.dismiss();
                        ArrayList<How> hows = response.getHowList();
                        renderAll(hows);
                    }
                    @Override
                    protected void onCancelled() {
                        progressDialog.dismiss();
                    }
                };
        task.execute();
    }

    private void loadAllData(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("拼命加载中······");
        progressDialog.show();

        AsyncTask<Void,Void,GetChildHowResponse> task =
                new AsyncTask<Void, Void, GetChildHowResponse>() {
                    @Override
                    protected GetChildHowResponse doInBackground(Void... params) {
                        HowService svc = ServiceFactory.get(HowService.class);
                        Call<GetChildHowResponse> responseCall = svc.getChildHow(curhow,WoyaoooApplication.userId,from);
                        try {
                            GetChildHowResponse response = responseCall.execute().body();
                            return response;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                    @Override
                    protected void onPostExecute(final GetChildHowResponse response) {
                        progressDialog.dismiss();
                        ArrayList<How> hows = response.getHowList();
                        renderAll(hows);
                    }
                    @Override
                    protected void onCancelled() {
                        progressDialog.dismiss();
                    }
                };
        task.execute();
    }


    private void renderAll( ArrayList<How> thehow ){


        ArrayList<Map<String,String>> mapList = new ArrayList<>();

        for(How howww : thehow){
            HashMap<String,String> map = new HashMap<>();
            map.put("no", howww.getNo());
            map.put("name", howww.getName());
            map.put("description", howww.getDescription());
            map.put("image", howww.getImage());
            map.put("childs", howww.getChilds() +"");
            map.put("selectable", howww.getSelectable().toString());
            mapList.add(map);
        }
        allAdapter = new SingleTreeAdapter(this,  mapList );

        allAdapter.setChangedHandler(new SingleTreeAdapter.Changed() {
            @Override
            public void itemSelected(Map<String, String> one) {
                mAdapter.AddNewOne( one) ;
                mAdapter.notifyDataSetChanged();
                changed = true;
            }


            @Override
            public void itemGetChildren(Map<String, String> one) {

                parents.put(one.get("name"), curhow );
                curhow = one.get("name");
                howtitle.setText( curhow );

                loadAllData();
            }
        });


        this.alllist.setAdapter(allAdapter);



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}