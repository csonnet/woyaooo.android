package com.woyao;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.util.Common;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class SingleTreeAdapter extends RecyclerView.Adapter<SingleTreeAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private ArrayList<Map<String,String>> thedata = new ArrayList<Map<String,String>>();
    private Changed onChanged;
    Context thecontext;

    public SingleTreeAdapter(Context context, ArrayList<Map<String,String>> data  ) {

        this.mInflater = LayoutInflater.from(context);
        thecontext = context;
        this.thedata = data;

    }


    public void AddNewOne( Map<String,String> onedata ) {
        for (Map<String,String> one : thedata){
            if ( one.get("name")  == onedata.get("name") ){
                one.put("selected","true");
                return;
            }
        }
        thedata.add(onedata);
    }

    public interface Changed{
        void itemSelected(Map<String, String> one);
        void itemGetChildren(Map<String, String> one);
    }

    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    public String getSelection() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected").equals("true" )){
                it.add((String) one.get("no"));
            }
        }

        return  Common.listToString(it );
    }

    public String getSelectionName() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                it.add((String) one.get("name"));
            }
        }

        return  Common.listToString(it );
    }

    public int getSelectionCount(){
        int count = 0;
        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                count += 1;
            }
        }
        return count;
    }


    @Override
    public SingleTreeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.single_tree_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SingleTreeAdapter.ViewHolder holder, final int position) {


        holder.thetitle.setText( thedata.get(position).get("name") );

        holder.thedescription.setText( thedata.get(position).get("description") );

        if (thedata.get(position).containsKey("image") && StringUtil.notNullOrEmpty(thedata.get(position).get("image")) ) {
            holder.theimage.setVisibility(View.VISIBLE);
            holder.theimage.setImageResource(R.drawable.no_avartar);
                Picasso.with(thecontext)
                        .load(thedata.get(position).get("image"))
                        .into(holder.theimage);

        }else{
            holder.theimage.setVisibility(View.GONE);
        }

        if (thedata.get(position).get("childs").equals("0")  || thedata.get(position).get("childs").equals("1")  ) {
            holder.thenext.setVisibility(View.GONE);
        }else{
            holder.thenext.setVisibility(View.VISIBLE);
        }


        if (thedata.get(position).get("description") != null && thedata.get(position).get("description").length()==0 ) {
            holder.thedescription.setVisibility(View.GONE);
        }else{
            holder.thedescription.setVisibility(View.VISIBLE);
        }
        holder.thepanel.setBackgroundColor(Color.WHITE);

        holder.thepanel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    // 按下
                    case MotionEvent.ACTION_DOWN:
//                        view.setBackgroundColor(Color.LTGRAY);
                        break;
                    // 移动
                    case MotionEvent.ACTION_MOVE:

                        break;
                    // 拿起
                    case MotionEvent.ACTION_UP:

                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        holder.thepanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( ! thedata.get(position).get("childs").equals("0")   && ! thedata.get(position).get("childs").equals("1")  ) {
                    onChanged.itemGetChildren(  thedata.get(position) );
                }else{
                    HashMap<String,String> it = new HashMap<String, String>();

                    for(Map.Entry<String, String> entry:thedata.get(position).entrySet()){
                        it.put(entry.getKey(),entry.getValue());
                    }
                    it.put("selected","true");
                    onChanged.itemSelected(it );
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public TextView thedescription;
        public LinearLayout thepanel;
        public ImageView thenext;
        public ImageView theimage;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.id_text);
            thedescription = (TextView) view.findViewById(R.id.id_description);
            thenext = (ImageView) view.findViewById(R.id.id_next);
            theimage = (ImageView) view.findViewById(R.id.id_image);
            thepanel = (LinearLayout) view.findViewById(R.id.id_panel);
        }

    }
}
