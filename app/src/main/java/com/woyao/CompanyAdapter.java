package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.CompanySummary;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private  List<CompanySummary> thedata = new ArrayList<CompanySummary>();
    private Changed onChanged;

    private String Selection = null ;
    Context thecontext;

    public CompanyAdapter(Context context, List<CompanySummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata= data;

    }


    public interface Changed{
        void view(CompanySummary id);
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.chance_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        CompanySummary curData =  thedata.get(position);

        holder.theTitle.setText( curData.getTitle() );

        holder.theSubtitle.setText( curData.getDescription() );


            holder.theMsg.setVisibility(View.INVISIBLE);

        holder.theImage.setImageResource(R.drawable.no_avartar);
        if (StringUtil.notNullOrEmpty(curData.getSnailview())) {
            Picasso.with(thecontext)
                    .load( curData.getSnailview())
                    .into(holder.theImage );
        }
        holder.theContent.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                CompanySummary curdata = thedata.get(holder.getAdapterPosition());
                onChanged.view(curdata );
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theTitle;
        public TextView theSubtitle;
        public LinearLayout theContent;
        public CircleImageView theImage;
        public Button theMsg;

        public ViewHolder(View view) {
            super(view);
            theTitle = (TextView) view.findViewById(R.id.chance_title);
            theSubtitle = (TextView) view.findViewById(R.id.chance_subtext);
            theContent = (LinearLayout) view.findViewById(R.id.chance_content);
            theImage = (CircleImageView) view.findViewById(R.id.chance_image);
            theMsg  = (Button)  view.findViewById(R.id.chance_msg);
        }

    }

}
