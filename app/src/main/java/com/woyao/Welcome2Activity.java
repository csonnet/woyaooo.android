package com.woyao;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.woyao.WoyaoooApplication.userId;

public class Welcome2Activity extends AppCompatActivity {
    private ViewPager viewPager;
    private TextView thetitle;
    private TextView welcome_next;
    private TextView thedescription;
    private Button visit;
    private Button loginButton;
    private Button registerButton;
    public static Welcome2Activity instance = null;

    private String[] titles = new String[]{
           "平等丰富",
           "高效公平",
            "稳定诚信",
    };
    private String[] descriptions = new String[]{
            "大数据人工智能深入挖掘分析，融汇各行各业开发经营者的合作智慧",
            "诺贝尔奖算法智能匹配合作需求，不断为大家创造丰富的合作机会",
            "移动互联随时随地把握合作趋势，建立关系，让我们合作共赢",
    };
    private int[] images = new int[]{
            R.drawable.ic_launcher,
            R.drawable.ic_launcher,
            R.drawable.ic_launcher
    };
    private List<ImageView> imageViews;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        setContentView(R.layout.activity_welcome2);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        thetitle = (TextView) findViewById(R.id.ad_title);
        thedescription = (TextView) findViewById(R.id.ad_description);
        imageViews = new ArrayList<ImageView>();



        for (int i = 0; i < images.length; i++) {
            ImageView iv = new ImageView(Welcome2Activity.this);
            iv.setImageResource(images[i]);
            imageViews.add(iv);
        }

        welcome_next = (TextView) findViewById(R.id.welcome_next);

        visit = (Button) findViewById(R.id.visit);


        visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences shared = getApplication().getSharedPreferences("login", MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putString("username", "匿名");
                editor.putBoolean("logged", false);
                editor.putInt("userId", 0);
                userId = 0;
                editor.commit();

                Intent intent = new Intent(Welcome2Activity.this, MainActivity.class);
                startActivity(intent);
            }
        });


        loginButton = (Button) findViewById(R.id.login_button);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Welcome2Activity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        registerButton = (Button) findViewById(R.id.register_button);


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Welcome2Activity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });


        //为ViewPager添加适配器
        viewPager.setAdapter(new MyAdapter());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == images.length - 1) {
                    visit.setVisibility(View.VISIBLE);
                    loginButton.setVisibility(View.VISIBLE);
                    registerButton.setVisibility(View.VISIBLE);
                } else {
                    loginButton.setVisibility(View.GONE);
                    registerButton.setVisibility(View.GONE);
                    visit.setVisibility(View.GONE);
                }
                welcome_next.setText( position + 1 + "/3");
                thetitle.setText(titles[position] );
                thedescription.setText(descriptions[position] );
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    class MyAdapter extends PagerAdapter {
        //返回导航页的个数
        @Override
        public int getCount() {
            return images.length;
        }

        //判断是否由对象生成
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        //加载页面
        //ViewGroup:父控件指ViewPager
        //position:当前子控件在父控件中的位置
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView iv = imageViews.get(position);
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            container.addView(iv);
            return iv;
        }

        //移除页面
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
