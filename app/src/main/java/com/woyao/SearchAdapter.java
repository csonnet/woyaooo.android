package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.SearchItem;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder>
{
    private LayoutInflater mInflater;

    private  List<SearchItem> thedata = new ArrayList<SearchItem>();

    private SearchAdapter.Changed onChanged;
    String type;

    Context thecontext;

    public SearchAdapter(Context context, List<SearchItem> data ,String thetype) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);
        type = thetype;
        thedata.addAll(data);

    }

    public void setChangedHandler(SearchAdapter.Changed changed){
        this.onChanged = changed;
    }

    public interface Changed{
        void view(SearchItem cs);
        void act(SearchItem cs);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.search_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {

        final SearchItem curData =  thedata.get(position);
        holder.theOrder.setText( position +  1+"");

        holder.theTitle.setText(curData.getTitle());
        holder.theDescription.setText(curData.getDescription());

        if (type.equals("contact")){
            holder.actBtn.setVisibility(View.VISIBLE);
        }else{
            holder.actBtn.setVisibility(View.GONE);
        }

        String snail = curData.getSnailview();
        if (StringUtil.notNullOrEmpty(snail)) {

            Picasso.with(thecontext)
                    .load(snail)
                    .into(holder.snailview);
        }

        holder.panel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchItem curdata = thedata.get(holder.getAdapterPosition());

                onChanged.view(curdata  );
            }
        });

        holder.actBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchItem curdata = thedata.get(holder.getAdapterPosition());
                onChanged.act(curdata  );
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theOrder;
        public TextView theTitle;
        public TextView theDescription;
        public Button actBtn;
        public CircleImageView snailview;
        public LinearLayout panel;

        public ViewHolder(View view) {
            super(view);
            panel = (LinearLayout) view.findViewById(R.id.id_panel);
            theOrder = (TextView) view.findViewById(R.id.user_order);
            theTitle = (TextView) view.findViewById(R.id.user_title);
            theDescription = (TextView) view.findViewById(R.id.user_description);
            actBtn = (Button) view.findViewById(R.id.id_user_act_relation);
            snailview  = (CircleImageView) view.findViewById(R.id.user_snailview);
        }

    }

}
