package com.woyao;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import static com.woyao.WoyaoooApplication.userId;

public class WebviewActivity extends AppCompatActivity {
    private  String link ;
    private  WebView webview ;
    private ClipboardManager myClipboard;
    private ClipData myClip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //       ab.setHomeAsUpIndicator(R.drawable.icon_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
//        ab.setDisplayShowTitleEnabled(false); // disable the default title element

        Intent intent = getIntent();
        link = intent.getStringExtra("link");

        Boolean sharable = intent.getBooleanExtra( "sharable", true);


        this.setTitle(intent.getStringExtra("title"));

        final Button sharebtn = (Button) findViewById(R.id.web_share);

        if (!sharable){
            sharebtn.setVisibility(View.GONE);
        }

        sharebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String text = link;

                Dialog alertDialog = new AlertDialog.Builder(WebviewActivity.this).
                        setTitle("分享").
                        setMessage(text).
                        setIcon(R.drawable.ic_launcher).
                        setPositiveButton("复制", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                myClip = ClipData.newPlainText("text", text);
                                myClipboard.setPrimaryClip(myClip);
                            }
                        }).
                        create();
                alertDialog.show();
            }
        });

        webview = (WebView)findViewById(R.id.webView);

        webview.loadUrl(link);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent();
        setResult(0, intent);
        finish();

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }



}
