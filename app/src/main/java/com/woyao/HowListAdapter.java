package com.woyao;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.How;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class HowListAdapter extends RecyclerView.Adapter<HowListAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private ArrayList<How> thedata = new ArrayList<How>();
    private Changed onChanged;
    Context thecontext;

    public HowListAdapter(Context context, ArrayList<How> data  ) {

        this.mInflater = LayoutInflater.from(context);
        thecontext = context;
        this.thedata = data;

    }


    public interface Changed{
        void itemSelected(How one);
        void itemGetChildren(How one);
    }

    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public HowListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.how_tree_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HowListAdapter.ViewHolder holder, final int position) {


        holder.thetitle.setText( thedata.get(position).getName() );

        holder.thedescription.setText( thedata.get(position).getDescription() );

        if (StringUtil.notNullOrEmpty(thedata.get(position).getImage()) ) {
            holder.theimage.setVisibility(View.VISIBLE);
            holder.theimage.setImageResource(R.drawable.no_avartar);
                Picasso.with(thecontext)
                        .load(thedata.get(position).getImage())
                        .into(holder.theimage);

        }else{
            holder.theimage.setVisibility(View.GONE);
        }

        if (thedata.get(position).getChilds() > 0  ) {
            holder.thenext.setVisibility(View.VISIBLE);
        }else{
            holder.thenext.setVisibility(View.GONE);
        }


        if (thedata.get(position).getDescription().length()==0 ) {
            holder.thedescription.setVisibility(View.GONE);
        }else{
            holder.thedescription.setVisibility(View.VISIBLE);
        }
        holder.thepanel.setBackgroundColor(Color.WHITE);


        holder.thepanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( thedata.get(position).getChilds() > 0  ) {
                    onChanged.itemGetChildren(  thedata.get(position) );
                }else{

                    onChanged.itemSelected(thedata.get(position) );
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public TextView thedescription;
        public LinearLayout thepanel;
        public ImageView thenext;
        public ImageView theimage;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.id_text);
            thedescription = (TextView) view.findViewById(R.id.id_description);
            thenext = (ImageView) view.findViewById(R.id.id_next);
            theimage = (ImageView) view.findViewById(R.id.id_image);
            thepanel = (LinearLayout) view.findViewById(R.id.id_panel);
        }

    }
}
