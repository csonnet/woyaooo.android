package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.woyao.core.model.Category;
import com.woyao.core.model.GetChildCategoryResponse;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

public class ConfirmCategory extends AppCompatActivity {

    LinearLayoutManager allLayoutManager = null;
    private RecyclerView alllist = null;
    private SingleTreeAdapter allAdapter;

    Boolean changed = false;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_confirm_type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //       ab.setHomeAsUpIndicator(R.drawable.icon_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
//        ab.setDisplayShowTitleEnabled(false); // disable the default title element


        alllist = (RecyclerView) findViewById(R.id.id_type_list);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));


        this.setTitle("请选择合作领域");

        loadAllData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

            Intent intent = new Intent();
            intent.putExtra("changed", changed);
            setResult(0, intent);
            finish();

            return true;

    }




   private void loadAllData(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("拼命加载中······");
        progressDialog.show();

        AsyncTask<Void,Void,GetChildCategoryResponse> task =
                new AsyncTask<Void, Void, GetChildCategoryResponse>() {
                    @Override
                    protected GetChildCategoryResponse doInBackground(Void... params) {
                        AccountService svc = ServiceFactory.get(AccountService.class);
                        Call<GetChildCategoryResponse> responseCall = svc.getChildCategory("",WoyaoooApplication.userId,"");
                        try {
                            GetChildCategoryResponse response = responseCall.execute().body();
                            return response;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                    @Override
                    protected void onPostExecute(final GetChildCategoryResponse response) {
                        progressDialog.dismiss();
                        ArrayList<Category> categories = response.getCategoryList();
                        renderAll(categories);
                    }
                    @Override
                    protected void onCancelled() {
                        progressDialog.dismiss();
                    }
                };
        task.execute();
    }


    private void renderAll( ArrayList<Category> categories ){

        ArrayList<Map<String,String>> mapList = new ArrayList<>();

        for(Category cattt : categories){
            HashMap<String,String> map = new HashMap<>();
            map.put("no", cattt.getNo());
            map.put("name", cattt.getName());
            map.put("description", cattt.getDescription());
            map.put("image", cattt.getImage());
            map.put("childs", cattt.getChilds() +"");
            mapList.add(map);
        }
        allAdapter = new SingleTreeAdapter(this,  mapList );

        allAdapter.setChangedHandler(new SingleTreeAdapter.Changed() {
            @Override
            public void itemSelected(Map<String, String> one) {
                Intent intent = new Intent();
                intent.putExtra("category", one.get("no"));
                intent.putExtra("category_name", one.get("name"));
                setResult(666, intent);
                finish();
//                    Intent intent = new Intent();
//                    intent.putExtra("from", "chance"  );
//                    intent.putExtra("is_new", true);
//                    intent.putExtra("category", one.get("no"));
//                    intent.putExtra("category_name", one.get("name"));
//                    intent.putExtra("description", one.get("description"));
//                    intent.setClass(ConfirmCategory.this, DemandNewActivity.class);
//                    startActivityForResult(intent, 1);
            }


            @Override
            public void itemGetChildren(Map<String, String> one) {

                loadAllData();
            }
        });


        this.alllist.setAdapter(allAdapter);



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 666 ) {

            Intent intent = new Intent();
            Integer demand_id = data.getIntExtra("demand_id", 0);
            if (demand_id == 0) {
                intent.putExtra("demand_id", demand_id);
                intent.putExtra("changed", false);
                setResult(0, intent);
            } else {
                intent.putExtra("demand_id", demand_id);
                intent.putExtra("changed", true);
                setResult(666, intent);
            }
            finish();
        }

    }



        @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent();
            intent.putExtra("changed", changed);
            setResult(0, intent);
            finish();

            return true;

        }
        return super.onKeyDown(keyCode, event);
    }

}

