package com.woyao;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.Service;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class ServiceMgmtAdapter extends RecyclerView.Adapter<ServiceMgmtAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private List<Service> thedata = new ArrayList<Service>();
    private Changed onChanged;

    Context thecontext;

    public ServiceMgmtAdapter(Context context, List<Service> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata= data;

    }


    public interface Changed{
        void view(Service ms);
        void buy(Service ms);
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.service_mgmt_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Service curData =  thedata.get(position);

        holder.theOrder.setText(  (position +1) +"") ;
        
        if (curData.getRecommend()> 0){
            holder.theWhole.setBackgroundColor(  ContextCompat.getColor(thecontext, R.color.colorLightGrey));

        }else{
            holder.theWhole.setBackgroundColor(  ContextCompat.getColor(thecontext, R.color.colorWhite));
        }
        holder.theTitle.setText( curData.getTitle() );

        holder.theSubtitle.setText( curData.getDescription() );

        if (StringUtil.notNullOrEmpty(curData.getImage())) {
            Picasso.with(thecontext)
                    .load(curData.getImage())
                    .into(holder.theImage);
            holder.theImage.setVisibility(View.VISIBLE);
        }else{
            holder.theImage.setVisibility(View.GONE);
        }
        
        holder.theContent.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Service curdata = thedata.get(holder.getAdapterPosition());
                onChanged.view(curdata  );
            }
        });

        holder.manageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Service curdata = thedata.get(holder.getAdapterPosition());
                onChanged.buy(curdata  );

            }
        });


    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout theWhole;
        public TextView theTitle;
        public TextView theSubtitle;
        public LinearLayout theContent;
        public ImageView theImage;
        public Button manageBtn;
        public TextView theOrder;

        public ViewHolder(View view) {
            super(view);

            theTitle = (TextView) view.findViewById(R.id.id_service_title);
            theSubtitle = (TextView) view.findViewById(R.id.id_service_description);
            theContent = (LinearLayout) view.findViewById(R.id.id_service_content);
            theWhole = (LinearLayout) view.findViewById(R.id.id_service_mgmt_item);

            theImage = (ImageView) view.findViewById(R.id.id_service_image);
            manageBtn  = (Button)  view.findViewById(R.id.id_service_action);

            theOrder  = (TextView)  view.findViewById(R.id.id_service_order);
        }

    }




}
