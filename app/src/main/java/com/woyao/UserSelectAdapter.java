package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.UserSummary;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class UserSelectAdapter extends RecyclerView.Adapter<UserSelectAdapter.ViewHolder>
{
    private LayoutInflater mInflater;

    private  List<UserSummary> thedata = new ArrayList<UserSummary>();

    private UserSelectAdapter.Changed onChanged;

    Context thecontext;

    public UserSelectAdapter(Context context, List<UserSummary> data) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);
        thedata.addAll(data);

    }

    public void setChangedHandler(UserSelectAdapter.Changed changed){
        this.onChanged = changed;
    }

    public interface Changed{
        void view(UserSummary cs);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.user_select_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {

        final UserSummary curData =  thedata.get(position);
        holder.theOrder.setText( position +  1+"");

        holder.theTitle.setText(curData.getTitle());
        holder.theDescription.setText(curData.getDescription());

        String snail = curData.getSnailview();
        if (StringUtil.notNullOrEmpty(snail)) {

            Picasso.with(thecontext)
                    .load(snail)
                    .into(holder.snailview);
        }
        holder.theCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserSummary curdata = thedata.get(holder.getAdapterPosition());
                curdata.setSelected( !curData.getSelected());
                holder.theCheck.setChecked(curData.getSelected());

                onChanged.view(curdata  );
            }
        });

        holder.panel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserSummary curdata = thedata.get(holder.getAdapterPosition());
                curdata.setSelected( !curData.getSelected());
                holder.theCheck.setChecked(curData.getSelected());

                onChanged.view(curdata  );
            }
        });



    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theOrder;
        public TextView theTitle;
        public TextView theDescription;
        public CircleImageView snailview;
        public LinearLayout panel;
        public CheckBox theCheck;

        public ViewHolder(View view) {
            super(view);
            panel = (LinearLayout) view.findViewById(R.id.id_panel);
            theOrder = (TextView) view.findViewById(R.id.user_order);
            theTitle = (TextView) view.findViewById(R.id.user_title);
            theDescription = (TextView) view.findViewById(R.id.user_description);
            snailview  = (CircleImageView) view.findViewById(R.id.user_snailview);
            theCheck  = (CheckBox) view.findViewById(R.id.user_checkbox);
        }

    }

}
