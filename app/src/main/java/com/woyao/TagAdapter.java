package com.woyao;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.woyao.core.model.Tag;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private List<Tag> thedata = new ArrayList<Tag>();
    private Changed onChanged;
    Context thecontext;
    Resources resources;
    private TextView lastTxt = null;
    private Integer lastPos = 0;

    public TagAdapter(Context context, Resources res, List<Tag> data  ) {

        this.mInflater = LayoutInflater.from(context);
        thecontext = context;
        resources  = res;
        thedata = data;

    }


    public interface Changed{
        void itemSelected(Tag one);
    }

    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }


    @Override
    public TagAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.tag_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TagAdapter.ViewHolder holder, final int position) {


        holder.thetitle.setText( thedata.get(position).getTitle() );

        if (thedata.get(position).getSelected()){
            holder.thetitle.setTextColor(resources.getColor(R.color.colorAccent));
            lastTxt = holder.thetitle;
            lastPos = position;
        }else{
            holder.thetitle.setTextColor(resources.getColor(R.color.colorPrimaryDark));
        }
//
//
//        if (thedata.get(position).containsKey("image") && StringUtil.notNullOrEmpty(thedata.get(position).get("image")) ) {
//            holder.theimage.setVisibility(View.VISIBLE);
//            holder.theimage.setImageResource(R.drawable.no_avartar);
//                Picasso.with(thecontext)
//                        .load(thedata.get(position).get("image"))
//                        .into(holder.theimage);
//
//        }else{
//            holder.theimage.setVisibility(View.GONE);
//        }

        holder.thetitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastPos == position){
                    return;
                }

                Tag lastTag = thedata.get(lastPos);
                lastTag.setSelected(false);
                thedata.set(lastPos,lastTag);
                if (lastTxt != null){
                    lastTxt.setTextColor(resources.getColor(R.color.colorPrimaryDark));
                }

                lastPos = position;
                Tag tag = thedata.get(position);
                tag.setSelected(true);
                thedata.set(position,tag);
                lastTxt = (TextView) view;
                lastTxt.setTextColor(resources.getColor(R.color.colorAccent));
                onChanged.itemSelected(thedata.get(position) );

            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public CircleImageView theimage;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.tag_title);

//            theimage = (CircleImageView) view.findViewById(R.id.id_image);
//            thepanel = (LinearLayout) view.findViewById(R.id.id_panel);
        }

    }
}
