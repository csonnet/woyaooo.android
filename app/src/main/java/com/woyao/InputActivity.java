package com.woyao;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.Timer;
import java.util.TimerTask;

public class InputActivity extends AppCompatActivity {
    EditText editText = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        this.setTitle("输入内容");


        Button thebtn = (Button) findViewById(R.id.add_getdecription);

        thebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("content", editText.getText().toString());
                setResult(666, intent);
                finish();
            }
        });


        TextInputLayout textInputLayout = null;
        Intent intent = getIntent();
        String type =   intent.getStringExtra( "type") ;
        String help =   intent.getStringExtra( "help") ;


        if ( type != null  ) {
            switch (type) {
                case "refer_fee":
                    this.setTitle("悬赏介绍");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                    textInputLayout.setHint(getString(R.string.refer_fee));
                    break;
                case "comment":
                    this.setTitle("合作评价");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.relation_comment_help));
                    break;
                case "relation_memo":
                    this.setTitle("合作备注");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.relation_memo_help));
                    break;
                 case "relation_title":
                    this.setTitle("合作事宜");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.relation_title_help));
                     textInputLayout.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
                    break;
                case "relation_desc":
                    this.setTitle("合作内容");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.relation_description_help));
                    break;
                case "agreement":
                    this.setTitle("合作目的");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.agreement));
                    break;
                case "agreement_else":
                    this.setTitle("合作注意");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.agreement_else));
                    break;
                case "right_duty":
                    this.setTitle("合作分工");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.right_duty));
                    break;
                case "relation_right_duty":
                    this.setTitle("对方分工");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.relation_right_duty));
                    break;
                case "tag":
                    this.setTitle("关系标签");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint("填写关系标签");
                    break;
                case "role":
                    this.setTitle("组织身份");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint("填写组织身份");
                    break;

                case "member_desc":
                    this.setTitle("身份说明");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.member_desc));
                    break;
                case "orgaddress":
                    this.setTitle("组织地址");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint("请填写组织所在地址");
                    break;
                case "orgwebsite":
                    this.setTitle("组织网址");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint("请填写组织网站地址");
                    break;
                case "orgtitle":
                    this.setTitle("组织名称");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint("请填写组织名称");
                    textInputLayout.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(24)});
                    break;
                case "orgdesc":
                    this.setTitle("组织介绍");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint("请填写企业或机构介绍");
                    break;
                case "demandtitle":
                    if (help != null && !help.equals("")){
                        this.setTitle(help);
                    }else {
                        this.setTitle("请填写标题");
                    }
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.demand_title));
                    textInputLayout.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                    break;
                case "demandrequiremnt":
                    this.setTitle("合作要求");
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.demand_requirement_help));
                    break;
                case "demandresource":
                    if (help != null && !help.equals("")){
                        this.setTitle(help);
                    }else {
                        this.setTitle(getResources().getString(R.string.demand_resources));
                    }

                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getResources().getString(R.string.demand_resources));
                    break;

                case "displayname":
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.displayname));
                    break;
                case "intro":
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.intro));
                    break;
                case "phone":
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.phone));
                    break;
                case "wechat":
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.wechat));
                    break;
                case "email":
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_single);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.email));
                    break;
                case "office":
                    textInputLayout = (TextInputLayout) findViewById(R.id.id_multiple);
                    textInputLayout.setVisibility(View.VISIBLE);
                    textInputLayout.setHint(getString(R.string.office));
                    break;
                default:

            }
            if (help != null){
                textInputLayout.setHint(help);
            }
            editText = textInputLayout.getEditText();
            CharSequence tmp = intent.getCharSequenceExtra(  "content");
            editText.setText( tmp );
            editText.setSelection(tmp.length());



            editText.requestFocus();




        }


    }

    @Override
    protected void onResume() {
        super.onResume();
//        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            public void run() {
                InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(editText, 0);
            }

        }, 500);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

}
