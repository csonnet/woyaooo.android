package com.woyao;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class InterestScrollview extends ScrollView {

    private Changed onChanged = null;
    public InterestScrollview(Context context) {
        super(context);
    }
    public InterestScrollview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InterestScrollview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void requestChildFocus(View child, View focused) {
    }

    public interface Changed{
        void Changed( String state );
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }
    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

        int newScrollY = scrollY + deltaY;
        final int bottom = maxOverScrollY + scrollRangeY;
        final int top = -maxOverScrollY;
        if (newScrollY > bottom) {
            this.onChanged.Changed("bottom");
            System.out.println("滑动到底部");
        }
        else if (newScrollY < top) {
            this.onChanged.Changed("top");
            System.out.println("滑动到顶端");
        }
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
    }
}
