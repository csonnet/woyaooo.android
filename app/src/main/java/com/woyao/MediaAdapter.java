package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.MediaSummary;
import com.woyao.core.util.Common;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private  ArrayList<MediaSummary> thedata = new ArrayList<MediaSummary>();
    private Changed onChanged = null;
    public boolean changed = false;
    private String main_image ="";

    Context thecontext;

    public MediaAdapter(Context context, ArrayList<MediaSummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata= data;

    }

    public String getMainImage(){
        return main_image ;
    }

    public ArrayList<MediaSummary> getMedias(){
        return thedata;
    }

    public void addMedia( String filepath ,String  mediaid){
        changed = true;
        MediaSummary ms = new MediaSummary();
        ms.setId(mediaid);
        if (!filepath.endsWith("mp4")) {
            ms.setSnailview(filepath + "?x-oss-process=image/resize,m_fixed,h_60,w_60");
        }else{
            ms.setSnailview( Common.defaultpic );
        }
        ms.setUrl( filepath );

        Integer pos = thedata.size();
        notifyItemInserted( pos);
        thedata.add(pos,ms);
        notifyItemRangeChanged(pos,thedata.size());

        if (onChanged != null) onChanged.Changed( thedata.size() );
    }

    public interface Changed{
        void Changed( Integer count );
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.media_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
            final MediaSummary curData =  thedata.get(position);
            holder.theImage.setImageResource(R.drawable.no_avartar);
            if (StringUtil.notNullOrEmpty(curData.getUrl())) {
                if (curData.getUrl().endsWith( "mp4")) {
                    holder.thePlayBtn.setVisibility( View.VISIBLE);
                }else {
                    holder.thePlayBtn.setVisibility( View.GONE);
                }
                Picasso.with(thecontext)
                        .load(curData.getSnailview())
                        .into(holder.theImage);
            }

            holder.delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    thedata.remove( position );
                    changed = true;
                    notifyItemRemoved( position);
                    notifyItemRangeChanged(position,thedata.size());
                    if (onChanged != null) onChanged.Changed(thedata.size() );
                }
            });

//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    View contentView=LayoutInflater.from(thecontext).inflate(R.layout.media_action, null, false);
////                    PopupWindow window=new PopupWindow(contentView);
//                    final PopupWindow window=new PopupWindow(contentView, 240, 240, true);
//                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    window.setOutsideTouchable(true);
//                    window.showAsDropDown(v, 0, 0);
//
//                    Button  thebtn = (Button) contentView.findViewById(R.id.media_set_main);
//                    thebtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            main_image =  curData.getId();
//                            changed = true;
//                            window.dismiss();
////                             Common.alert(thecontext,curData.getUrl() );
////                            onChanged.setMain(curData.getId()  );
//
//
//                        }
//                    });
//
//
//
//                    Button  deletebtn = (Button) contentView.findViewById(R.id.media_set_delete);
//                    deletebtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            changed = true;
//                            window.dismiss();
////                            onChanged.delete(curData.getId()  );
//                            notifyItemRemoved( position);
//                            thedata.remove( position );
//                            notifyItemRangeChanged(position,thedata.size());
//                        }
//                    });
//
////                    Dialog alertDialog = new AlertDialog.Builder(thecontext).
////                            setTitle("操作").
////                            setMessage("有图有真相，视频更真诚").
////                            setIcon(R.drawable.ic_launcher).
////                            setPositiveButton("设置为主图", new DialogInterface.OnClickListener() {
////
////                                @Override
////                                public void onClick(DialogInterface dialog, int which) {
////                                    onChanged.setMain(curData.getUrl()  );
////                                }
////                            }).
////                            setNegativeButton("删除", new DialogInterface.OnClickListener() {
////
////                                @Override
////                                public void onClick(DialogInterface dialog, int which) {
////
////                                    onChanged.delete(curData.getOrigin()  );
////                                    notifyItemRemoved( position);
////                                    thedata.remove( position );
////                                    notifyItemRangeChanged(position,thedata.size());
////                                    // delete the pic in server now
////                                }
////                            }).
////                            create();
////                    alertDialog.show();
//                }
//            });


    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {



    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView theImage;
        public Button thePlayBtn;
        public Button delBtn;

        public ViewHolder(View view) {
            super(view);
            theImage = (ImageView) view.findViewById(R.id.media_pic);
            thePlayBtn = (Button) view.findViewById(R.id.media_play);
            delBtn = (Button) view.findViewById(R.id.media_delete);
        }

    }

}
