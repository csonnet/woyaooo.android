package com.woyao;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.woyao.core.util.Common;

import java.util.Timer;
import java.util.TimerTask;

public class VideoActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback{

    private SurfaceView surfaceView;
    private MediaRecorder mediaRecorder;

    String videoPath = "";
    private ProgressBar progress;
    private int currentProgress = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_video);

        surfaceView = ( SurfaceView) findViewById(R.id.surfaceView1);
        surfaceView.getHolder().addCallback(this);

        progress = ( ProgressBar) findViewById(R.id.recording );

        videoPath = Common.getDiskCachePath(this) +"/"+System.currentTimeMillis()+".mp4";
//        mVideoView.setVideoURI (Uri.parse ("android.resource://"+getPackageName ()+"/"+R.raw.video));

        progress = (ProgressBar) findViewById(R.id.recording);




    }



    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.btnStart)
        {
            if (mediaRecorder!=null)
                return;
            // 刻录按钮
            try {
                mediaRecorder=new MediaRecorder();

                Camera camera = Camera.open();
                Camera.Parameters parameters = camera.getParameters();
//              parameters.setRotation(90);
//                parameters.setPreviewSize(640, 480);
//                parameters.setPictureSize(640, 480);
//                camera.setParameters(parameters);
                camera.setDisplayOrientation(90);
                camera.unlock();
                mediaRecorder.setCamera(camera);
                mediaRecorder.reset();

                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//                mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//                mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
                mediaRecorder.setVideoFrameRate(100);//每秒3帧
                mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
                mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
                mediaRecorder.setMaxDuration(60*1000*1);


//设置最大录制的大小 单位，字节
                mediaRecorder.setMaxFileSize(1024*1024*10);
//音频一秒钟包含多少数据位
                mediaRecorder.setAudioEncodingBitRate(128);
//设置选择角度，顺时针方向，因为默认是逆向90度的，这样图像就是正常显示了,这里设置的是观看保存后的视频的角度
                mediaRecorder.setOrientationHint(90);


//设置录像的分辨率
                mediaRecorder.setVideoSize(640, 480);
                mediaRecorder.setVideoEncodingBitRate(5 * 1024 * 1024 );  // 设置帧频率，然后就清晰了
                mediaRecorder.setOutputFile(videoPath);
                mediaRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
                mediaRecorder.prepare();
                mediaRecorder.start();

                TimerTask task = new TimerTask(){
                    public void run(){
                        //execute the task
                        if (progress.getProgress() < 100)  {
                            currentProgress += 1 ;
                            progress.setProgress(  currentProgress  );
                        }else {
                            stoppp();
                        }
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 0, 600);

//            Toast.makeText(getApplicationContext(), "录像", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
           view.setVisibility(View.GONE);
        }
        if (view.getId()==R.id.btnStop)
        {
            stoppp();
        }

    }

    void stoppp(){
        if (mediaRecorder==null)
            return;
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder=null;
//            Toast.makeText(getApplicationContext(), "停止录像", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.putExtra("path", videoPath );
        setResult(666, intent);
        finish();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }
}
