package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.woyao.core.model.Category;
import com.woyao.core.model.GetChildCategoryResponse;
import com.woyao.core.model.KeyValueList;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

public class FilterCategory extends AppCompatActivity {
    TextView categoryTitleTxt = null;
    HashMap<String,String> parents = new HashMap<String,String>() ;

    private  String curCategory = "";

    private RecyclerView popList = null;
    LinearLayoutManager mLayoutManager =null;
    private NewFilterAapter mAdapter;

    LinearLayoutManager allLayoutManager = null;
    private RecyclerView alllist = null;
    private SingleTreeAdapter allAdapter;

    ProgressDialog progressDialog;
    Boolean changed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //       ab.setHomeAsUpIndicator(R.drawable.icon_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
//        ab.setDisplayShowTitleEnabled(false); // disable the default title element

        popList = (RecyclerView) findViewById(R.id.recyclerCategory);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        popList.setLayoutManager(mLayoutManager);

        alllist = (RecyclerView) findViewById(R.id.id_type_list);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        TextView roottitle = (TextView) findViewById(R.id.type_roottitle);

        roottitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curCategory ="";
                categoryTitleTxt.setText(curCategory);
                loadAllData();
            }
        });

        categoryTitleTxt = (TextView) findViewById(R.id.alltitle);

        Button gobackbtn = (Button) findViewById(R.id.goback);

        gobackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (parents.get(curCategory) != null) {

                    curCategory = parents.get(curCategory) ;
                    categoryTitleTxt.setText(curCategory);
                    loadAllData();
                }
            }
        });

        Button confirmbtn = (Button) findViewById(R.id.id_filter_category_confirm);
        Button nextbtn = (Button) findViewById(R.id.filter_type_finish);
        confirmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result",mAdapter.getSelectionKeyValue() );
                setResult(666, intent);
                finish();
            }
        });
        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result",mAdapter.getSelectionKeyValue() );
                setResult(666, intent);
                finish();
            }
        });
        setTitle("请选择合作行业");

        Intent intent = getIntent();

//        String from = intent.getStringExtra("from");
//        if ( from != null && from.equals("search")) {
//            setTitle("选择寻求的资源");
//        }
        KeyValueList initList = (KeyValueList)intent.getExtras().get("category_list");

        mAdapter = new NewFilterAapter(this, initList.getContent(),initList.getContent());

        mAdapter.setChangedHandler( new NewFilterAapter.Changed(){
            @Override
            public void dataChanged() {
                changed = true;
            }
        });

        popList.setAdapter(mAdapter);

        loadAllData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (parents.get(curCategory) != null) {

            curCategory = parents.get(curCategory) ;
            categoryTitleTxt.setText(curCategory);
            loadAllData();
            return false;
        }else {
            Intent intent = new Intent();
            intent.putExtra("changed", changed);
            setResult(0, intent);
            finish();

            return true;
        }
    }


    private void loadAllData(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("拼命加载中······");
        progressDialog.show();

        AsyncTask<Void,Void, GetChildCategoryResponse> task =
                new AsyncTask<Void, Void, GetChildCategoryResponse>() {
                    @Override
                    protected GetChildCategoryResponse doInBackground(Void... params) {
                        AccountService svc = ServiceFactory.get(AccountService.class);
                        Call<GetChildCategoryResponse> responseCall = svc.getChildCategory(curCategory,WoyaoooApplication.userId,"");
                        try {
                            GetChildCategoryResponse response = responseCall.execute().body();
                            return response;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                    @Override
                    protected void onPostExecute(final GetChildCategoryResponse response) {
                        progressDialog.dismiss();
                        ArrayList<Category> categories = response.getCategoryList();
                        renderAll(categories);
                    }
                    @Override
                    protected void onCancelled() {
                        progressDialog.dismiss();
                    }
                };
        task.execute();
    }


    private void renderAll( ArrayList<Category> categories ){

        ArrayList<Map<String,String>> mapList = new ArrayList<>();

        for(Category cattt : categories){
            HashMap<String,String> map = new HashMap<>();
            map.put("no", cattt.getNo());
            map.put("name", cattt.getName());
            map.put("description", cattt.getDescription());
            map.put("image", cattt.getImage());
            map.put("childs", cattt.getChilds() +"");
            map.put("selected", cattt.getSelectable().toString());
            mapList.add(map);
        }
        allAdapter = new SingleTreeAdapter(this,  mapList );

        allAdapter.setChangedHandler(new SingleTreeAdapter.Changed() {
            @Override
            public void itemSelected(Map<String, String> one) {

                mAdapter.AddNewOne( one) ;
                mAdapter.notifyDataSetChanged();
                changed = true;

            }


            @Override
            public void itemGetChildren(Map<String, String> one) {
                parents.put(one.get("name"), curCategory);
                curCategory = one.get("name");
                categoryTitleTxt.setText(curCategory);

                loadAllData();
            }
        });


        this.alllist.setAdapter(allAdapter);



    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (parents.get(curCategory) != null) {

                curCategory = parents.get(curCategory) ;
                categoryTitleTxt.setText(curCategory);
                loadAllData();
                return false;
            }else {
                Intent intent = new Intent();
                setResult(0, intent);
                finish();
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}



