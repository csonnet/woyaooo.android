package com.woyao;

import android.content.Context;
import android.os.AsyncTask;

import com.woyao.core.util.Common;

public class VideoSnapTask extends AsyncTask<Void, Void, String> {
    private String videoPath;
    private Context context;
    private Callback callback;

    // 定义回调接口，用于任务完成后通知调用者
    public interface Callback {
        void onSnapCompleted(String imagePath);
        void onError(Exception e);
    }

    public VideoSnapTask(String videoPath, Context context, Callback callback) {
        this.videoPath = videoPath;
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {

            String imagePath = Common.getVideoSnapFile( videoPath, context);

            return imagePath;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {
            callback.onSnapCompleted(result);
        } else {
            callback.onError(new Exception("Failed to generate snapshot"));
        }
    }


}

