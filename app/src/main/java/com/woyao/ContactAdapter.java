package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.woyao.core.model.ContactSummary;
import com.woyao.core.model.UserSummary;
import com.woyao.core.util.Common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder>
{
    private LayoutInflater mInflater;

    private  List<ContactSummary> thedata = new ArrayList<ContactSummary>();

    private ContactAdapter.Changed onChanged;

    Context thecontext;
    private  Boolean changing = false;

    public ContactAdapter(Context context,  List<ContactSummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata.addAll(data);

    }

    public void setChangedHandler(ContactAdapter.Changed changed){
        this.onChanged = changed;
    }

    public interface Changed{
        void view( UserSummary cs );
        void selectionChanged( Integer num);
    }

    public String getSelection() {

        ArrayList<String> it = new ArrayList<String>();

        for(ContactSummary one: thedata){
            if (  one.getSelected()  ){
                it.add(one.getTitle() +"#" + one.getDescription());
            }else{
                Log.i("woyaooo",one.getTitle());
            }
        }

        return  Common.listToString(it );
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.invite_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {

        final ContactSummary curData =  thedata.get(position);
        holder.theOrder.setText( position +  1+"");

//        if (curData.getInvited()){
//            holder.inviteBtn.setEnabled(false);
//            holder.inviteBtn.setText("已邀约");
//        }else{
//            holder.inviteBtn.setEnabled(true);
//            holder.inviteBtn.setText("邀约");
//        }
        changing = true;
        holder.theCheck.setChecked( curData.getSelected());
        changing = false;

        holder.theTitle.setText(curData.getTitle());
        holder.theDescription.setText(curData.getDescription());

//        holder.inviteBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                UserSummary curdata = thedata.get(holder.getAdapterPosition());
//                view.setEnabled(false);
//                ((Button)view).setText("已邀约");
//                ((Button)view).setEnabled(false);
//                onChanged.view(curdata  );
//
//            }
//        });

        holder.theCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!changing) {
                    ContactSummary curdata = thedata.get(holder.getAdapterPosition());

                    curdata.setSelected(b);
                    ArrayList<String> it = new ArrayList<String>();

                    for(ContactSummary one: thedata){
                        if (  one.getSelected()  ){
                            it.add(one.getTitle() +"#" + one.getDescription());
                        }else{
                            Log.i("woyaooo",one.getTitle());
                        }
                    }
                    onChanged.selectionChanged( it.size());
                }
            }
        });

//        holder.theCheck.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                UserSummary curdata = thedata.get(holder.getAdapterPosition());
//
//                curData.setSelected(!curData.getSelected());
////                holder.theCheck.setChecked(curData.getSelected());
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theOrder;
        public TextView theTitle;
        public TextView theDescription;
        public LinearLayout thePanel;
        public CheckBox theCheck;
        public Button inviteBtn;

        public ViewHolder(View view) {
            super(view);
            thePanel = (LinearLayout) view.findViewById(R.id.id_contact_panel);
            theOrder = (TextView) view.findViewById(R.id.contact_order);
            theTitle = (TextView) view.findViewById(R.id.contact_title);
            theDescription = (TextView) view.findViewById(R.id.contact_desc);
            theCheck  = (CheckBox) view.findViewById(R.id.contact_checkbox);
            inviteBtn = (Button) view.findViewById(R.id.id_invite_one);
        }

    }

}
