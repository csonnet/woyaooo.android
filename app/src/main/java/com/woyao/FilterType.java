package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.woyao.core.model.GetChildTypeResponse;
import com.woyao.core.model.KeyValue;
import com.woyao.core.model.KeyValueList;
import com.woyao.core.model.Type;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.Common;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

public class FilterType extends AppCompatActivity {
    TextView typeTitleTxt = null;
    HashMap<String,String> parents = new HashMap<String,String>() ;

    private  String curType = "";

    private RecyclerView popList = null;
    LinearLayoutManager mLayoutManager =null;
    private NewFilterAapter mAdapter;

    LinearLayoutManager allLayoutManager = null;
    private RecyclerView alllist = null;
    private SingleTreeAdapter allAdapter;

    ProgressDialog progressDialog;
    Boolean changed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter_type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //       ab.setHomeAsUpIndicator(R.drawable.icon_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
//        ab.setDisplayShowTitleEnabled(false); // disable the default title element

        popList = (RecyclerView) findViewById(R.id.recyclerType);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        popList.setLayoutManager(mLayoutManager);

        alllist = (RecyclerView) findViewById(R.id.id_type_list);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        TextView roottitle = (TextView) findViewById(R.id.type_roottitle);

        roottitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curType ="";
                typeTitleTxt.setText(curType);
                loadAllData();
            }
        });

        typeTitleTxt = (TextView) findViewById(R.id.alltitle);

        Button gobackbtn = (Button) findViewById(R.id.goback);

        gobackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (parents.get(curType) != null) {

                    curType = parents.get(curType) ;
                    typeTitleTxt.setText(curType);
                    loadAllData();
                }
            }
        });

        Button nextbtn = (Button) findViewById(R.id.filter_type_finish);

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result",mAdapter.getSelectionKeyValue() );
                setResult(666, intent);
                finish();
            }
        });
        setTitle("请选择资源");

        Intent intent = getIntent();

//        String from = intent.getStringExtra("from");
//        if ( from != null && from.equals("search")) {
//            setTitle("选择寻求的资源");
//        }
        KeyValueList initList = (KeyValueList)intent.getExtras().get("type_list");
        String thetitle = intent.getStringExtra("title");
        if (thetitle != null){
            setTitle( thetitle);
        }

        mAdapter = new NewFilterAapter(this, initList.getContent(),initList.getContent());

        mAdapter.setChangedHandler( new NewFilterAapter.Changed(){
            @Override
            public void dataChanged() {
                changed = true;
            }
        });

        popList.setAdapter(mAdapter);

        loadAllData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (parents.get(curType) != null) {

            curType = parents.get(curType) ;
            typeTitleTxt.setText(curType);
            loadAllData();
            return false;
        }else {
            Intent intent = new Intent();
            intent.putExtra("changed", changed);
            setResult(0, intent);
            finish();

            return true;
        }
    }




    private void loadAllData(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("拼命加载中······");
        progressDialog.show();

        AsyncTask<Void,Void, GetChildTypeResponse> task =
                new AsyncTask<Void, Void, GetChildTypeResponse>() {
                    @Override
                    protected GetChildTypeResponse doInBackground(Void... params) {
                        AccountService svc = ServiceFactory.get(AccountService.class);
                        Call<GetChildTypeResponse> responseCall = svc.getChildType(curType,WoyaoooApplication.userId,"filter");
                        try {
                            GetChildTypeResponse response = responseCall.execute().body();
                            return response;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                    @Override
                    protected void onPostExecute(final GetChildTypeResponse response) {
                        progressDialog.dismiss();
                        ArrayList<Type> types = response.getTypeList();
                        renderAll(types);
                    }
                    @Override
                    protected void onCancelled() {
                        progressDialog.dismiss();
                    }
                };
        task.execute();
    }


    private void renderAll( ArrayList<Type> types ){


        ArrayList<Map<String,String>> mapList = new ArrayList<>();

        for(Type typpp : types){
            HashMap<String,String> map = new HashMap<>();
            map.put("no", typpp.getNo());
            map.put("name", typpp.getName());
            map.put("description", typpp.getDescription());
            map.put("image", typpp.getImage());
            map.put("childs", typpp.getChilds() +"");
            map.put("selected", typpp.getSelectable().toString());
            mapList.add(map);
        }
        allAdapter = new SingleTreeAdapter(this,  mapList );

        allAdapter.setChangedHandler(new SingleTreeAdapter.Changed() {
            @Override
            public void itemSelected(Map<String, String> one) {
                for (KeyValue kv : mAdapter.getSelectionKeyValue().getContent() ){
                    if (kv.getNo().equals( one.get("no"))){
                        return;
                    }
                }
                if (mAdapter.getSelectionKeyValue().getContent().size() >=6 ){
                    Common.alert(FilterType.this,"最多选择6个");
                    return;
                }

                mAdapter.AddNewOne( one) ;
                mAdapter.notifyDataSetChanged();
                changed = true;
            }


            @Override
            public void itemGetChildren(Map<String, String> one) {

                parents.put(one.get("name"), curType);
                curType = one.get("name");
                typeTitleTxt.setText(curType);

                loadAllData();
            }
        });


        this.alllist.setAdapter(allAdapter);



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (parents.get(curType) != null) {

                curType = parents.get(curType) ;
                typeTitleTxt.setText(curType);
                loadAllData();
                return false;
            }else {
                Intent intent = new Intent();
                setResult(0, intent);
                finish();
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}



