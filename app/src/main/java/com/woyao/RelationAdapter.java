package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.RelationSummary;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class RelationAdapter extends RecyclerView.Adapter<RelationAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private  List<RelationSummary> thedata = new ArrayList<RelationSummary>();
    private Changed onChanged;

    private String Selection = null ;
    Context thecontext;

    public RelationAdapter(Context context, List<RelationSummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata= data;

    }


    public interface Changed{
        void view( RelationSummary r, View v);
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.relation_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final RelationSummary curData =  thedata.get(position);

        holder.theTitle.setText( curData.getTitle() );

        if (curData.getAccess_right().equals("private")){
            holder.theTitle.setTextColor(thecontext.getResources().getColor(R.color.colorTextSecondary) );
        }else{
            holder.theTitle.setTextColor(thecontext.getResources().getColor(R.color.colorTextDarkGrey) );
        }

        holder.theOrder.setText(  (position +1) +"") ;

        holder.theSubtitle.setText( curData.getDescription() );

        if (curData.getMessage_num() >0 ) {
            holder.theMsg.setVisibility(View.VISIBLE);
            holder.theMsg.setText(curData.getMessage_num() + "");
        }else{
            holder.theMsg.setVisibility(View.INVISIBLE);
        }

        holder.theImage.setImageResource(R.drawable.no_avartar);
        if (StringUtil.notNullOrEmpty(curData.getSnailview())) {
//            ImageLoader.load(holder.theImage, curData.getSnailview());
            Picasso.with(thecontext)
                    .load(curData.getSnailview())
                    .into(holder.theImage);
        }

        holder.theContent.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                RelationSummary curdata = thedata.get(holder.getAdapterPosition());
                curData.setMessage_num( 0 );
                thedata.set(holder.getAdapterPosition(),curData );
                holder.theMsg.setVisibility(View.INVISIBLE);
                onChanged.view(curdata ,v );
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theTitle;
        public TextView theSubtitle;
        public LinearLayout theContent;
        public CircleImageView theImage;
        public Button theMsg;
        public TextView theOrder;

        public ViewHolder(View view) {
            super(view);
            theTitle = (TextView) view.findViewById(R.id.chance_title);
            theSubtitle = (TextView) view.findViewById(R.id.chance_subtext);
            theContent = (LinearLayout) view.findViewById(R.id.chance_content);
            theImage = (CircleImageView) view.findViewById(R.id.chance_image);
            theMsg  = (Button)  view.findViewById(R.id.chance_msg);
            theOrder  = (TextView)  view.findViewById(R.id.chance_order);
        }

    }

}
