package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.woyao.core.model.GetMyMarkResponse;
import com.woyao.core.model.MarkSummary;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.woyao.WoyaoooApplication.userId;

public class MyUpdatesActivity extends AppCompatActivity {

    private List<MarkSummary> items = new ArrayList<MarkSummary>();
    private MarkAdapter allAdapter;
    private RecyclerView alllist = null;
    LinearLayoutManager allLayoutManager = null;
    String content = "";

    ProgressDialog progressDialog;

    Button addUpdate;
    private Integer ADD_UPDATE_CODE = 3000;
    private Integer VIEW_UPDATE_CODE = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout



        alllist = (RecyclerView)findViewById(R.id.items);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        addUpdate = (Button) findViewById(R.id.update_add);
        addUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent();
                intent.putExtra("id",0);
                intent.setClass(MyUpdatesActivity.this, MoveActivity.class);

                startActivityForResult(intent, ADD_UPDATE_CODE);
            }
        });

        loadData( );

        this.registerForContextMenu(alllist);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        finish();
        return true;
    }


    private AsyncTask<Void,Void, GetMyMarkResponse> task = null;
    private void loadData( ){
        if(task != null)return;

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("正在获取数据······");
        progressDialog.show();

        task =new AsyncTask<Void, Void, GetMyMarkResponse>() {
            @Override
            protected GetMyMarkResponse doInBackground(Void... params) {
                AccountService svc = ServiceFactory.get(AccountService.class);
                Call<GetMyMarkResponse> responseCall = svc.getMyMoves(userId);
                try {
                    GetMyMarkResponse response = responseCall.execute().body();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(final GetMyMarkResponse response) {
                progressDialog.dismiss();
                render(response);
                task = null;
            }
            @Override
            protected void onCancelled() {
                progressDialog.dismiss();
            }
        };
        task.execute((Void)null);
    }
    private void render(GetMyMarkResponse response){

        items = response.getContent();
        this.setTitle( "我的动态：" + items.size() );


        allAdapter = new MarkAdapter(this,  items );

        allAdapter.setChangedHandler(new MarkAdapter.Changed() {
            @Override
            public void view(MarkSummary r) {
                getIt(r.getId() );
            }
        });



        alllist.setAdapter(allAdapter);

    }

    private void getIt( final Integer curid ){
        Intent intent = new Intent();
        intent.setClass(MyUpdatesActivity.this, MoveActivity.class);
        intent.putExtra("id", curid  );
        startActivityForResult(intent, VIEW_UPDATE_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == ADD_UPDATE_CODE  ||  requestCode == VIEW_UPDATE_CODE)   && resultCode == 666 ) {

            loadData();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}

