package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.TextView;

import com.woyao.core.model.GetMyUserResponse;
import com.woyao.core.model.PartnerList;
import com.woyao.core.model.PartnerSummary;
import com.woyao.core.model.UserSummary;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.woyao.WoyaoooApplication.userId;

public class ConfirmUserActivity extends AppCompatActivity {
    ProgressDialog progressDialog;

    private SearchView mSearchView;
    private List<UserSummary> userItems = new ArrayList<UserSummary>();
    private UserSelectAdapter userAdapter;
    private RecyclerView userList = null;
    LinearLayoutManager userLayoutManager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.invite_toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout


        userList = (RecyclerView)findViewById(R.id.user_items);
        userLayoutManager = new LinearLayoutManager(ConfirmUserActivity.this);
        userLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        userList.setLayoutManager(userLayoutManager);
        userList.addItemDecoration(new DividerItemDecoration(ConfirmUserActivity.this, DividerItemDecoration.VERTICAL_LIST));


        this.setTitle("选择合作伙伴");

        mSearchView = (SearchView) findViewById(R.id.user_search);
//        mSearchView.setVisibility(View.GONE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadUser(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        loadUser("");
    }




    public void loadUser( final String kw){

        AsyncTask<Void,Void, GetMyUserResponse> task1 =new AsyncTask<Void, Void, GetMyUserResponse>() {
            @Override
            protected GetMyUserResponse doInBackground(Void... params) {
                AccountService svc = ServiceFactory.get(AccountService.class);
                Call<GetMyUserResponse> responseCall = svc.getMyUser(userId,kw);
                try {
                    GetMyUserResponse response = responseCall.execute().body();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(final GetMyUserResponse response) {

                renderUser(response);

            }
            @Override
            protected void onCancelled() {

            }
        };
        task1.execute((Void)null);
    }
    private void renderUser(GetMyUserResponse response){

        userItems = response.getContent().getItems();
        TextView userSummary = findViewById(R.id.user_summary);
        userSummary.setText(response.getContent().getTitle());

        userAdapter = new UserSelectAdapter(ConfirmUserActivity.this, userItems  );

        userAdapter.setChangedHandler(new UserSelectAdapter.Changed() {
            @Override
            public void view(UserSummary cs) {
                Intent intent = new Intent();
                PartnerList kvl = new PartnerList();
                ArrayList<PartnerSummary> partners = new ArrayList<PartnerSummary>();

                PartnerSummary ps = new PartnerSummary();
                ps.setDemand_id( cs.getDemand_id());
                ps.setTitle( cs.getTitle());
                ps.setUser_id(cs.getUser_id());
                ps.setMobile("");
                ps.setDisplayname(cs.getTitle());
                ps.setImage(cs.getSnailview());
                ps.setSnailview(cs.getSnailview());
                ps.setDescription(cs.getDescription());
                partners.add(ps);

                kvl.setContent(partners );
                intent.putExtra("partners", kvl  );
                setResult(666, intent);

                finish();

            }


        });

        userList.setAdapter(userAdapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        setResult(0, intent);
        finish();
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
