package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetChildTypeResponse extends BaseResponse implements Serializable {
    @JsonProperty("content")
    private ArrayList<Type> typeList;

    public ArrayList<Type> getTypeList() {
        return typeList;
    }

    public void setTypeList(ArrayList<Type> howList) {
        this.typeList = howList;
    }


}
