package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/10/13 0013.
 */
public class Category extends BaseResponse implements Serializable {
    private String no ="";
    private String name ="";
    private String description ="";

    private String image="";
    private int childs;

    private Boolean selectable = true;

    public void setNo(String no) {
        this.no = no;
    }
    public String getNo() {
        return no;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }
    public int getChilds() {
        return childs;
    }

    public Boolean getSelectable() {
        return selectable;
    }
}
