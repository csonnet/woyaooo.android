package com.woyao.core.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.woyao.core.model.BaseResponse;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetBusinessResponse extends BaseResponse implements Serializable {
    private BusinessCategory content;

    public BusinessCategory getContent() {
        return content;
    }

    public void setContent(BusinessCategory content) {
        this.content = content;
    }


}