package com.woyao.core.model;

import com.woyao.core.util.Utils;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-28.
 */
public class Appointment  implements Serializable {
    private String id = "0";
    private String mode = "phone";
    private String date = "";
    private String date_day = "";
    private String date_min = "00:00";
    private String time ="";
    private String address ="";
    private String contact = "";
    private String message = "";
    private String status = "new";
    private String title = "";

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        try {
            date_day = date.substring(0, 10);
            date_min = date.substring(11);
        }catch (Exception e){

        }
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDate_day() {
        return date_day;
    }



    public void setDate_day(String date_day) {
        this.date_day = date_day;
        this.date =  date_day + " " + date_min ;
    }

    public String getDate_min() {
        return date_min;
    }

    public void setDate_min(String date_min) {
        this.date_min = date_min;
        this.date =  date_day + " " + date_min ;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription(){
        String desc = "";
        desc += "沟通方式："+ Utils.getMode( mode )+ "\n";
        desc += "联系方式："+ contact + "\n" ;
        desc += "沟通日期："+ date + "\n";
        desc += "沟通时长："+ time + "分钟\n" ;
        desc += "沟通地址："+ address+ "\n";
        desc += message ;
        return desc;
    }
}
