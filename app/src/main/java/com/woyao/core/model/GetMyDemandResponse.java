package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyDemandResponse extends BaseResponse {
    @JsonDeserialize(contentAs = DemandSummary.class)
    private List<DemandSummary> content;

    public List<DemandSummary> getContent() {
        return content;
    }

    public void setContent(List<DemandSummary> content) {
        this.content = content;
    }
}
