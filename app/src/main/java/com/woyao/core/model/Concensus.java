package com.woyao.core.model;

import android.text.BoringLayout;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Concensus implements Serializable {
    private Integer id= 0;
    private String md5= "";

    private String partners ="";
    private String description="";

    private String memo ="";
    private String act ="";
    private String act_text ="";

    private String agreement ="";
    private String right_duty ="";
    private String relation_right_duty ="";
    private Float duration = 1.0f;
    private String agreement_else ="";

    private String idcard ="";
    private String buzzcard ="";
    private String relation_idcard ="";
    private String relation_buzzcard ="";

    private String idcard_snail ="";
    private String buzzcard_snail ="";
    private String relation_idcard_snail ="";
    private String relation_buzzcard_snail ="";

    private Boolean changable = true;

    @JsonProperty("sign_date")
    private Date sign_date = new Date();

    @JsonProperty("relation_sign_date")
    private Date relation_sign_date = new Date();



    public Boolean getChangable() {
        return changable;
    }

    public void setChangable(Boolean changable) {
        this.changable = changable;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }


    public String getPartners() {
        return partners;
    }

    public void setPartners(String partners) {
        this.partners = partners;
    }


    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }


    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getAct_text() {
        return act_text;
    }

    public void setAct_text(String act_text) {
        this.act_text = act_text;
    }

    public String getAgreement_else() {
        return agreement_else;
    }

    public void setAgreement_else(String agreement_else) {
        this.agreement_else = agreement_else;
    }

    public String getRelation_right_duty() {
        return relation_right_duty;
    }

    public String getRight_duty() {
        return right_duty;
    }

    public void setRelation_right_duty(String relation_right_duty) {
        this.relation_right_duty = relation_right_duty;
    }

    public void setRight_duty(String right_duty) {
        this.right_duty = right_duty;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }


    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getBuzzcard() {
        return buzzcard;
    }

    public String getRelation_idcard() {
        return relation_idcard;
    }

    public void setRelation_idcard(String relation_idcard) {
        this.relation_idcard = relation_idcard;
    }

    public void setBuzzcard(String buzzcard) {
        this.buzzcard = buzzcard;
    }

    public String getRelation_buzzcard() {
        return relation_buzzcard;
    }

    public void setRelation_buzzcard(String relation_buzzcard) {
        this.relation_buzzcard = relation_buzzcard;
    }

    public Date getSign_date() {
        return sign_date;
    }

    public void setSign_date(Date sign_date) {
        this.sign_date = sign_date;
    }

    public Date getRelation_sign_date() {
        return relation_sign_date;
    }

    public void setRelation_sign_date(Date relation_sign_date) {
        this.relation_sign_date = relation_sign_date;
    }

    public String getBuzzcard_snail() {
        return buzzcard_snail;
    }

    public String getIdcard_snail() {
        return idcard_snail;
    }

    public String getRelation_buzzcard_snail() {
        return relation_buzzcard_snail;
    }

    public String getRelation_idcard_snail() {
        return relation_idcard_snail;
    }

    public void setBuzzcard_snail(String buzzcard_snail) {
        this.buzzcard_snail = buzzcard_snail;
    }

    public void setIdcard_snail(String idcard_snail) {
        this.idcard_snail = idcard_snail;
    }

    public void setRelation_buzzcard_snail(String relation_buzzcard_snail) {
        this.relation_buzzcard_snail = relation_buzzcard_snail;
    }

    public void setRelation_idcard_snail(String relation_idcard_snail) {
        this.relation_idcard_snail = relation_idcard_snail;
    }
}
