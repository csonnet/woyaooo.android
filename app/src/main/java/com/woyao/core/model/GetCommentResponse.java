package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetCommentResponse extends BaseResponse implements Serializable{
    private Comment content;

    public Comment getContent() {
        return content;
    }

    public void setContent(Comment content) {
        this.content = content;
    }
}
