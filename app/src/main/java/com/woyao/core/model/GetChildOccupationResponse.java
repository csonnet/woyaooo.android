package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetChildOccupationResponse extends BaseResponse implements Serializable {
    @JsonProperty("content")
    private ArrayList<KeyValue> content;

    public ArrayList<KeyValue> getContent() {
        return content;
    }

    public void setContent(ArrayList<KeyValue> content) {
        this.content = content;
    }
}
