package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class MemberSummary implements Serializable {
    private Integer id =0 ;
    private Integer org_id =0 ;
    private Integer user_id =0 ;
    private String title ="个人";
    private String description="";
    private String role="";
    private String status="";
    private String snailview="";
    private Integer message_num=0;
    private boolean current = false;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public Integer getMessage_num() {
        return message_num;
    }

    public void setMessage_num(Integer message_num) {
        this.message_num = message_num;
    }


    public boolean getCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public Integer getOrg_id() {
        return org_id;
    }

    public Integer getUser_id() {
        return user_id;
    }
}
