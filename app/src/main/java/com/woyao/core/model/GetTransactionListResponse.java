package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetTransactionListResponse extends BaseResponse {
    @JsonDeserialize(contentAs = TransactionOption.class)
    private List<TransactionOption> content;

    public List<TransactionOption> getContent() {
        return content;
    }

    public void setContent(List<TransactionOption> content) {
        this.content = content;
    }
}
