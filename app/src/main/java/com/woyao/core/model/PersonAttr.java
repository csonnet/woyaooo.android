package com.woyao.core.model;

/**
 * Copyright 2016 aTool.org
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Auto-generated: 2016-05-13 14:44:7
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class PersonAttr implements Serializable {

    private String id ="";
    private String displayname ="";
    private String snailview ="";

    private String location ="";
    private String intro ="";
    private String role ="";
    private String verify = "";

    @JsonProperty("attribs")
    private List<List<String>> attribs = new ArrayList<List<String>>();

    public String getDisplayname() {
        return displayname;
    }

    public String getId() {
        return id;
    }

    public String getSnailview() {
        return snailview;
    }

    public String getRole() {
        return role;
    }

    public String getLocation() {
        return location;
    }

    public List<List<String>> getAttribs() {
        return attribs;
    }

    public String getIntro() {
        return intro;
    }

    public String getVerify() {
        return verify;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public void setAttribs(List<List<String>> attribs) {
        this.attribs = attribs;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }
}