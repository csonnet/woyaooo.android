package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetSubjectsResponse extends BaseResponse {
    @JsonDeserialize(contentAs = MyTag.class)
    private List<MyTag> content;

    public List<MyTag> getContent() {
        return content;
    }

    public void setContent(List<MyTag> content) {
        this.content = content;
    }
}
