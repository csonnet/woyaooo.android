package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Member implements Serializable {
    private String caption = "";
    private Integer id;
    private Integer org_id;
    private String orgtitle = "";
    private String status = "";
    private String title  = "";
    private String description  = "";
    private String business = "" ;
    private String business_name = "" ;
    private Integer self_employed = 0 ;

    private Integer admin = 0 ;

    @JsonDeserialize(contentAs = Talk.class, as = ArrayList.class)
    private List<Talk> messages = new ArrayList<Talk>();

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrg_id() {
        return org_id;
    }

    public void setOrg_id(Integer org_id) {
        this.org_id = org_id;
    }

    public String getOrgtitle() {
        return orgtitle;
    }

    public void setOrgtitle(String orgtitle) {
        this.orgtitle = orgtitle;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Integer getSelf_employed() {
        return self_employed;
    }

    public void setSelf_employed(Integer self_employed) {
        this.self_employed = self_employed;
    }

    public List<Talk> getMessages() {
        return messages;
    }

    public Integer getAdmin() {
        return admin;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCaption() {
        return caption;
    }
}
