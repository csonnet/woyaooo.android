package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Org implements Serializable {
    private Integer id =0 ;
    private String title ="";
    private String members_summary = "";
    private String description ="";
    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  business = new ArrayList<KeyValue>();
    private String logo  ="";
    private String certification ="";
    private String address ="";
    private String website ="";
    private String type ="";
    private String type_name ="";
    private Float fee = 0.0f ;

    @JsonDeserialize(contentAs = MemberMgmtSummary.class)
    private List<MemberMgmtSummary> members;

    private Integer verified =0 ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<KeyValue>  getBusiness() {
        return business;
    }

    public void setBusiness(ArrayList<KeyValue>  business) {
        this.business = business;
    }


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public Integer getVerified() {
        return verified;
    }

    public Float getFee() {
        return fee;
    }

    public void setFee(Float fee) {
        this.fee = fee;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public List<MemberMgmtSummary> getMembers() {
        return members;
    }

    public String getMembers_summary() {
        return members_summary;
    }
}
