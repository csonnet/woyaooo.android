package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class SearchItem implements Serializable {
    private String title;
    private String description;
    private String snailview;
    private Integer id  =0 ;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
