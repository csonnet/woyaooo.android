package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyTagResponse extends BaseResponse {
    @JsonDeserialize(contentAs = Tag.class)
    private List<Tag> content;

    public List<Tag> getContent() {
        return content;
    }

    public void setContent(List<Tag> content) {
        this.content = content;
    }
}
