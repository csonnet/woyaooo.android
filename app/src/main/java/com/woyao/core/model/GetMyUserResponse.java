package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyUserResponse extends BaseResponse {
    @JsonDeserialize(contentAs = RelationSearch.class)
    private UserSearch content;

    public UserSearch getContent() {
        return content;
    }

    public void setContent(UserSearch content) {
        this.content = content;
    }
}
