package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class RelationSearch implements Serializable {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonDeserialize(contentAs = RelationSummary.class)
    private List<RelationSummary> items;

    @JsonDeserialize(contentAs = Tag.class)
    private List<Tag> statis = new ArrayList<Tag>();

    public List<RelationSummary> getItems() {
        return items;
    }

    public void setItems(List<RelationSummary> items) {
        this.items = items;
    }

    public List<Tag> getStatis() {
        return statis;
    }
}
