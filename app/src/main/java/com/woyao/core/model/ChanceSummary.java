package com.woyao.core.model;

/**
 * Copyright 2016 aTool.org
 */
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Auto-generated: 2016-05-13 14:44:7
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class ChanceSummary implements Serializable {

    private int id = 0;
    private int pid = 0;
    private int mid = 0;
    private int unread = 0;
    private String status = "match";
    private String object_status = "match";
    private int demand_id = 0 ;
    private String demand_title="选择匹配的业务";
    private int available = 1;
    private String image = "";
    private String video ="";
    private String title = "test haiming";
    private String description = "";
    private Date displaydate = new Date();
    private String apply_text ="合作";
    private String ignore_text ="放弃";
    private Boolean marked = false;
    private Integer message_num = 0;
    private String status_text ="合作";
    private String social ="查看 10 关注 1 想合作 1";
    private String message = "";
    private String matchness ="";
    private String value ="";

    @JsonProperty("manager")
    private Manager manager = new Manager();

    public int getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(int demand_id) {
        this.demand_id = demand_id;
    }

    public int getId() {
        return id;
    }

    public int getPid() {
        return pid;
    }


    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getObject_status() {
        return object_status;
    }

    public String getImage() {
        return image;
    }


    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Manager getManager() {
        return manager;
    }


    public Date getDisplaydate() {
        return displaydate;
    }

    public void setDisplaydate(Date displaydate) {
        this.displaydate = displaydate;
    }

    public int getAvailable() {
        return available;
    }


    public Boolean getMarked() {
        return marked;
    }

    public void setMarked(Boolean marked) {
        this.marked = marked;
    }


    public String getApply_text() {
        return apply_text;
    }

    public void setApply_text(String apply_text) {
        this.apply_text = apply_text;
    }

    public String getMessage() {
        return message;
    }

    public Integer getMessage_num() {
        return message_num;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setObject_status(String object_status) {
        this.object_status = object_status;
    }

    public int getMid() {
        return mid;
    }

    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    public String getDemand_title() {
        return demand_title;
    }

    public String getIgnore_text() {
        return ignore_text;
    }

    public String getSocial() {
        return social;
    }

    public String getMatchness() {
        return matchness;
    }

    public String getValue() {
        return value;
    }
}