package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetBalanceResponse extends BaseResponse {
    @JsonDeserialize(contentAs = Balance.class)
    private List<Balance> content;

    @JsonDeserialize(contentAs = Service.class)
    private List<Service> services;

    private Integer basic  =0 ;

   public List<Balance> getContent() {
        return content;
    }

    public void setContent(List<Balance> content) {
        this.content = content;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public Integer getBasic() {
        return basic;
    }
}
