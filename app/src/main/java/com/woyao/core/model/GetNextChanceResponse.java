package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-13.
 */
public class GetNextChanceResponse extends BaseResponse  implements Serializable {
    private Chance content;
    private Integer message_num =0 ;
    private String type = "" ;

    public void setMessage_num(Integer message_num) {
        this.message_num = message_num;
    }

    public Integer getMessage_num() {
        return message_num;
    }

    public Chance getContent() {
        return content;
    }

    public void setContent(Chance content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
