package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/10/13 0013.
 */
public class How implements Serializable {
    private String no ="";
    private String name ="";

    private String name_help ="";
    private String description ="";
    private String description_help ="";
    private String image="";
    private int childs = 0;
    private Boolean selectable = true;

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  type_list = new ArrayList<KeyValue>();

    private String name_caption ="";
    private String description_caption ="";
    private String type_caption="";
    private String  enduse_caption="";
    private String  specification_caption="";
    private String  processing_caption="";
    private String  durable_caption="";
    private String  types_caption="";
    private String  relation_caption="";
    private String  include_caption="";
    private String  requirement_caption="";
    private String  refer_fee_caption="";
    private String  intention_fee_caption="";
    private String  value_caption="";
    private String  quantity_caption="";

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  types_list = new ArrayList<KeyValue>();


    public void setNo(String no) {
        this.no = no;
    }
    public String getNo() {
        return no;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }
    public int getChilds() {
        return childs;
    }

    public Boolean getSelectable() {
        return selectable;
    }


    public String getType_caption() {
        return type_caption;
    }

    public String getEnduse_caption() {
        return enduse_caption;
    }

    public String getProcessing_caption() {
        return processing_caption;
    }

    public String getSpecification_caption() {
        return specification_caption;
    }

    public String getDurable_caption() {
        return durable_caption;
    }

    public ArrayList<KeyValue> getTypes_list() {
        return types_list;
    }

    public String getDescription_help() {
        return description_help;
    }

    public String getTypes_caption() {
        return types_caption;
    }

    public String getDescription_caption() {
        return description_caption;
    }

    public String getName_caption() {
        return name_caption;
    }

    public String getName_help() {
        return name_help;
    }

    public String getRequirement_caption() {
        return requirement_caption;
    }

    public String getInclude_caption() {
        return include_caption;
    }

    public String getIntention_fee_caption() {
        return intention_fee_caption;
    }

    public String getRefer_fee_caption() {
        return refer_fee_caption;
    }

    public String getRelation_caption() {
        return relation_caption;
    }

    public ArrayList<KeyValue> getType_list() {
        return type_list;
    }

    public String getQuantity_caption() {
        return quantity_caption;
    }

    public String getValue_caption() {
        return value_caption;
    }
}
