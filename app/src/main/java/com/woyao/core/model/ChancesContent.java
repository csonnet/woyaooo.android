package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-13.
 */
public class ChancesContent implements Serializable {
    private String description = "" ;
    private String caption = "我要合作网" ;
    @JsonDeserialize(contentAs = ChanceSummary.class)
    private ArrayList<ChanceSummary> items;

    public ArrayList<ChanceSummary> getItems() {
        return items;
    }

    public String getCaption() {
        return caption;
    }

    public String getDescription() {
        return description;
    }
}
