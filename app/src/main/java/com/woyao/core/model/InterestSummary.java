package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-31.
 */
public class InterestSummary implements Serializable {
    private Integer id =0 ;
    private Integer user_id =0 ;
    private Integer demand_id =0 ;
    private String snailview="";
    private String  displayname ="个人";
    private String  member_title ="个人";
    private String description="";

    private Integer relation_id =0 ;
    private String  relation_type ="demand";
    private String title ="";  // demand/org title
    private String created="";
    private Integer positive =0 ;
    private Integer negative =0 ;
    private Boolean voted = false;

    @JsonDeserialize(contentAs = String.class, as = ArrayList.class)
    private ArrayList<String>  subject = new ArrayList<String>();

    @JsonDeserialize(contentAs = MediaSummary.class, as = ArrayList.class)
    private ArrayList<MediaSummary>  medias = new ArrayList<MediaSummary>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }


    public Integer getUser_id() {
        return user_id;
    }

    public ArrayList<MediaSummary> getMedias() {
        return medias;
    }

    public ArrayList<String> getSubject() {
        return subject;
    }

    public String getRelation_type() {
        return relation_type;
    }

    public String getDisplayname() {
        return displayname;
    }

    public Integer getRelation_id() {
        return relation_id;
    }

    public Boolean getVoted() {
        return voted;
    }

    public void setVoted(Boolean voted) {
        this.voted = voted;
    }

    public Integer getNegative() {
        return negative;
    }

    public Integer getPositive() {
        return positive;
    }

    public String getCreated() {
        return created;
    }

    public void setPositive(Integer positive) {
        this.positive = positive;
    }

    public void setNegative(Integer negative) {
        this.negative = negative;
    }

    public String getMember_title() {
        return member_title;
    }

    public Integer getDemand_id() {
        return demand_id;
    }

}
