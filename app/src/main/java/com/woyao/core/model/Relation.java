package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Relation implements Serializable {

    private String caption ="";
    private Integer id= 0;    //partner_id
    private Integer mid= 0;
    private String image="";  // no usage
    private Integer demand_id = 0; // my member id
    private String demand_title = "";
    private Integer member_id = 0; // my member id
    private String member_title = "";
    private Integer confirmed = 1; // my member id
    private String status ="ongoing";   // applied or not
    private String relation_status ="";
    private String match_status ="";   // applied or not
    private String memo="";
    private String title="";
    private String description="";
    private String how="";
    private String how_name="";
    private String begin_date="";
    private String end_date="";
    private Boolean commentable = true;
    private Boolean talkable = true;
    private Boolean addable = true;

    private String access_right = "public";


    @JsonDeserialize(contentAs = PartnerSummary.class, as = ArrayList.class)
    private List<PartnerSummary> partners = new ArrayList<PartnerSummary>();

    @JsonDeserialize(contentAs = Talk.class, as = ArrayList.class)
    private List<Talk> messages = new ArrayList<Talk>();

    @JsonDeserialize(contentAs = CommentSummary.class, as = ArrayList.class)
    private List<CommentSummary> comments  = new ArrayList<CommentSummary>();


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHow() {
        return how;
    }

    public void setHow(String how) {
        this.how = how;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getMember_title() {
        return member_title;
    }

    public void setMember_title(String member_title) {
        this.member_title = member_title;
    }

    public String getHow_name() {
        return how_name;
    }

    public void setHow_name(String how_name) {
        this.how_name = how_name;
    }

    public Integer getMember_id() {
        return member_id;
    }

    public void setMember_id(Integer member_id) {
        this.member_id = member_id;
    }

    public String getAccess_right() {
        return access_right;
    }

    public void setAccess_right(String access_right) {
        this.access_right = access_right;
    }

    public List<CommentSummary> getComments() {
        return comments;
    }

    public void setComments(List<CommentSummary> comments) {
        this.comments = comments;
    }

    public List<Talk> getMessages() {
        return messages;
    }

    public void setMessages(List<Talk> messages) {
        this.messages = messages;
    }

    public Integer getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(Integer demand_id) {
        this.demand_id = demand_id;
    }

    public String getDemand_title() {
        return demand_title;
    }

    public void setDemand_title(String demand_title) {
        this.demand_title = demand_title;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }


    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }



    public List<PartnerSummary> getPartners() {
        return partners;
    }

    public void setPartners(List<PartnerSummary> partners) {
        this.partners = partners;
    }

    public Boolean getCommentable() {
        return commentable;
    }

    public Boolean getTalkable() {
        return talkable;
    }

    public Boolean getAddable() {
        return addable;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getMatch_status() {
        return match_status;
    }


    public String getRelation_status() {
        return relation_status;
    }
}
