package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class UserSearch implements Serializable {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonDeserialize(contentAs = UserSummary.class)
    private List<UserSummary> items;

    public List<UserSummary> getItems() {
        return items;
    }

    public void setItems(List<UserSummary> items) {
        this.items = items;
    }

}
