package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class BusinessCategory implements Serializable {
    @JsonProperty("value")
    private Integer count;
    @JsonProperty("description")
    private String description;
    @JsonProperty("no")
    private String no;
    @JsonProperty("image")
    private String image;
    @JsonProperty("top")
    private String parentNo;
    @JsonProperty("name")
    private String name;
    @JsonProperty("childs")
    private Integer childs;

    @JsonProperty("how")
    private ArrayList<String> how;

    @JsonProperty("develop")
    private ArrayList<String> develop;

    @JsonProperty("operate")
    private ArrayList<String> operate;

    private String originName;

    private ArrayList<BusinessCategory> children;
    private BusinessCategory parent;

    public ArrayList<String> getHow() {
        return how;
    }

    public void setHow(ArrayList<String> thehow) {
        this.how = thehow;
    }

    public ArrayList<String> getDevelop() {
        return develop;
    }

    public void setDevelop(ArrayList<String> thedevelop) {
        this.develop = thedevelop;
    }

    public ArrayList<String> getOperate() {
        return operate;
    }

    public void setOperate(ArrayList<String> theoperate) {
        this.operate = theoperate;
    }




    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getParentNo() {
        return parentNo;
    }

    public void setParentNo(String parentNo) {
        this.parentNo = parentNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<BusinessCategory> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<BusinessCategory> children) {
        this.children = children;
    }

    public BusinessCategory getParent() {
        return parent;
    }

    public void setParent(BusinessCategory parent) {
        this.parent = parent;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public Integer getChilds() {
        return childs;
    }

    public void setChilds(Integer thecount) {
        this.childs = thecount;
    }

}
