package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-13.
 */
public class GetOrgResponse extends BaseResponse  implements Serializable {
    private Org org;

    public Org getContent() {
        return org;
    }

    public void setContent(Org content) {
        this.org = content;
    }
}
