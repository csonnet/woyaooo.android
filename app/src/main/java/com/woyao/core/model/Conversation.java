package com.woyao.core.model;

/**
 * Copyright 2016 aTool.org
 */
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Auto-generated: 2016-05-13 14:44:7
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Conversation implements Serializable {


    @JsonDeserialize(contentAs = PartnerSummary.class, as = ArrayList.class)
    private List<PartnerSummary> participants;

    @JsonDeserialize(contentAs = Talk.class, as = ArrayList.class)
    private List<Talk> talks = new ArrayList<Talk>();
    private Integer id = 0;  //tid
    private Integer mid = 0;  //mid
    private Integer relation_user_id = 0;
    private String title = "";
    private String description = "";
    private String type = "";
    private String status = "";
    private String user_status = "match";
    private Integer partner_id = 0;

    private Integer relation_demand_id = 0;

    private Boolean showaction = false;


    public Boolean getShowaction() {
        return showaction;
    }

    public Integer getRelation_demand_id() {
        return relation_demand_id;
    }

    public void setRelation_demand_id(Integer relation_demand_id) {
        this.relation_demand_id = relation_demand_id;
    }

    public List<Talk> getTalks() {
        return talks;
    }

    public void setTalks(List<Talk> talks) {
        this.talks = talks;
    }


    public List<PartnerSummary> getParticipants() {
        return participants;
    }

    public void setParticipants(List<PartnerSummary> participants) {
        this.participants = participants;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRelation_user_id() {
        return relation_user_id;
    }

    public Integer getMid() {
        return mid;
    }

    public String getType() {
        return type;
    }
}