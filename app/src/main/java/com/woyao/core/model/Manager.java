package com.woyao.core.model;

/**
 * Copyright 2016 aTool.org
 */
import java.io.Serializable;

/**
 * Auto-generated: 2016-05-13 14:44:7
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Manager implements Serializable {

    private int id = 0;

    private String snailview = "";
    private String displayname ="";
    private String location = "";
    private String role = "";

    public int getId() {
        return id;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getLocation() {
        return location;
    }

    public String getRole() {
        return role;
    }

    public String getSnailview() {
        return snailview;
    }
}