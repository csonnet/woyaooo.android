package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-05-13.
 */
public class GetOrganizationResponse extends BaseResponse   {
    @JsonDeserialize(contentAs = Organization.class)
    private Organization org;

    public Organization getContent() {
        return org;
    }

    public void setContent(Organization content) {
        this.org = content;
    }
}
