package com.woyao.core.model;

/**
 * Copyright 2016 aTool.org
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Auto-generated: 2016-05-13 14:44:7
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Person implements Serializable {

    private int id =0;
    private String image ="";
    private String title ="";
    private String description ="";

    private String members_summary ="";
    private String partners_summary ="";
    private String demands_summary ="";
    private String agreements_summary = "";

    @JsonProperty("moves")
    @JsonDeserialize(contentAs = InterestSummary.class, as = ArrayList.class)
    private List<InterestSummary> moves = new ArrayList<InterestSummary>();

    private String moves_summary = "";

    @JsonProperty("agreements")
    @JsonDeserialize(contentAs = Agreement.class, as = ArrayList.class)
    private List<Agreement> agreements = new ArrayList<Agreement>();

    private List< Map<String,String>> attrs;

    @JsonDeserialize(contentAs = NewItemSummary.class, as = ArrayList.class)
    private List<NewItemSummary> members;

    @JsonDeserialize(contentAs = PartnerComment.class, as = ArrayList.class)
    private List<PartnerComment> partners;

    @JsonDeserialize(contentAs = NewItemSummary.class, as = ArrayList.class)
    private List<NewItemSummary> demands;

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Map<String, String>> getAttrs() {
        return attrs;
    }

    public void setAttrs(List<Map<String, String>> attrs) {
        this.attrs = attrs;
    }

    public List<NewItemSummary> getMembers() {
        return members;
    }

    public void setMembers(List<NewItemSummary> members) {
        this.members = members;
    }

    public List<PartnerComment> getPartners() {
        return partners;
    }

    public void setPartners(List<PartnerComment> partners) {
        this.partners = partners;
    }

    public List<NewItemSummary> getDemands() {
        return demands;
    }

    public void setDemands(List<NewItemSummary> demands) {
        this.demands = demands;
    }

    public String getPartners_summary() {
        return partners_summary;
    }

    public void setPartners_summary(String partners_summary) {
        this.partners_summary = partners_summary;
    }

    public String getMembers_summary() {
        return members_summary;
    }

    public void setMembers_summary(String members_summary) {
        this.members_summary = members_summary;
    }

    public String getDemands_summary() {
        return demands_summary;
    }

    public void setDemands_summary(String demands_summary) {
        this.demands_summary = demands_summary;
    }


    public List<Agreement> getAgreements() {
        return agreements;
    }

    public void setAgreements(List<Agreement> agreements) {
        this.agreements = agreements;
    }

    public String getAgreements_summary() {
        return agreements_summary;
    }

    public void setAgreements_summary(String agreements_summary) {
        this.agreements_summary = agreements_summary;
    }

    public String getMoves_summary() {
        return moves_summary;
    }

    public void setMoves_summary(String moves_summary) {
        this.moves_summary = moves_summary;
    }

    public List<InterestSummary> getMoves() {
        return moves;
    }

    public void setMoves(List<InterestSummary> moves) {
        this.moves = moves;
    }
}