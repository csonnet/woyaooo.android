package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Charge implements Serializable {

    private Integer id = 0;
    private String title="";
    private String description="";
    private String image ="";

    @JsonProperty("services")
    @JsonDeserialize(contentAs = ChargeChoice.class, as = ArrayList.class)
    private List<ChargeChoice> services = new ArrayList<ChargeChoice>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ChargeChoice> getServices() {
        return services;
    }

    public void setServices(List<ChargeChoice> services) {
        this.services = services;
    }
}
