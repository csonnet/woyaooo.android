package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-28.
 */
public class Comment implements Serializable {

    private String title = "";
    private String description= "";
    private Integer id = 0;
    private Integer user_id = 0;
    private Integer mid = 0;
    private Integer partner_id = 0;
    private Float charge = 0.0f;
    private Integer need_permit = 1;
    private Boolean editable =false;
    private Integer rate= 0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Float getCharge() {
        return charge;
    }

    public void setCharge(Float charge) {
        this.charge = charge;
    }

    public Integer getNeed_permit() {
        return need_permit;
    }

    public void setNeed_permit(Integer need_permit) {
        this.need_permit = need_permit;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Integer getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }
}
