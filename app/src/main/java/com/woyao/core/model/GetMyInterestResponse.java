package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyInterestResponse extends BaseResponse {
    @JsonDeserialize(contentAs = InterestSummary.class)
    private List<InterestSummary> content;

    public List<InterestSummary> getContent() {
        return content;
    }

    public void setContent(List<InterestSummary> content) {
        this.content = content;
    }
}
