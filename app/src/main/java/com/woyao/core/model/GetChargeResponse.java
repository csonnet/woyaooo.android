package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetChargeResponse extends BaseResponse implements Serializable{
    private Charge content;

    public Charge getContent() {
        return content;
    }

    public void setContent(Charge content) {
        this.content = content;
    }
}
