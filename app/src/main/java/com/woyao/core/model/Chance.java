package com.woyao.core.model;

/**
 * Copyright 2016 aTool.org
 */
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Auto-generated: 2016-05-13 14:44:7
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Chance implements Serializable {

    private int id = 0;
    private int pid = 0;
    private int context = 0;
    private String prompt = "";
    private String caption = "合作机会";
    private String status = "match";
    private String status_text= "合作";
    private String object_status = "match";
    private int demand_id = 0 ;
    private int available = 1;



    private String category="";
    private String type = "";
    private String image = "";
    private String title = "";
    private String resourcex = "";
    private String enduse = "";
    private String processing = "";
    private String specification = "";
    private String durable="";

    private String types="";
    private String requirement = "";
    private String description = "";
    private int intention_fee =0;
    private Float refer_fee =0.0f;


    private String relation = "";
    private String how = "";
    private String xhow = "";
    private String xhow_name = "";
    private String include = "";
    private int mid =0;
    private int order =0;
    private int total =0;
    private Integer unread =0;

    private int budget=0;
    private int charge=0;
    private int bidding=0;
    @JsonProperty("publish")
    private Date publish = new Date();

    private String displaydate = "";
    private Integer commentable = 1;
    private String refer_text ="介绍￥";
    private String apply_text ="合作";
    private Integer message_num = 0;

    private Boolean hide_refer = false;

    private Boolean marked = false;


    private List<String> pics = new ArrayList<String>();

    @JsonProperty("explan")
    @JsonDeserialize(contentAs = ItemSummary.class)
    private ItemSummary explan = new ItemSummary();

    @JsonProperty("attention")
    @JsonDeserialize(contentAs = ItemSummary.class)
    private ItemSummary attention = new ItemSummary();

    @JsonProperty("created")
    private Date created = new Date();

    @JsonProperty("attribs")
    private List<List<String>> attribs = new ArrayList<List<String>>();

    @JsonProperty("resource")
    private Map<String,String> resource = new HashMap<String, String>();

    @JsonProperty("relations")
    private List<HashMap<String, String>> relations = new ArrayList<HashMap<String, String>>() ;

    @JsonProperty("org")
    private Map<String,String> org = new HashMap<String, String>();

    @JsonProperty("manager")
    @JsonDeserialize(contentAs = PersonAttr.class)
    private PersonAttr manager = new PersonAttr();

    @JsonProperty("partners")
    @JsonDeserialize(contentAs = PartnerComment.class, as = ArrayList.class)
    private List<PartnerComment> partners = new ArrayList<PartnerComment>();

    @JsonProperty("moves")
    @JsonDeserialize(contentAs = InterestSummary.class, as = ArrayList.class)
    private List<InterestSummary> moves = new ArrayList<InterestSummary>();

    private String moves_summary = "";

    @JsonProperty("related")
    @JsonDeserialize(contentAs = RelatedChance.class, as = ArrayList.class)
    private List<RelatedChance> related = new ArrayList<RelatedChance>();

    private String related_summary = "";

    @JsonProperty("agreements")
    @JsonDeserialize(contentAs = Agreement.class, as = ArrayList.class)
    private List<Agreement> agreements = new ArrayList<Agreement>();

    @JsonProperty("comments")
    @JsonDeserialize(contentAs = Comment.class, as = ArrayList.class)
    private List<Comment> comments = new ArrayList<Comment>();


    private String agreements_summary = "";

    private String partners_summary = "";

    private String comments_summary = "";

    @JsonProperty("refers")
    @JsonDeserialize(contentAs = Comment.class, as = ArrayList.class)
    private List<Comment> refers= new ArrayList<Comment>();

    public int getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(int demand_id) {
        this.demand_id = demand_id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public void setContext(int context) {
        this.context =  context;
    }
    public int getContext() {
        return context;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }
    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }


    public String getObject_status() {
        return object_status;
    }
    public void setObject_status(String object_status) {
        this.object_status = object_status;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String getImage() {
        return image;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }



    public void setRelation(String relation) {
        this.relation = relation;
    }
    public String getRelation() {
        return relation;
    }

    public void setHow(String how) {
        this.how = how;
    }
    public String getHow() {
        return how;
    }


    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
    }

    public PersonAttr getManager() {
        return manager;
    }

    public Map<String,String> getResource() {
        return resource;
    }

    public void setResource(Map<String,String> resource) {
        this.resource = resource;
    }

    public Map<String,String> getOrg() {
        return org;
    }

    public void setOrg(Map<String,String> org) {
        this.org = org;
    }



    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Comment> getRefers() {
        return refers;
    }

    public void setRefers(List<Comment> refers) {
        this.refers = refers;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }
    public int getMid() {
        return mid;
    }


    public void setBudget(int budget) {
        this.budget = budget;
    }
    public int getBudget() {
        return budget;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }
    public int getCharge() {
        return charge;
    }

    public void setBidding(int bidding) {
        this.bidding = bidding;
    }
    public int getBidding() {
        return bidding;
    }



    public void setPics(List<String> pics) {
        this.pics = pics;
    }
    public List<String> getPics() {
        return pics;
    }

    public void setPublish(Date publish) {
        this.publish = publish;
    }
    public Date getPublish() {
        return publish;
    }

    public String getDisplaydate() {
        return displaydate;
    }

    public void setDisplaydate(String displaydate) {
        this.displaydate = displaydate;
    }

    public void setExplan(ItemSummary explan) {
        this.explan = explan;
    }
    public ItemSummary getExplan() {
        return explan;
    }

    public void setAttention(ItemSummary attention) {
        this.attention = attention;
    }
    public ItemSummary getAttention() {
        return attention;
    }


    public void setCreated(Date created) {
        this.created = created;
    }
    public Date getCreated() {
        return created;
    }

    public void setOrder(int order) {
        this.order = order;
    }
    public int getOrder() {
        return order;
    }


    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }

    public String getXhow() {
        return xhow;
    }

    public void setXhow(String xhow) {
        this.xhow = xhow;
    }

    public String getXhow_name() {
        return xhow_name;
    }

    public void setXhow_name(String xhow_name) {
        this.xhow_name = xhow_name;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getIntention_fee() {
        return intention_fee;
    }

    public void setIntention_fee(int intention_fee) {
        this.intention_fee = intention_fee;
    }

    public String getResourcex() {
        return resourcex;
    }

    public void setResourcex(String resourcex) {
        this.resourcex = resourcex;
    }

    public String getComments_summary() {
        return comments_summary;
    }

    public void setComments_summary(String comments_summary) {
        this.comments_summary = comments_summary;
    }

    public Integer getCommentable() {
        return commentable;
    }

    public void setCommentable(Integer commentable) {
        this.commentable = commentable;
    }

    public Float getRefer_fee() {
        return refer_fee;
    }

    public void setRefer_fee(Float refer_fee) {
        this.refer_fee = refer_fee;
    }

    public String getPartners_summary() {
        return partners_summary;
    }

    public void setPartners_summary(String partners_summary) {
        this.partners_summary = partners_summary;
    }

    public List<PartnerComment> getPartners() {
        return partners;
    }

    public void setPartners(List<PartnerComment> partners) {
        this.partners = partners;
    }

    public String getRefer_text() {
        return refer_text;
    }

    public void setRefer_text(String refer_text) {
        this.refer_text = refer_text;
    }

    public Integer getMessage_num() {
        return message_num;
    }

    public void setMessage_num(Integer message_num) {
        this.message_num = message_num;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public Boolean getMarked() {
        return marked;
    }

    public void setMarked(Boolean marked) {
        this.marked = marked;
    }

    public List<HashMap<String, String>> getRelations() {
        return relations;
    }

    public void setRelations(List<HashMap<String, String>> relations) {
        this.relations = relations;
    }

    public Integer getUnread() {
        return unread;
    }

    public void setUnread(Integer unread) {
        this.unread = unread;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public List<Agreement> getAgreements() {
        return agreements;
    }

    public void setAgreements(List<Agreement> agreements) {
        this.agreements = agreements;
    }

    public String getAgreements_summary() {
        return agreements_summary;
    }

    public void setAgreements_summary(String agreements_summary) {
        this.agreements_summary = agreements_summary;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getApply_text() {
        return apply_text;
    }

    public void setApply_text(String apply_text) {
        this.apply_text = apply_text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProcessing() {
        return processing;
    }

    public String getEnduse() {
        return enduse;
    }


    public String getSpecification() {
        return specification;
    }

    public String getCategory() {
        return category;
    }

    public String getDurable() {
        return durable;
    }

    public String getTypes() {
        return types;
    }

    public Boolean getHide_refer() {
        return hide_refer;
    }

    public List<List<String>> getAttribs() {
        return attribs;
    }

    public List<InterestSummary> getMoves() {
        return moves;
    }

    public String getMoves_summary() {
        return moves_summary;
    }

    public List<RelatedChance> getRelated() {
        return related;
    }

    public String getRelated_summary() {
        return related_summary;
    }
}