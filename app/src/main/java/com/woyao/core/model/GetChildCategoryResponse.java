package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetChildCategoryResponse extends BaseResponse implements Serializable {
    @JsonProperty("content")
    private ArrayList<Category> categoryList;

    public ArrayList<Category> getCategoryList() {
        return categoryList;
    }



}
