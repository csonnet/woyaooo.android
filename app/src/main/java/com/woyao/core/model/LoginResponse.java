package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by summerwind on 2016-05-11.
 */
public class LoginResponse extends BaseResponse  {


    @JsonProperty("content")
    private Map<String,String> content = new HashMap<String, String>();

    private String userid ="";

    public String getUserid() {
        return userid;
    }

    public Map<String, String> getContent() {
        return content;
    }
}
