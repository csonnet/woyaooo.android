package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Contact implements Serializable {
    private String id;
    private String snailview;
    private String title;
    private String description;
    private String tag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
