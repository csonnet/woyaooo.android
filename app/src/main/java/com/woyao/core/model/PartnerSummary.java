package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-28.
 */
public class PartnerSummary implements Serializable {
    private Integer id = 0;   // partner id
    private Integer user_id = 0;
    private Integer demand_id = 0;
    private String displayname = "";
    private String snailview = "";
    private String image = "";  // demand image
    private String mobile= "";
    private String wechat= "";
    private String email= "";
    private String office= "";

    private String title= "";
    private String description = "";
    private Boolean selected = false;
    private Boolean deletable = false;


    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public Integer getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(Integer demand_id) {
        this.demand_id = demand_id;
    }


    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getDeletable() {
        return deletable;
    }

    public String getWechat() {
        return wechat;
    }

    public String getOffice() {
        return office;
    }

    public String getEmail() {
        return email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
