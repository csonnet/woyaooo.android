package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetDemandResponse extends BaseResponse {
    @JsonDeserialize(contentAs = Demand.class)
    private Demand content;

    public Demand getContent() {
        return content;
    }

    public void setContent(Demand content) {
        this.content = content;
    }
}
