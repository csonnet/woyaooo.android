package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by summerwind on 2016-05-12.
 */
public class User  implements Serializable {
    public boolean businessChanged = false;
    public boolean cooperateChanged = false;
    public boolean memberChanged = false;
    private int id =0;
    private String key ="";
    private String Memo ="";
    private String snailview ="";
    private String idcard ="";
    private String buzzcard ="";
    private String cert ="";
    private int province =0;
    private int city=0;
    private int county=0;
    private String location="";
    private String occupation="";
    private String occupation_name="";
    private String category="";
    private String category_name="";
    private String title="";  //member_id
    private String verify="";
    private int verified =0;

    private String displayname="";
    private String mobile="";
    private Date registerTime;
    private int sex =0;
    private String birthday = "";
    private String edu="";
    private String intro="";

    private String phone="";
    private String wechat="";
    private String email="";
    private String office="";

    private String import_contact="yes";
    private int message_num =0;
    private int chance_num =0;
    private int cooperate_num =0;
    private int interest_num =0;
    private int person_num = 0;

    private int moves_num =0;
    private int marks_num =0;
    private int talks_num =0;
    private int agreements = 0;
    private int marked_num =0;
    private int history_num = 0;
    private int iapply = 0 ;
    private int applyme = 0 ;

    private String business  = "";
    private String business_name = "";

    private int demand_id =0;
    private String demand_title ="";
    private int member_id =0;
    private String displayDate="";

    private Integer temporal = 0 ;

    private String message ="";
    private  String mode = "phone";
    private String appoint_date="";
    private String appoint_time= "";
    private String appoint_address="";
    private String appoint_contact= "";

    private String customer_service = "";
    private String summary = "";
    private String service = "";
    private String slogan = "";

    private String performance ="";

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  category_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  relation_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  include_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  how_list = new ArrayList<KeyValue>();

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    public int getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(int demand_id) {
        this.demand_id = demand_id;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }


    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public int getVerified() {
        return verified;
    }

    public void setVerified(int verified) {
        this.verified = verified;
    }

    public int getProvince() {
        return province;
    }

    public void setProvince(int province) {
        this.province = province;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }




    public int getCounty() {
        return county;
    }

    public void setCounty(int county) {
        this.county = county;
    }



    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImport_contact() {
        return import_contact;
    }

    public void setImport_contact(String import_contact) {
        this.import_contact = import_contact;
    }

    public int getMessage_num() {
        return message_num;
    }

    public void setMessage_num(int message_num) {
        this.message_num = message_num;
    }

    public void setBusinessChanged(boolean businessChanged) {
        this.businessChanged = businessChanged;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    public String getMemo() {
        return Memo;
    }

    public void setMemo(String memo) {
        Memo = memo;
    }

    public ArrayList<KeyValue> getInclude_list() {
        return include_list;
    }

    public void setInclude_list(ArrayList<KeyValue> include_list) {
        this.include_list = include_list;
    }

    public ArrayList<KeyValue> getRelation_list() {
        return relation_list;
    }

    public void setRelation_list(ArrayList<KeyValue> relation_list) {
        this.relation_list = relation_list;
    }

    public int getChance_num() {
        return chance_num;
    }

    public int getCooperate_num() {
        return cooperate_num;
    }

    public int getPerson_num() {
        return person_num;
    }

    public void setChance_num(int chance_num) {
        this.chance_num = chance_num;
    }

    public void setCooperate_num(int cooperate_num) {
        this.cooperate_num = cooperate_num;
    }

    public void setPerson_num(int person_num) {
        this.person_num = person_num;
    }

    public ArrayList<KeyValue> getHow_list() {
        return how_list;
    }

    public void setHow_list(ArrayList<KeyValue> how_list) {
        this.how_list = how_list;
    }

    public Integer getTemporal() {
        return temporal;
    }

    public void setTemporal(Integer temporal) {
        this.temporal = temporal;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAppoint_date() {
        return appoint_date;
    }

    public void setAppoint_date(String appoint_date) {
        this.appoint_date = appoint_date;
    }

    public String getAppoint_time() {
        return appoint_time;
    }

    public void setAppoint_time(String appoint_time) {
        this.appoint_time = appoint_time;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getAppoint_address() {
        return appoint_address;
    }

    public void setAppoint_address(String appoint_address) {
        this.appoint_address = appoint_address;
    }

    public String getAppoint_contact() {
        return appoint_contact;
    }

    public void setAppoint_contact(String appoint_contact) {
        this.appoint_contact = appoint_contact;
    }

    public String getBuzzcard() {
        return buzzcard;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public int getInterest_num() {
        return interest_num;
    }

    public void setInterest_num(int interest_num) {
        this.interest_num = interest_num;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCustomer_service() {
        return customer_service;
    }

    public String getSummary() {
        return summary;
    }

    public String getSlogan() {
        return slogan;
    }

    public String getService() {
        return service;
    }

    public int getAgreements() {
        return agreements;
    }

    public int getMarks_num() {
        return marks_num;
    }

    public int getMoves_num() {
        return moves_num;
    }

    public int getTalks_num() {
        return talks_num;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public ArrayList<KeyValue> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(ArrayList<KeyValue> category_list) {
        this.category_list = category_list;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupation_name() {
        return occupation_name;
    }

    public void setOccupation_name(String occupation_name) {
        this.occupation_name = occupation_name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public int getHistory_num() {
        return history_num;
    }

    public int getMarked_num() {
        return marked_num;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDemand_title() {
        return demand_title;
    }

    public int getApplyme() {
        return applyme;
    }

    public int getIapply() {
        return iapply;
    }


}
