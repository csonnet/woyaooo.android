package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetRelationResponse extends BaseResponse implements Serializable{
    private Relation content;

    public Relation getContent() {
        return content;
    }

    public void setContent(Relation content) {
        this.content = content;
    }
}
