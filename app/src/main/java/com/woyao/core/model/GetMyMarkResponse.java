package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyMarkResponse extends BaseResponse {
    @JsonDeserialize(contentAs = MarkSummary.class)
    private List<MarkSummary> content;

    public List<MarkSummary> getContent() {
        return content;
    }

    public void setContent(List<MarkSummary> content) {
        this.content = content;
    }
}
