package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyRelationResponse extends BaseResponse {
    @JsonDeserialize(contentAs = RelationSearch.class)
    private RelationSearch content;

    public RelationSearch getContent() {
        return content;
    }

    public void setContent(RelationSearch content) {
        this.content = content;
    }
}
