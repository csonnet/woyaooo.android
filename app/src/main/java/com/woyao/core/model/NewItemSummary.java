package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class NewItemSummary implements Serializable {
    private Integer id =0;
    private Integer user_id =0;
    private String title ="";
    private String description="";
    private String image="";
    private Float charge = 0.0f;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setCharge(Float charge) {
        this.charge = charge;
    }

    public Float getCharge() {
        return charge;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }
}
