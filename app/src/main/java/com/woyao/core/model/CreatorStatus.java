package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-29.
 */
public class CreatorStatus implements Serializable{
    public static final String DEFER = "defer";
    public static final String MATCH = "match";
    public static final String APPLY = "apply";
    public static final String ACCEPT = "accept";
    private String status;
    private String updated;
    private String how;
    private String content;
    private String explan;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getHow() {
        return how;
    }

    public void setHow(String how) {
        this.how = how;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getExplan() {
        return explan;
    }

    public void setExplan(String explan) {
        this.explan = explan;
    }
}
