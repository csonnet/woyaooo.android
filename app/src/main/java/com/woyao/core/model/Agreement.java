package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-28.
 */
public class Agreement implements Serializable {

    private String image = "";
    private String title= "";   //评论内容
    private String description= "";   //合作介绍
    private Integer id = 0;    //match_id
    private Integer relation_user_id = 0;
    private Integer relation_demand_id = 0;

    @JsonProperty("comments")
    @JsonDeserialize(contentAs = CommentSummary.class, as = ArrayList.class)
    private List<CommentSummary> comments = new ArrayList<CommentSummary>();

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CommentSummary> getComments() {
        return comments;
    }

    public void setComments(List<CommentSummary> comments) {
        this.comments = comments;
    }

    public Integer getRelation_user_id() {
        return relation_user_id;
    }

    public Integer getRelation_demand_id() {
        return relation_demand_id;
    }
}
