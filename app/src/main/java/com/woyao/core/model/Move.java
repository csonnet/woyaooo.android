package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Move implements Serializable {
    private Integer id =0;
    private Integer user_id =0;
    private Integer demand_id =0;
    private String demand_title ="";
    private String category = "";

    private List<String> subject = new ArrayList<String>();
    private String description="";

    @JsonDeserialize(contentAs = MediaSummary.class, as = ArrayList.class)
    private ArrayList<MediaSummary>  medias = new ArrayList<MediaSummary>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<MediaSummary> getMedias() {
        return medias;
    }

    public void setMedias(ArrayList<MediaSummary> medias) {
        this.medias = medias;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getDemand_title() {
        return demand_title;
    }

    public void setDemand_title(String demand_title) {
        this.demand_title = demand_title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    public Integer getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(Integer demand_id) {
        this.demand_id = demand_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
