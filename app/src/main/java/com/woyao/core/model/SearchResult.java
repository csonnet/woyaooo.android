package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class SearchResult implements Serializable {

    @JsonDeserialize(contentAs = SearchItem.class)
    private List<SearchItem> chances;

    @JsonDeserialize(contentAs = SearchItem.class)
    private List<SearchItem> orgs;

    @JsonDeserialize(contentAs = SearchItem.class)
    private List<SearchItem> persons;


    private String chances_summary = "";

    private String orgs_summary = "";

    private String persons_summary = "";

    public List<SearchItem> getChances() {
        return chances;
    }

    public void setChances(List<SearchItem> chances) {
        this.chances = chances;
    }


    public String getChances_summary() {
        return chances_summary;
    }

    public void setChances_summary(String chances_summary) {
        this.chances_summary = chances_summary;
    }

    public List<SearchItem> getOrgs() {
        return orgs;
    }

    public List<SearchItem> getPersons() {
        return persons;
    }

    public String getOrgs_summary() {
        return orgs_summary;
    }

    public String getPersons_summary() {
        return persons_summary;
    }
}
