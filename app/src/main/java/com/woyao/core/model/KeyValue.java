package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-29.
 */
public class KeyValue implements Serializable {
    private String no;
    private String name;


    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KeyValue() {
    }

    public KeyValue(String no, String name) {
        this.no = no;
        this.name = name;
    }


}
