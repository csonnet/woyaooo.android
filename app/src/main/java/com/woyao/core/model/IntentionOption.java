package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-29.
 */
public class IntentionOption implements Serializable {
    private Integer no = 0;
    private String name = "";
    private Boolean selected = false;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IntentionOption() {
    }

    public IntentionOption(Integer no, String name,Boolean selected) {
        this.no = no;
        this.name = name;
        this.selected = selected;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
