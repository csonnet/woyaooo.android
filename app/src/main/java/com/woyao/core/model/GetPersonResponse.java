package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-05-13.
 */
public class GetPersonResponse extends BaseResponse  {
    @JsonDeserialize(contentAs = Person.class)
    private Person person;

    public Person getContent() {
        return person;
    }

    public void setContent(Person content) {
        this.person = content;
    }
}
