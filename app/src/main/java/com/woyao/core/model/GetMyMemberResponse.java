package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyMemberResponse extends BaseResponse {


    @JsonDeserialize(contentAs = Tag.class)
    private List<Tag> statis = new ArrayList<Tag>();

    @JsonDeserialize(contentAs = MemberSummary.class)
    private List<MemberSummary> content;

    public List<MemberSummary> getContent() {
        return content;
    }

    public void setContent(List<MemberSummary> content) {
        this.content = content;
    }

    public List<Tag> getStatis() {
        return statis;
    }
}
