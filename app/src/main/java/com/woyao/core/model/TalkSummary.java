package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class TalkSummary implements Serializable {
    private Integer id;
    private String title;
    private String description;
    private String snailview;
    private Integer message_num;
    private String type="match";

    private String access_right = "public";


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public Integer getMessage_num() {
        return message_num;
    }

    public void setMessage_num(Integer message_num) {
        this.message_num = message_num;
    }

    public String getType() {
        return type;
    }

    public String getAccess_right() {
        return access_right;
    }

    public void setAccess_right(String access_right) {
        this.access_right = access_right;
    }

    public void setType(String type) {
        this.type = type;
    }
}
