package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetContactResponse extends BaseResponse implements Serializable{
    private Contact content;

    public Contact getContent() {
        return content;
    }

    public void setContent(Contact content) {
        this.content = content;
    }
}
