package com.woyao.core.model;

/**
 * Created by summerwind on 2016-06-01.
 */
public class Service {
    private int id;
    private String image;
    private String service_id;
    private String title;
    private String description;
    private String link;
    private float amount;
    private int recommend = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getService_id() {
        return service_id;
    }

    public int getRecommend() {
        return recommend;
    }

    public String getImage() {
        return image;
    }

    public String getLink() {
        return link;
    }
}
