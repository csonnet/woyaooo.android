package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-20.
 */
public class AreaResponse extends BaseResponse {

    @JsonProperty("content")
    @JsonDeserialize(using = AreaArrayDeserializer.class)
    private ArrayList<Area> content;

    public ArrayList<Area> getContent() {
        return content;
    }

    public void setContent(ArrayList<Area> content) {
        this.content = content;
    }
}
