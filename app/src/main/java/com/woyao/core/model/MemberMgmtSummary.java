package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class MemberMgmtSummary implements Serializable {
    private Integer id =0 ;
    private Integer user_id =0 ;
    private String title ="";
    private String description="";
    private String status="";
    private String snailview="";
    private Boolean verified = false;
    private Boolean editable = true;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

    public String getTitle() {
        return title;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public Boolean getEditable() {
        return editable;
    }

    public Boolean getVerified() {
        return verified;
    }
}
