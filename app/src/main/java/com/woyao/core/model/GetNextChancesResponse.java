package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-13.
 */
public class GetNextChancesResponse extends BaseResponse  implements Serializable {
    private String type = "" ;

    private ChancesContent content;

    public ChancesContent getContent() {
        return content;
    }

    public void setContent(ChancesContent content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
