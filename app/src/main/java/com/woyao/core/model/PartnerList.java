package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-31.
 */
public class PartnerList implements Serializable {

    @JsonDeserialize(contentAs = PartnerSummary.class, as = ArrayList.class)
    private ArrayList<PartnerSummary> content;

    public ArrayList<PartnerSummary> getContent() {
        return content;
    }

    public void setContent(ArrayList<PartnerSummary> content) {
        this.content = content;
    }

}
