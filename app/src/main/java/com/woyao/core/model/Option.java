package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-17.
 */
public class Option  implements Serializable {
    private String relation;
    private String include;
    private String busikey;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
    }

    public String getBusikey() {
        return busikey;
    }

    public void setBusikey(String busikey) {
        this.busikey = busikey;
    }
}
