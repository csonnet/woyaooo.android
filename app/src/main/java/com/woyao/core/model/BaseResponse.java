package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-11.
 */
public class BaseResponse  implements Serializable {
    private String status;
    private String message;
    private String condition;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public Boolean isSuccess(){
        return "success".equals(status) || "sucess".equals(status);
    }


}
