package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class MediaSummary implements Serializable {
    private String id;
    private String url;
    private String snailview;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getSnailview() {
        return snailview;
    }

    public void setSnailview(String snailview) {
        this.snailview = snailview;
    }

}
