package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetChildHowResponse extends BaseResponse implements Serializable {
    @JsonProperty("content")
    private ArrayList<How> howList;

    public ArrayList<How> getHowList() {
        return howList;
    }

    public void setHowList(ArrayList<How> howList) {
        this.howList = howList;
    }


}
