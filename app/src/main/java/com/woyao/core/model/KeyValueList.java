package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-31.
 */
public class KeyValueList implements Serializable {

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue> content  = new ArrayList<KeyValue>();

    public ArrayList<KeyValue> getContent() {
        return content;
    }

    public void setContent(ArrayList<KeyValue> content) {
        this.content = content;
    }

}
