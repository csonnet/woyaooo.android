package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-28.
 */
public class RelatedChance implements Serializable {

    private String image = "";
    private Integer id = 0;
    private String title= "";   //评论内容
    private String description= "";   //合作介绍



    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
