package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetMyCompanyResponse extends BaseResponse implements Serializable {
    @JsonProperty("content")
    private ArrayList<CompanySummary> companylist;

    public ArrayList<CompanySummary> getCompanylist() {
        return companylist;
    }

    public void setCompanylist(ArrayList<CompanySummary> companylist) {
        this.companylist = companylist;
    }


}
