package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-28.
 */
public class PartnerComment implements Serializable {

    private String image = "";
    private Integer user_id = 0;
    private String title= "";   //评论内容
    private String description= "";   //合作介绍


    @JsonProperty("comments")
    @JsonDeserialize(contentAs = CommentSummary.class, as = ArrayList.class)
    private List<CommentSummary> comments = new ArrayList<CommentSummary>();

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }


}
