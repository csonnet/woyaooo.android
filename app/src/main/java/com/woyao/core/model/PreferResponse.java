package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-12.
 */
public class PreferResponse extends BaseResponse implements Serializable {
    private Prefer content;

    public Prefer getContent() {
        return content;
    }

    public void setContent(Prefer content) {
        this.content = content;
    }
}
