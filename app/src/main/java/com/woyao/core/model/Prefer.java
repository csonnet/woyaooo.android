package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-12.
 */
public class Prefer implements Serializable {

    private Integer demand_id = 0 ;
    private String demand_title = "" ;
    private Integer temporal = 0 ;

    private Integer matchatleast = 60 ;

    private Boolean demand_visible = false;
    private Boolean category_visible = false;
    private Boolean include_visible = false;
    private Boolean relation_visible = false;
    private Boolean how_visible = false;
    private Boolean temporal_visible = false;
    private Boolean types_visible = false;

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  category_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  relation_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  include_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  how_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  types_list = new ArrayList<KeyValue>();

    public ArrayList<KeyValue> getInclude_list() {
        return include_list;
    }

    public void setInclude_list(ArrayList<KeyValue> include_list) {
        this.include_list = include_list;
    }

    public ArrayList<KeyValue> getTypes_list() {
        return types_list;
    }

    public void setTypes_list(ArrayList<KeyValue> types_list) {
        this.types_list = types_list;
    }

    public ArrayList<KeyValue> getRelation_list() {
        return relation_list;
    }

    public void setRelation_list(ArrayList<KeyValue> relation_list) {
        this.relation_list = relation_list;
    }


    public ArrayList<KeyValue> getHow_list() {
        return how_list;
    }

    public void setHow_list(ArrayList<KeyValue> how_list) {
        this.how_list = how_list;
    }

    public Integer getTemporal() {
        return temporal;
    }

    public void setTemporal(Integer temporal) {
        this.temporal = temporal;
    }

    public Integer getDemand_id() {
        return demand_id;
    }

    public void setDemand_id(Integer demand_id) {
        this.demand_id = demand_id;
    }

    public String getDemand_title() {
        return demand_title;
    }

    public void setDemand_title(String demand_title) {
        this.demand_title = demand_title;
    }

    public ArrayList<KeyValue> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(ArrayList<KeyValue> category_list) {
        this.category_list = category_list;
    }

    public Boolean getCategory_visible() {
        return category_visible;
    }

    public Boolean getDemand_visible() {
        return demand_visible;
    }

    public Boolean getHow_visible() {
        return how_visible;
    }

    public Boolean getInclude_visible() {
        return include_visible;
    }

    public Boolean getRelation_visible() {
        return relation_visible;
    }

    public Boolean getTemporal_visible() {
        return temporal_visible;
    }

    public Boolean getTypes_visible() {
        return types_visible;
    }

    public Integer getMatchatleast() {
        return matchatleast;
    }

    public void setMatchatleast(Integer matchatleast) {
        this.matchatleast = matchatleast;
    }
}
