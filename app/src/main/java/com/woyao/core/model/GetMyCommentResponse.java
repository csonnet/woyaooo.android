package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyCommentResponse extends BaseResponse {
    @JsonDeserialize(contentAs = CommentSummary.class)
    private List<CommentSummary> content;

    public List<CommentSummary> getContent() {
        return content;
    }

    public void setContent(List<CommentSummary> content) {
        this.content = content;
    }
}
