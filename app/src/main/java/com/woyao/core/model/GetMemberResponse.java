package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMemberResponse extends BaseResponse {
    @JsonDeserialize(contentAs = Member.class)
    private Member content;

    public Member getContent() {
        return content;
    }

    public void setContent(Member content) {
        this.content = content;
    }
}
