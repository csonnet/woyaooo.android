package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetSearchResponse extends BaseResponse {
    @JsonDeserialize(contentAs = SearchResult.class)
    private SearchResult content;

    public SearchResult getContent() {
        return content;
    }

    public void setContent(SearchResult content) {
        this.content = content;
    }
}
