package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetConcensusResponse extends BaseResponse implements Serializable{
    private Concensus content;

    public Concensus getContent() {
        return content;
    }

    public void setContent(Concensus content) {
        this.content = content;
    }
}
