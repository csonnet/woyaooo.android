package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by summerwind on 2016-05-24.
 */
public class GetBusinessCategoryResponse extends BaseResponse implements Serializable {
    @JsonProperty("content")
    @JsonDeserialize(contentAs = BusinessCategory.class, as = ArrayList.class)
    private ArrayList<BusinessCategory> categoryList;

    public ArrayList<BusinessCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<BusinessCategory> categoryList) {
        this.categoryList = categoryList;
    }


}
