package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyContactResponse extends BaseResponse {
    @JsonDeserialize(contentAs = ContactSummary.class)
    private List<ContactSummary> content;

    public List<ContactSummary> getContent() {
        return content;
    }

    public void setContent(List<ContactSummary> content) {
        this.content = content;
    }
}
