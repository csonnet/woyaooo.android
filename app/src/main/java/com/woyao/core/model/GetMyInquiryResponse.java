package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyInquiryResponse extends BaseResponse {
    @JsonDeserialize(contentAs = InquirySummary.class)
    private List<InquirySummary> content;

    public List<InquirySummary> getContent() {
        return content;
    }

    public void setContent(List<InquirySummary> content) {
        this.content = content;
    }
}
