package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-13.
 */
public class GetMessageResponse extends BaseResponse  implements Serializable {
    private Conversation content;

    public Conversation getContent() {
        return content;
    }

    public void setContent(Conversation content) {
        this.content = content;
    }
}
