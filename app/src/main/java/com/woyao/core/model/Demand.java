package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by summerwind on 2016-05-31.
 */
public class Demand  implements Serializable {
    private Integer id =0;
    private Integer member_id =0;
    private String member_title ="";
    private String access_right ="public";

    private String displayTitle ="";

    private String title ="";
    private String resourcex="";
    private String requirement="";

    private String  how ="";
    private String  how_names="";

    private String  enduse="";
    private String  specification="";
    private String  processing="";
    private String  durable="";
    private Integer quantity =0;
    private Integer value =0;

    private Integer commentable =1;

    private  Float refer_fee= 0.0f;

    private Integer transaction = -1;
    private String  transaction_text ="";

    private String name_caption="";
    private String description_caption="";
    private String type_caption="";
    private String  enduse_caption="";
    private String  specification_caption="";
    private String  processing_caption="";
    private String  durable_caption="";

    private String  types_caption="";
    private String  relation_caption="";
    private String  include_caption="";
    private String  requirement_caption="";
    private String  refer_fee_caption="";
    private String  intention_fee_caption="";
    private String  value_caption="";
    private String  quantity_caption="";



    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  type_list = new ArrayList<KeyValue>();


    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  relation = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  include = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = KeyValue.class, as = ArrayList.class)
    private ArrayList<KeyValue>  types_list = new ArrayList<KeyValue>();

    @JsonDeserialize(contentAs = MediaSummary.class, as = ArrayList.class)
    private ArrayList<MediaSummary>  medias = new ArrayList<MediaSummary>();

    private String image = ""; // a snail view

    private String main_image = "";
    private List<String> pics = new ArrayList<>();


    private Integer available =0;

    public Integer getTransaction() {
        return transaction;
    }

    public void setTransaction(Integer transaction) {
        this.transaction = transaction;
    }

    public String getResourcex() {
        return resourcex;
    }

    public void setResourcex(String resourcex) {
        this.resourcex = resourcex;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMember_id() {
        return member_id;
    }

    public void setMember_id(Integer member_id) {
        this.member_id = member_id;
    }

    public String getMember_title() {
        return member_title;
    }

    public void setMember_title(String member_title) {
        this.member_title = member_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    

    public ArrayList<KeyValue> getRelation() {
        return relation;
    }

    public void setRelation(ArrayList<KeyValue> relation) {
        this.relation = relation;
    }


    public ArrayList<KeyValue> getInclude() {
        return include;
    }

    public void setInclude(ArrayList<KeyValue> include) {
        this.include = include;
    }


    public ArrayList<MediaSummary> getMedias() {
        return medias;
    }

    public void setMedias(ArrayList<MediaSummary> medias) {
        this.medias = medias;
    }



    public String getHow() {
        return how;
    }
    public void setHow(String how) {
        this.how = how;
    }

    public String getHow_names() {
        return how_names;
    }
    public void setHow_names(String how_names) {
        this.how_names = how_names;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public String getAccess_right() {
        return access_right;
    }

    public void setAccess_right(String access_right) {
        this.access_right = access_right;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }



    public Integer getCommentable() {
        return commentable;
    }

    public void setCommentable(Integer commentable) {
        this.commentable = commentable;
    }

    public Float getRefer_fee() {
        return refer_fee;
    }

    public void setRefer_fee(Float refer_fee) {
        this.refer_fee = refer_fee;
    }


    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }



    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getEnduse() {
        return enduse;
    }

    public void setEnduse(String enduse) {
        this.enduse = enduse;
    }

    public String getProcessing() {
        return processing;
    }

    public void setProcessing(String processing) {
        this.processing = processing;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }


    public String getDurable() {
        return durable;
    }

    public void setDurable(String durable) {
        this.durable = durable;
    }

    public ArrayList<KeyValue> getTypes_list() {
        return types_list;
    }

    public void setTypes_list(ArrayList<KeyValue> types_list) {
        this.types_list = types_list;
    }

    public ArrayList<KeyValue> getType_list() {
        return type_list;
    }

    public void setType_list(ArrayList<KeyValue> type_list) {
        this.type_list = type_list;
    }

    public String getRelation_caption() {
        return relation_caption;
    }

    public String getInclude_caption() {
        return include_caption;
    }

    public String getTypes_caption() {
        return types_caption;
    }

    public String getSpecification_caption() {
        return specification_caption;
    }

    public String getProcessing_caption() {
        return processing_caption;
    }

    public String getEnduse_caption() {
        return enduse_caption;
    }

    public String getDurable_caption() {
        return durable_caption;
    }

    public String getType_caption() {
        return type_caption;
    }

    public String getRequirement_caption() {
        return requirement_caption;
    }

    public String getRefer_fee_caption() {
        return refer_fee_caption;
    }

    public String getIntention_fee_caption() {
        return intention_fee_caption;
    }

    public String getValue_caption() {
        return value_caption;
    }

    public String getQuantity_caption() {
        return quantity_caption;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getName_caption() {
        return name_caption;
    }

    public String getDescription_caption() {
        return description_caption;
    }

    public String getTransaction_text() {
        return transaction_text;
    }

    public void setTransaction_text(String transaction_text) {
        this.transaction_text = transaction_text;
    }
}
