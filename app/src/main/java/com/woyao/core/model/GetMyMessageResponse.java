package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-06-01.
 */
public class GetMyMessageResponse extends BaseResponse {
    @JsonDeserialize(contentAs = MessageSearch.class)
    private MessageSearch content;

    public MessageSearch getContent() {
        return content;
    }

    public void setContent(MessageSearch content) {
        this.content = content;
    }
}
