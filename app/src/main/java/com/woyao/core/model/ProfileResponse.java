package com.woyao.core.model;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-12.
 */
public class ProfileResponse extends BaseResponse implements Serializable {
    private User content;

    public User getContent() {
        return content;
    }

    public void setContent(User content) {
        this.content = content;
    }
}
