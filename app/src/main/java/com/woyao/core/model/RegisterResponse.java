package com.woyao.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by summerwind on 2016-05-15.
 */
public class RegisterResponse extends BaseResponse  implements Serializable {

    @JsonProperty("content")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
