package com.woyao.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by summerwind on 2016-05-31.
 */
public class GetMoveResponse extends BaseResponse {
    @JsonDeserialize(contentAs = Move.class)
    private Move content;

    public Move getContent() {
        return content;
    }

    public void setContent(Move content) {
        this.content = content;
    }
}
