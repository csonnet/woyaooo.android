package com.woyao.core;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSPlainTextAKSKCredentialProvider;
import com.woyao.WoyaoooApplication;
import com.woyao.core.util.DateUtil;

import java.io.FileNotFoundException;
import java.util.Date;


/**
 * Created by smellycat on 2016/9/1.
 */
public class FileUtil {
    private final static String endpoint = "https://oss-cn-hangzhou.aliyuncs.com";
    private final static String accessKeyId = "IDwc212K1e0QnPCG";
    private final static String accessKeySecret = "xXouCAaaZM7V1Eh2W4WuGNJotK8Q0I";

    public final static String videoBucketName = "woyaooovideo";
    public final static String bucketName = "woyaooo1";
    private static Resources res;
    private static int resId;

    public static OSS getOss(Context context){
        OSSCredentialProvider credentialProvider = new OSSPlainTextAKSKCredentialProvider(accessKeyId, accessKeySecret);
        OSS oss = new OSSClient(context, endpoint, credentialProvider);
        return oss;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        FileUtil.res = res;
        FileUtil.resId = resId;

        // 先把inJustDecodeBounds设置为true 取得原始图片的属性
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // 然后算一下我们想要的最终的属性
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // 在decode的时候 别忘记直接 把这个属性改为false 否则decode出来的是null
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // 先从options 取原始图片的 宽高
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            //一直对这个图片进行宽高 缩放，每次都是缩放1倍，然后这么叠加，当发现叠加以后 也就是缩放以后的宽或者高小于我们想要的宽高
            //这个缩放就结束 跳出循环 然后就可以得到我们极限的inSampleSize值了。
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String getVideoObjectKey(){
        return "video_" + WoyaoooApplication.userId + "_" + DateUtil.format(new Date(), "yyMMddHHmmss"  ) +".mp4";
    }


    public static String getAvatarObjectKey(){
        return "snail_" + DateUtil.format(new Date(), "yyMMddHHmmss") + "_"+ WoyaoooApplication.userId;
    }

    public static String getIDcardObjectKey(){
        return "id_" + DateUtil.format(new Date(), "yyMMddHHmmss") + "_"+ WoyaoooApplication.userId;
    }

    public static String getBUZZcardObjectKey(){
        return "buzz_" + DateUtil.format(new Date(), "yyMMddHHmmss") + "_"+ WoyaoooApplication.userId;
    }
    public static String getCertObjectKey(){
        return "cert_" + DateUtil.format(new Date(), "yyMMddHHmmss") + "_"+ WoyaoooApplication.userId;
    }

    public static String getPicObjectKey(){
        return  WoyaoooApplication.userId + "_" + DateUtil.format(new Date(), "yyMMddHHmmss");
    }

    public static String getBuzObjectKey(){
        return WoyaoooApplication.userId+"_business";
    }


    public static Bitmap decodeUri(Uri selectedImage,Context context) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o2);

    }
}
