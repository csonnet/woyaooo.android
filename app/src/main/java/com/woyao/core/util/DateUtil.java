package com.woyao.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by summerwind on 2016-05-13.
 */
public class DateUtil {
    public static String Current(){
        return format(new Date(), "yyyy-MM-dd HH:mm:ss");
    }
    public static String format(Date date){
        return format(date, "yyyy-MM-dd HH:mm:ss");
    }
    public static String format(Date date, String format){
        if(date == null)return null;
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }
}
