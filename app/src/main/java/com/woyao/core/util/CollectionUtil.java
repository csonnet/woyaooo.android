package com.woyao.core.util;

import java.util.Collection;

/**
 * Created by summerwind on 2016-05-13.
 */
public class CollectionUtil {
    public static <T> boolean notEmpty(T[] array){
        return array!=null&&array.length>0;
    }
    public static <T> boolean isEmpty(T[] array){
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Collection collection){
        return collection == null || collection.isEmpty();
    }

    public static boolean notEmpty(Collection collection){
        return collection != null && !collection.isEmpty();
    }

}
