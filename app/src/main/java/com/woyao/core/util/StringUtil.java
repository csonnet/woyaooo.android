package com.woyao.core.util;

import java.util.List;

/**
 * Created by summerwind on 2016-05-13.
 */
public class StringUtil {
    public static boolean isNullOrEmpty(String src){
        if(src == null || src.length() == 0){
            return true;
        }
        return false;
    }
    public static boolean notNullOrEmpty(String src){
        return src != null && src.length()>0;
    }


    public static String join(List<String> src, String split){
        StringBuilder builder=new StringBuilder();
        for(int i=0;i<src.size();i++){
            if(i==(src.size()-1)){
                builder.append(src.get(i));
            }else{
                builder.append(src.get(i)).append(split);
            }
        }

        return builder.toString();
    }

    public static String join(String[] src, String split){
        StringBuilder builder=new StringBuilder();
        for(int i=0;i<src.length;i++){
            if(i==(src.length-1)){
                builder.append(src[i]);
            }else{
                builder.append(src[i]).append(split);
            }
        }

        return builder.toString();
    }
}
