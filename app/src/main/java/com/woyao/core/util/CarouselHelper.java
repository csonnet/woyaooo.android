package com.woyao.core.util;

import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.woyao.R;

/**
 * Created by summerwind on 2016-06-25.
 */
public class CarouselHelper {
    public static void setDefault(CarouselView carouselView){

        try{
            carouselView.setPageCount(1);
            carouselView.setImageListener(new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
                    imageView.setImageResource(R.drawable.no_pic);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
