package com.woyao.core.util;

import android.content.Context;
import android.content.res.Resources;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woyao.R;
import com.woyao.WoyaoooApplication;

import okhttp3.HttpUrl;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import okhttp3.OkHttpClient;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.Request;
import java.io.IOException;
import com.woyao.WoyaoooApplication;

/**
 * Created by summerwind on 2016-05-11.
 */
public class ServiceFactory {
    private static Retrofit _retrofit;

    public synchronized static Retrofit getInstance(){
        if(_retrofit == null){
            // Define the interceptor, add authentication headers
//            Interceptor interceptor = new Interceptor() {
//                @Override
//                public Response intercept(Chain chain) throws IOException {
//                    Request oldRequest = chain.request();
//
//                    // 添加新的参数
//                    HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
//                            .newBuilder()
//                            .scheme(oldRequest.url().scheme())
//                            .host(oldRequest.url().host())
//                            .addQueryParameter("apikey",WoyaoooApplication.apikey)
//                            .addQueryParameter("apisecret",WoyaoooApplication.apisecret);
//
//                    // 新的请求
//                    Request newRequest = oldRequest.newBuilder()
//                            .method(oldRequest.method(), oldRequest.body())
//                            .url(authorizedUrlBuilder.build())
//                            .build();
//
//                    return chain.proceed(newRequest);
//                }
//            };
//
//            // Add the interceptor to OkHttpClient
//            OkHttpClient client = new OkHttpClient();
//            client.interceptors().add(interceptor);


            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            _retrofit = new Retrofit.Builder()
                    .baseUrl(WoyaoooApplication.resources.getString(R.string.base_url))
                    .addConverterFactory(JacksonConverterFactory.create(mapper))
                    .build();
//                    .client(client)

        }
        return _retrofit;
    }
    public static <TService> TService get(Class<TService> clazz){
        return getInstance().create(clazz);
    }


}
