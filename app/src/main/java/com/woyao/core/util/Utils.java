package com.woyao.core.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

import com.woyao.core.model.KeyValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/4/25 0025.
 */

public class Utils {

    public static float dip2px(Context _context, float _dpValue) {
        float scale = _context.getResources().getDisplayMetrics().density;
        return _dpValue * scale;
    }

    /**
     * 获取 DisplayMetrics
     *
     * @param _context context
     * @return DisplayMetrics
     */
    public static DisplayMetrics getDisplayMetrics(Context _context) {
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        ((Activity) _context).getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics;
    }

    public static String getMode( String status ) {

        if (status.equals("phone")){
            return "电话";
        }
        if (status.equals("video")){
            return "视频";
        }
        if (status.equals("meet")){
            return "面谈";
        }
        return "电话";
    }

    public static List<String> getTexts(List<KeyValue> pairs) {
        List<String> list = new ArrayList<>();
        for (KeyValue pair : pairs) {
            list.add(pair.getName());
        }
        return list;
    }


}
