package com.woyao.core.service;

import com.woyao.core.model.GetChildHowResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by summerwind on 2016-05-24.
 */
public interface HowService {
    /**
     * 获取子类型
     * @param parenthow
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST("getchildhow")
    Call<GetChildHowResponse> getChildHow(@Field("how") String parenthow,@Field("userid")int userId,@Field("from") String from );

    /**
     * 获取子类型
     * @param type
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST("gettypehow")
    Call<GetChildHowResponse> getTypeHow(@Field("type") String type,@Field("userid")int userId );

    /**
     * 获取子类型
     * @param keyword
     * @return
     */
    @FormUrlEncoded
    @POST("searchhow")
    Call<GetChildHowResponse> searchHow(@Field("keyword") String keyword);



}
