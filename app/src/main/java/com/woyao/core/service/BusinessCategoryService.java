package com.woyao.core.service;

import com.woyao.core.model.GetBusinessCategoryResponse;
import com.woyao.core.model.GetBusinessResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by summerwind on 2016-05-24.
 */
public interface BusinessCategoryService {
    /**
     * 获取子类型
     * @param parentBusinessNo
     * @param userId
     * @param suggest
     * @param from
     * @return
     */
    @FormUrlEncoded
    @POST("getchildbusiness")
    Call<GetBusinessCategoryResponse> getChildCategory(@Field("buz")String parentBusinessNo,@Field("userid")int userId,@Field("suggest")String suggest,@Field("from")String from);

    @FormUrlEncoded
    @POST("getpossiblebusiness")
    Call<GetBusinessResponse> getPossibleBusiness(@Field("userid")int userId);

    /**
     * 获取搜索结果
     * @param keyword
     * @return
     */
    @FormUrlEncoded
    @POST("searchbusiness")
    Call<GetBusinessCategoryResponse> searchBusiness(@Field("keyword")String keyword);


    /**
     * 获取业务详情
     * @param BusinessNo
     * @return
     */
    @FormUrlEncoded
    @POST("getbusiness")
    Call<GetBusinessResponse> getBusiness(@Field("buz")String BusinessNo);


}
