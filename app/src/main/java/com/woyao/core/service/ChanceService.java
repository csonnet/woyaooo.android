package com.woyao.core.service;

import com.woyao.core.model.ApplyResponse;
import com.woyao.core.model.BaseResponse;
import com.woyao.core.model.GetNextChanceResponse;
import com.woyao.core.model.GetNextChanceSumaryResponse;
import com.woyao.core.model.GetNextChancesResponse;
import com.woyao.core.model.GetOrganizationResponse;
import com.woyao.core.model.GetPersonResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by summerwind on 2016-05-13.
 */

public interface ChanceService {
    /**
     * 下个机会
     * @param userId
     * @return
     */
    @POST("next")
    @FormUrlEncoded
    Call<GetNextChanceSumaryResponse> next(@Field("userid") int userId,@Field("login") String login ,@Field("ignore") int pid);


    /**
     * 下个机会
     * @param userId
     * @param act
     * @return
     */
    @POST("nexts")
    @FormUrlEncoded
    Call<GetNextChancesResponse> nexts(@Field("userid") int userId,
                                       @Field("act") String act,@Field("login") String login);

    /**
     * 下个机会
     * @param userId
     * @param pid
     * @return
     */
    @POST("view")
    @FormUrlEncoded
    Call<BaseResponse> view(@Field("userid") int userId,
                                       @Field("pid") Integer pid);


    /**
     * 申请合作
     * @param userId
     * @param businessId
     * @param demandid
     * @param pid
     * @return
     */
    @POST("apply")
    @FormUrlEncoded
    Call<ApplyResponse> apply(@Field("userid")int userId, @Field("how")String how,@Field("bid")int businessId,
                              @Field("demandid")int demandid, @Field("pid")int pid, @Field("relation_user_id")int relation_user_id);

    /**
     * 申请合作
     * @param userId
     * @param id
     * @param status
     * @return
     */
    @POST("mark")
    @FormUrlEncoded
    Call<BaseResponse> mark(@Field("userid")int userId,
                            @Field("id")int id,
                            @Field("bid")int bid,
                            @Field("status") String status);

    /**
     * 申请合作
     * @param userId
     * @param id
     * @param bid
     * @return
     */
    @POST("consult")
    @FormUrlEncoded
    Call<BaseResponse> consult(@Field("userid")int userId,
                            @Field("id")int id,
                            @Field("bid")int bid);

    /**
     * 申请合作
     * @param userId
     * @param mid
     * @param pid
     * @return
     */
    @POST("quit")
    @FormUrlEncoded
    Call<BaseResponse> quit(@Field("userid")int userId,
                            @Field("mid")int mid,
                            @Field("pid")int pid);

    /**
     * 举报合作
     * @param userId
     * @param mid
     * @param pid
     * @param id
     * @param content
     * @return
     */
    @POST("quit")
    @FormUrlEncoded
    Call<BaseResponse> report(@Field("userid")int userId,
                            @Field("id")int id,
                            @Field("pid")int pid,
                            @Field("mid")int mid,
                            @Field("content")String content);

    /**
     * getuser
     * @param userId
     * @param businessId
     * @param rid
     * @param partner_id
     * @return
     */
    @POST("get")
    Call<GetNextChanceResponse> getChance(@Query("userid") int userId, @Query("bid")int businessId, @Query("rid")String rid, @Query("pid")int partner_id);

    /**
     * getadvice
     * @param userId
     * @param mid
     * @param pid
     * @return
     */
    @POST("getadvice")
    Call<BaseResponse> getAdvice(@Query("userid") int userId, @Query("mid")int mid, @Query("pid")int pid);

    /**
     * gtuser
     * @param userId
     * @param relationId
     * @return
     */
    @POST("getperson")
    Call<GetPersonResponse> getPerson(@Query("userid") int userId, @Query("id")int relationId);

    /**
     * getuser
     * @param userId
     * @param orgId
     * @return
     */
    @POST("getorganization")
    Call<GetOrganizationResponse> getOrganization(@Query("userid") int userId, @Query("id")int orgId);


    /**
     * 分享
     * @param userId
     * @param bid
     * @param relation_demand_id
     * @return
     */
    @POST("refer")
    @FormUrlEncoded
    Call<BaseResponse> refer(@Field("userid") int userId, @Field("bid")int bid, @Field("relation_demand_id")Integer relation_demand_id  );

    /**
     * 分享
     * @param userId
     * @param mobiles
     * @return
     */
    @POST("invite")
    @FormUrlEncoded
    Call<BaseResponse> invite(@Field("userid") int userId, @Field("mobiles")String mobiles );


    /**
     * 分享
     * @param userId
     * @param comment_id
     * @return
     */
    @POST("getcommentcontent")
    @FormUrlEncoded
    Call<BaseResponse> getCommentContent(@Field("userid") int userId, @Field("id")int comment_id);

    /**
     * 分享
     * @param userId
     * @param comment_id
     * @return
     */
    @POST("dig")
    @FormUrlEncoded
    Call<BaseResponse> dig(@Field("userid") int userId,  @Field("id")int comment_id);



}
