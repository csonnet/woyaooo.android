package com.woyao.core.service;

import com.woyao.core.model.BaseResponse;
import com.woyao.core.model.GetBalanceResponse;
import com.woyao.core.model.GetChargeResponse;
import com.woyao.core.model.GetChildCategoryResponse;
import com.woyao.core.model.GetChildOccupationResponse;
import com.woyao.core.model.GetChildTypeResponse;
import com.woyao.core.model.GetCommentResponse;
import com.woyao.core.model.GetConcensusResponse;
import com.woyao.core.model.GetContactResponse;
import com.woyao.core.model.GetDemandResponse;
import com.woyao.core.model.GetMemberResponse;
import com.woyao.core.model.GetMemberTypeResponse;
import com.woyao.core.model.GetMessageResponse;
import com.woyao.core.model.GetMoveResponse;
import com.woyao.core.model.GetMyCommentResponse;
import com.woyao.core.model.GetMyCompanyResponse;
import com.woyao.core.model.GetMyContactResponse;
import com.woyao.core.model.GetMyDemandResponse;
import com.woyao.core.model.GetMyInquiryResponse;
import com.woyao.core.model.GetMyInterestResponse;
import com.woyao.core.model.GetMyMarkResponse;
import com.woyao.core.model.GetMyMemberResponse;
import com.woyao.core.model.GetMyMessageResponse;
import com.woyao.core.model.GetMyRelationResponse;
import com.woyao.core.model.GetMyUserResponse;
import com.woyao.core.model.GetOrgResponse;
import com.woyao.core.model.GetOrganizationResponse;
import com.woyao.core.model.GetPersonResponse;
import com.woyao.core.model.GetRelationResponse;
import com.woyao.core.model.GetSearchResponse;
import com.woyao.core.model.GetSubjectsResponse;
import com.woyao.core.model.GetTransactionListResponse;
import com.woyao.core.model.LoginResponse;
import com.woyao.core.model.PreferResponse;
import com.woyao.core.model.ProfileResponse;
import com.woyao.core.model.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by summerwind on 2016-05-11.
 */
public interface AccountService {

    /**
     * 登录
     * @param device
     * @param source
     * @param version
     * @return
     */
    @POST("getuserid")
    @FormUrlEncoded
    Call<BaseResponse> getUserId(@Field("device")String device, @Field("source")String source, @Field("version")String version);

    /**
     * 登录
     * @param userId
     * @return
     */
    @POST("checkversion")
    @FormUrlEncoded
    Call<BaseResponse> checkVersion(@Field("userid")int userId, @Field("version")String version);

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> login(@Field("mobile")String username, @Field("password")String password);

    @FormUrlEncoded
    @POST("setpassword")
    Call<BaseResponse> setPassword(@Field("userid")int userId,
                                   @Field("old")String oldPwd,
                                   @Field("old")String newPwd);

    /**
     * 获取帐号信息
     * @param userId
     * @param source
     * @return
     */
    @POST("getprofile")
    @FormUrlEncoded
    Call<ProfileResponse> getProfile(@Field("userid")int userId,@Field("source")String source,@Field("version")String version);


    /**
     * 设置用户
     * @param userId
     * @param attr
     * @param content
     * @return
     */
    @POST("setprofileattr")
    @FormUrlEncoded
    Call<BaseResponse> setProfileAttr(@Field("userid")int userId,
                                      @Field("attr")String attr,
                                      @Field("content")String content);

    /**
     * 获取偏好信息
     * @param userId
     * @return
     */
    @POST("getprefer")
    @FormUrlEncoded
    Call<PreferResponse> getPrefer(@Field("userid")int userId);


    /**
     * 设置用户
     * @param userId
     * @param relation
     * @param include
     * @param how
     * @param temporal
     * @return
     */
    @POST("setprefer")
    @FormUrlEncoded
    Call<BaseResponse> setPrefer(@Field("userid")int userId,
                                 @Field("demand_id")Integer demand_id,
                                 @Field("types")String type,
                                      @Field("relation")String relation,
                                      @Field("include")String include,
                                      @Field("how")String how,
                                 @Field("temporal")String temporal,
                                 @Field("category")String category,
                                 @Field("matchatleast")Integer matchatleast);

    /**
     * 发送验证码
     * @param key
     * @param userId
     * @param username
     * @return
     */
    @POST("sendcode")
    @FormUrlEncoded
    Call<BaseResponse> sendCode(@Field("apikey")String key ,@Field("userid")int userId,@Field("mobile")String username);


    /**
     * 注册
     * @param username
     * @param password
     * @param rid
     * @return
     */
    @POST("register")
    @FormUrlEncoded
    Call<RegisterResponse> register(@Field("userid")Integer userid,@Field("mobile")String username, @Field("password")String password, @Field("rid")String rid,@Field("lng") String longitude,
                                    @Field("lat")String latitude, @Field("source")String source);

    /**
     * 忘记密码
     * @param username
     * @param password
     * @return
     */
    @POST("resetpassword")
    @FormUrlEncoded
    Call<BaseResponse> resetPassword(@Field("mobile")String username, @Field("new")String password);


    @FormUrlEncoded
    @POST("getmydemand")
    Call<GetMyDemandResponse> getMyDemand(@Field("userid")int userId , @Field("all")String all);

    @POST("getdemand")
    @FormUrlEncoded
    Call<GetDemandResponse> getDemand(@Field("userid")int userId, @Field("id")int demandId);

    @POST("adddemand")
    @FormUrlEncoded
    Call<BaseResponse> addDemand(@Field("userid")int userId
            , @Field("member_id")int member_id
            , @Field("how")String how
            , @Field("type")String type
            , @Field("types")String types
            , @Field("title")String title
            , @Field("resourcex")String resource
            , @Field("main_image")String main_image
            , @Field("image")String image
            , @Field("requirement")String requirement
            , @Field("include")String include
            , @Field("relation")String relation
            , @Field("access_right")String access_right
            , @Field("intention_fee")String intention_fee
            , @Field("commentable")Integer commentable
            , @Field("refer_fee")Float refer_fee
            , @Field("enduse")String enduse
            , @Field("specification")String specification
            , @Field("processing")String processing
            , @Field("value")Integer value
            , @Field("quantity")Integer quantity
            , @Field("transaction")Integer transaction
    );

    @POST("deldemand")
    @FormUrlEncoded
    Call<BaseResponse> deleteDemand(@Field("userid")int userId
            , @Field("id")int demand_id);

    @POST("setdemandattr")
    @FormUrlEncoded
    Call<BaseResponse> setDemandAttr(@Field("userid")int userId,
                                     @Field("id")Integer id,
                                     @Field("attr")String attr,
                                     @Field("content")String content,
                                     @Field("act")String act);

    @FormUrlEncoded
    @POST("getmyinterest")
    Call<GetMyInterestResponse> getMyInterest(@Field("userid")int userId, @Field("num")int thenum);

    @FormUrlEncoded
    @POST("getmymoves")
    Call<GetMyMarkResponse> getMyMoves(@Field("userid")int userId);

    @FormUrlEncoded
    @POST("getmyfollowers")
    Call<GetMyMarkResponse> getMyFollowers(@Field("userid")int userId);

    @FormUrlEncoded
    @POST("addmove")
    Call<BaseResponse> addMove(@Field("userid")int userId, @Field("demand_id")int demand_id, @Field("subject")String subject, @Field("category")String category, @Field("description")String description, @Field("image")String image);


    @FormUrlEncoded
    @POST("getmove")
    Call<GetMoveResponse> getMove(@Field("userid")int userId, @Field("id")int id);

    @FormUrlEncoded
    @POST("addvote")
    Call<BaseResponse> addVote(@Field("userid")int userId,@Field("id")int id,@Field("act")String act);

    @FormUrlEncoded
    @POST("getsubjects")
    Call<GetSubjectsResponse> getSubjects(@Field("userid")int userId,@Field("category")String category );

    @FormUrlEncoded
    @POST("deletemove")
    Call<BaseResponse> deleteMove(@Field("userid")int userId,@Field("id")int id);

    @FormUrlEncoded
    @POST("getmymember")
    Call<GetMyMemberResponse> getMyMember(@Field("userid")int userId,@Field("status")String status,@Field("type")String type);

    @FormUrlEncoded
    @POST("getchildoccupation")
    Call<GetChildOccupationResponse> getChildOccupation(@Field("userid")int userId, @Field("occupation")String occupation);

    @FormUrlEncoded
    @POST("getmember")
    Call<GetMemberResponse> getMember(@Field("userid")int userId,@Field("id")int id);

    @FormUrlEncoded
    @POST("getmembertype")
    Call<GetMemberTypeResponse> getMemberType(@Field("userid")int userId);


    @POST("setmemberattr")
    @FormUrlEncoded
    Call<BaseResponse> setMemberAttr(@Field("userid")int userId,
                                     @Field("id")Integer id,
                                     @Field("attr")String attr,
                                     @Field("content")String content);

    @POST("addmember")
    @FormUrlEncoded
    Call<BaseResponse> addMember(@Field("userid")int userId,@Field("orgid")Integer orgid,
                                     @Field("orgtitle")String orgtitle,@Field("title")String title,@Field("status")String status,@Field("description")String description);


    @POST("deletemember")
    @FormUrlEncoded
    Call<BaseResponse> deleteMember(@Field("userid")int userId,@Field("id")int id);

    @POST("applyorg")
    @FormUrlEncoded
    Call<BaseResponse> applyOrg(@Field("userid")int userId,
                              @Field("orgid")Integer orgid);


    @POST("addorg")
    @FormUrlEncoded
    Call<BaseResponse> addOrg(@Field("userid")int userId,
                                         @Field("title")String title);

    @POST("searchorg")
    @FormUrlEncoded
    Call<GetMyCompanyResponse> searchOrg(@Field("userid")int userId,
                                                 @Field("keyword")String keyword);

    @POST("getorg")
    @FormUrlEncoded
    Call<GetOrgResponse> getOrg(@Field("userid")int userId,
                                   @Field("id")Integer id);


    @POST("setorgattr")
    @FormUrlEncoded
    Call<BaseResponse> setOrgAttr(@Field("userid")int userId,
                                     @Field("id")Integer id,
                                     @Field("attr")String attr,
                                     @Field("content")String content);

    /**
     * 举报
     * @param userId
     * @param id
     * @param type
     * @return
     */
    @POST("talk")
    @FormUrlEncoded
    Call<BaseResponse> talk(@Field("userid") int userId, @Field("id")int id, @Field("type")String type,@Field("content")String content, @Field("data")String data);

    /**
     * 举报
     * @param userId
     * @param mid
     * @return
     */
    @POST("addappoint")
    @FormUrlEncoded
    Call<BaseResponse> addAppoint(@Field("userid") int userId, @Field("mid")int mid, @Field("message")String message, @Field("mode")String mode, @Field("contact")String contact, @Field("address")String address, @Field("date")String date, @Field("time")String time);

    /**
     * 举报
     * @param userId
     * @param id
     * @return
     */
    @POST("setappointattr")
    @FormUrlEncoded
    Call<BaseResponse> setAppointAttr(@Field("userid") int userId, @Field("id")String id, @Field("attr")String attr, @Field("content")String content);


    /**
     * 举报
     * @param userId
     * @param id
     * @param id
     * @param type
     * @return
     */
    @POST("getmessage")
    @FormUrlEncoded
    Call<GetMessageResponse> getMessage(@Field("userid") int userId, @Field("id")int id , @Field("type")String type, @Field("bid")int bid );

    @FormUrlEncoded
    @POST("getmycontact")
    Call<GetMyContactResponse> getMyContacts(@Field("userid")int userId  , @Field("keyword")String keyword);


    @FormUrlEncoded
    @POST("getcontact")
    Call<GetContactResponse> getContact(@Field("userid")int userId, @Field("id")String id);

    @POST("setcontactattr")
    @FormUrlEncoded
    Call<BaseResponse> setContactAttr(@Field("userid")int userId,
                                       @Field("id")String id,
                                       @Field("attr")String attr,
                                       @Field("content")String content);

    @FormUrlEncoded
    @POST("getmyuser")
    Call<GetMyUserResponse> getMyUser(@Field("userid")int userId, @Field("keyword")String content);

    @FormUrlEncoded
    @POST("search")
    Call<GetSearchResponse> search(@Field("userid")int userId,
                                   @Field("type")String type,
                                   @Field("keyword")String keyword
                                   );


    @FormUrlEncoded
    @POST("getmyrelation")
    Call<GetMyRelationResponse> getMyRelation(@Field("userid")int userId,@Field("status")String status,@Field("num")Integer thenum);

    @FormUrlEncoded
    @POST("getmymark")
    Call<GetMyMarkResponse> getMyMark(@Field("userid")int userId,@Field("content")String content);

    @FormUrlEncoded
    @POST("getmytalk")
    Call<GetMyMessageResponse> getMyTalk(@Field("userid")int userId, @Field("status")String status,@Field("num")Integer thenum);

    @FormUrlEncoded
    @POST("getmyreference")
    Call<GetMyRelationResponse> getMyReference(@Field("userid")int userId,@Field("content")String content);

    @FormUrlEncoded
    @POST("getrelation")
    Call<GetRelationResponse> getRelation(@Field("userid")int userId, @Field("id")int relation_id, @Field("mid")int match_id);


    @FormUrlEncoded
    @POST("getpartner")
    Call<BaseResponse> getPartner(@Field("userid")int userId, @Field("id")int partner_id,@Field("attr")String attr);


    @POST("addrelation")
    @FormUrlEncoded
    Call<BaseResponse> addRelation(@Field("userid")int userId,
                                   @Field("relation_demand_id")Integer relation_demand_id,
                                   @Field("status")String status,
                                   @Field("demand_id")Integer demand_id,
                                   @Field("memo")String memo
                                   );

    @POST("setrelationattr")
    @FormUrlEncoded
    Call<BaseResponse> setRelationAttr(@Field("userid")int userId,
                                  @Field("id")Integer id,
                                       @Field("mid")Integer mid,
                                  @Field("attr")String attr,
                                  @Field("content")String content);


    @POST("confirmrelation")
    @FormUrlEncoded
    Call<BaseResponse> confirmRelation(@Field("userid")int userId,
                                       @Field("id")Integer id,
                                       @Field("status")String status,
                                       @Field("demand_id")Integer demand_id,
                                       @Field("memo")String memo
                                       );

    @POST("refuserelation")
    @FormUrlEncoded
    Call<BaseResponse> refuseRelation(@Field("userid")int userId,
                                       @Field("id")Integer id
    );

    @POST("getmycomment")
    @FormUrlEncoded
    Call<GetMyCommentResponse> getMyComment(@Field("userid")int userId, @Field("partner_id")int partner_id );

    @FormUrlEncoded
    @POST("getcomment")
    Call<GetCommentResponse> getComment(@Field("userid")int userId, @Field("id")int comment_id);

    @POST("addcomment")
    @FormUrlEncoded
    Call<BaseResponse> addComment(@Field("userid")int userId,@Field("partner_id")Integer partner_id,@Field("rate")Integer rate,@Field("description")String description,@Field("charge")Float charge,
                                  @Field("need_permit")Integer need_permit);

    @POST("setcommentattr")
    @FormUrlEncoded
    Call<BaseResponse> setCommentAttr(@Field("userid")int userId,
                                        @Field("id")Integer comment_id,
                                        @Field("attr")String attr,
                                        @Field("content")String content);


    @POST("addinquiry")
    @FormUrlEncoded
    Call<BaseResponse> addInquiry(@Field("userid")int userId,@Field("partner_id")Integer partner_id,@Field("content")String content);

    @POST("setinquiryattr")
    @FormUrlEncoded
    Call<BaseResponse> setInquiryAttr(@Field("userid")int userId,
                                      @Field("id")Integer inquiry_id,
                                      @Field("attr")String attr,
                                      @Field("content")String content);

    @POST("getmyinquiry")
    @FormUrlEncoded
    Call<GetMyInquiryResponse> getMyInquiry(@Field("userid")int userId,@Field("id")int partner_id);

    @FormUrlEncoded
    @POST("getorder")
    Call<BaseResponse> getOrder(@Field("userid")int userId,
                                        @Field("fee")String fee,@Field("service")String service);

    @FormUrlEncoded
    @POST("withdraw")
    Call<BaseResponse> withdraw(@Field("userid")int userId,
                                @Field("amount")String amount,@Field("channel")String channel,@Field("source")String source);

    @FormUrlEncoded
    @POST("getcharge")
    Call<GetChargeResponse> getCharge(@Field("userid")int userId, @Field("service")String service, @Field("amount")String amount,@Field("from")String from);

    @FormUrlEncoded
    @POST("authorize")
    Call<BaseResponse> authorize(@Field("userid")int userId, @Field("service")String service, @Field("trade")String trade, @Field("from")String from);


    @POST("setconcensusattr")
    @FormUrlEncoded
    Call<BaseResponse> setConcensusAttr(@Field("userid")int userId,
                                       @Field("id")Integer concensus_id,
                                       @Field("attr")String attr,
                                       @Field("content")String content);

    @POST("addconcensus")
    @FormUrlEncoded
    Call<BaseResponse> addConcensus(@Field("userid")int userId,
                                   @Field("id")Integer relation_id);
    @FormUrlEncoded
    @POST("getconcensus")
    Call<GetConcensusResponse> getConcensus(@Field("userid")int userId, @Field("id")int concensus_id);

    @FormUrlEncoded
    @POST("actconcensus")
    Call<BaseResponse> actConcensus(@Field("userid")int userId, @Field("id")Integer concensus_id, @Field("act")String act);

    @FormUrlEncoded
    @POST("quit")
    Call<BaseResponse> quit(@Field("userid")int userId, @Field("pid")Integer pid, @Field("mid")Integer mid );

    @FormUrlEncoded
    @POST("cancelquit")
    Call<BaseResponse> cancelquit(@Field("userid")int userId, @Field("pid")Integer pid, @Field("mid")Integer mid );


    @FormUrlEncoded
    @POST("getbalance")
    Call<GetBalanceResponse> getBalance(@Field("userid")int userId);


    @FormUrlEncoded
    @POST("getperson")
    Call<GetPersonResponse> getPerson(@Field("userid")int userId,@Field("id")int id);

    @FormUrlEncoded
    @POST("getorganization")
    Call<GetOrganizationResponse> getOrganization(@Field("userid")int userId, @Field("id")int id);


    @FormUrlEncoded
    @POST("reporterror")
    Call<BaseResponse> reportError(@Field("userid")int userId, @Field("type")String errorType, @Field("type")String activity, @Field("title")String title, @Field("description")String description);

    String TYPE_DEFER="defer";
    String TYPE_APPLY="apply";
    String TYPE_ACCEPT = "accept";
    String TYPE_HISTORY = "history";

    /**
     * 获取子类型
     * @param parent_type
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST("getchildtype")
    Call<GetChildTypeResponse> getChildType(@Field("type") String parent_type, @Field("userid")int userId, @Field("from") String from );

    /**
     * 获取子类型
     * @param category
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST("getchildcategory")
    Call<GetChildCategoryResponse> getChildCategory(@Field("category") String category, @Field("userid")int userId, @Field("from") String from );


    /**
     * 获取偏好信息
     * @param userId
     * @return
     */
    @POST("gettransactionlist")
    @FormUrlEncoded
    Call<GetTransactionListResponse> getTransactionList(@Field("userid")int userId);

}
