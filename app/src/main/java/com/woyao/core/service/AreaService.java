package com.woyao.core.service;

import com.woyao.core.model.AreaResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by summerwind on 2016-05-20.
 */
public interface AreaService {

    /**
     * 获取子地区
     * @param parentId
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST("getchildlocation")
    Call<AreaResponse> getArea(@Field("location") String parentId, @Field("userid")int userId,@Field("suggest") String suggest,@Field("from") String from);
}


