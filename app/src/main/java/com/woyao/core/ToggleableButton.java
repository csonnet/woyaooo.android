package com.woyao.core;

import android.content.Context;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Created by summerwind on 2016-05-18.
 */
public class ToggleableButton extends RadioButton {
    public ToggleableButton(Context context) {
        super(context);
    }

    @Override
    public void toggle(){
        if(isChecked()){
            if(getParent() instanceof RadioGroup){
                ((RadioGroup)getParent()).clearCheck();
            }
        }else{
            setChecked(true);
        }
    }
}
