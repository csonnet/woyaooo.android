package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.woyao.core.util.Common;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class HowFilterAdapter extends RecyclerView.Adapter<HowFilterAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private ArrayList<Map<String,String>> thedata = new ArrayList<Map<String,String>>();
    private Changed onChanged;

    private String Selection = null ;

    public HowFilterAdapter(Context context, ArrayList<Map<String,String>> data, ArrayList<String> thechosen  ) {

//        this.mInflater = LayoutInflater.from(context);
//
//        for(Map<String,String> one: data){
//            if (  thechosen.contains(one.get("name") ) )  {
//                one.put("selected","true");
//            }else{
//                one.put("selected","false");
//            }
//            thedata.add(one );
//        }

    }


    public void AddNewOne( Map<String,String> onedata ) {

        thedata.add(0,onedata);
    }
    public interface Changed{
        void dataChanged();
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    public String getSelection() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                it.add((String) one.get("no"));
            }
        }

        return  Common.listToString(it );
    }

    public int getSelectionCount(){
        int count = 0;
        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                count += 1;
            }
        }
        return count;
    }

    @Override
    public HowFilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.prefer_toggle_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HowFilterAdapter.ViewHolder holder, final int position) {
        holder.thecheck.setOnCheckedChangeListener(null);

        holder.thetitle.setText( thedata.get(position).get("name") );

        if (thedata.get(position).get("selected") == "true") {
            holder.thecheck.setChecked( true);
        }else{
            holder.thecheck.setChecked( false);
        }
        holder.thecheck.setTag(position);

        holder.thecheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final int thepos = (int) buttonView.getTag();
                if( isChecked ) {
                    thedata.get(thepos).put("selected", "true");
                }else{
                    thedata.get(thepos).put("selected", "false");
                }
                onChanged.dataChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public CheckBox thecheck;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.id_text);
            thecheck = (CheckBox) view.findViewById(R.id.id_checkbox);
        }

    }
}
