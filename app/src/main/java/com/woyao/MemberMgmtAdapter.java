package com.woyao;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.MemberMgmtSummary;
import com.woyao.core.util.Common;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class MemberMgmtAdapter extends RecyclerView.Adapter<MemberMgmtAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private List<MemberMgmtSummary> thedata = new ArrayList<MemberMgmtSummary>();
    private Changed onChanged;

    Context thecontext;

    public MemberMgmtAdapter(Context context, List<MemberMgmtSummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);

        thedata= data;

    }


    public interface Changed{
        void view(MemberMgmtSummary ms);
        void edit(MemberMgmtSummary ms);
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.member_mgmt_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MemberMgmtSummary curData =  thedata.get(position);

        holder.theOrder.setText(  (position +1) +"") ;
        if ( curData.getEditable() ){
            holder.manageBtn.setVisibility(View.VISIBLE);
        }else{
            holder.manageBtn.setVisibility(View.GONE);
        }

        if (! curData.getVerified() ){
            holder.verifyBtn.setVisibility(View.VISIBLE);
        }else{
            holder.verifyBtn.setVisibility(View.GONE);
        }

        if (curData.getStatus().equals("apply")){
            holder.theTitle.setTextColor(  ContextCompat.getColor(thecontext, R.color.colorTextPrimary));
        }else if( curData.getStatus().equals("ever")){
            holder.theTitle.setTextColor(  ContextCompat.getColor(thecontext, R.color.colorSubTitleText));
        }


        holder.theTitle.setText( curData.getTitle() );

        holder.theSubtitle.setText( curData.getDescription() );

        holder.theImage.setImageResource(R.drawable.no_avartar);
        if (StringUtil.notNullOrEmpty(curData.getSnailview())) {
            Picasso.with(thecontext)
                    .load(curData.getSnailview())
                    .into(holder.theImage);
        }
        holder.theContent.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                MemberMgmtSummary curdata = thedata.get(holder.getAdapterPosition());
                onChanged.view(curdata  );
            }
        });

        holder.manageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MemberMgmtSummary curdata = thedata.get(holder.getAdapterPosition());
                onChanged.edit(curdata  );

            }
        });
        holder.verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MemberMgmtSummary curdata = thedata.get(holder.getAdapterPosition());
                Common.setMemberAttr(curdata.getId(),"verified", "1");
                view.setVisibility(View.GONE);
                Common.alert( thecontext,"组织成员已经确认");

            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theTitle;
        public TextView theSubtitle;
        public LinearLayout theContent;
        public CircleImageView theImage;
        public Button manageBtn;
        public Button verifyBtn;
        public TextView theOrder;

        public ViewHolder(View view) {
            super(view);

            theTitle = (TextView) view.findViewById(R.id.member_mgmt_title);
            theSubtitle = (TextView) view.findViewById(R.id.member_mgmt_subtext);
            theContent = (LinearLayout) view.findViewById(R.id.member_mgmt_content);
            theImage = (CircleImageView) view.findViewById(R.id.member_mgmt_image);
            manageBtn  = (Button)  view.findViewById(R.id.id_member_mgmt_action);
            verifyBtn  = (Button)  view.findViewById(R.id.id_member_mgmt_verify);
            theOrder  = (TextView)  view.findViewById(R.id.member_mgmt_order);
        }

    }




}
