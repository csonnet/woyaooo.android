package com.woyao;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.woyao.core.model.BaseResponse;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;
import com.woyao.core.util.StringUtil;

import java.io.IOException;

import retrofit2.Call;

public class ModifyPasswordActivity extends AppCompatActivity {

    private EditText oldPassword;
    private EditText newPassword;
    private Button confirm;
    private Button cancel;
    AsyncTask<Void,Void,Boolean> task;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setTitle( "修改密码");

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element

        initUI();
    }

    private void initUI(){
        oldPassword = (EditText)findViewById(R.id.old_password);
        newPassword = (EditText)findViewById(R.id.new_password);
        confirm = (Button)findViewById(R.id.confirm);
        cancel = (Button)findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (task != null) {
                    return;
                }
                final String oldPwd = oldPassword.getText().toString();
                final String newPwd = newPassword.getText().toString();
                if(StringUtil.isNullOrEmpty(oldPwd)){
                    Toast.makeText(ModifyPasswordActivity.this, "旧密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(StringUtil.isNullOrEmpty(newPwd)){
                    Toast.makeText(ModifyPasswordActivity.this, "新密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(oldPwd.equals(newPwd)){
                    Toast.makeText(ModifyPasswordActivity.this, "新旧密码不能相同", Toast.LENGTH_SHORT).show();
                    return;
                }
                task = new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        AccountService svc = ServiceFactory.get(AccountService.class);
                        Call<BaseResponse> responseCall = svc.setPassword(WoyaoooApplication.userId, oldPwd, newPwd);
                        try {
                            BaseResponse response = responseCall.execute().body();
                            return response.isSuccess();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }

                    @Override
                    protected void onPostExecute(final Boolean flag) {
                        task = null;
                        if (flag) {
                            Toast.makeText(ModifyPasswordActivity.this, "设置", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(ModifyPasswordActivity.this, "设置出错", Toast.LENGTH_LONG).show();
                        }
                    }
                };
                task.execute((Void) null);
            }
        });
    }
}
