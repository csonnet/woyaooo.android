package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.model.InquirySummary;
import com.woyao.core.util.Common;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class InquiryAdapter extends RecyclerView.Adapter<InquiryAdapter.ViewHolder>
{
    private LayoutInflater mInflater;

    private  List<InquirySummary> thedata = new ArrayList<InquirySummary>();

    private InquiryAdapter.Changed onChanged;

    Context thecontext;

    public InquiryAdapter(Context context, List<InquirySummary> data ) {
        thecontext = context;
        this.mInflater = LayoutInflater.from(context);
        thedata.addAll(data);

    }

    public void setChangedHandler(InquiryAdapter.Changed changed){
        this.onChanged = changed;
    }

    public interface Changed{
        void view(InquirySummary cs);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.inquiry_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {

        final InquirySummary curData =  thedata.get(position);
        holder.theOrder.setText( position +  1+"");

        holder.theTitle.setText(curData.getTitle());
        holder.theDescription.setText(curData.getDescription());

        if (!curData.getStatus().equals("new")){
            holder.acceptBtn.setVisibility(View.INVISIBLE);
            holder.refuseBtn.setVisibility(View.INVISIBLE);
        }

        String snail = curData.getSnailview();
        if (StringUtil.notNullOrEmpty(snail)) {

            Picasso.with(thecontext)
                    .load(snail)
                    .into(holder.snailview);
        }

        holder.snailview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InquirySummary curdata = thedata.get(holder.getAdapterPosition());

                onChanged.view(curdata  );

            }
        });

        holder.acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InquirySummary curdata = thedata.get(holder.getAdapterPosition());
                Common.setInquiryAttrFeedback( thecontext,  curData.getId(),"status","accept");

                holder.acceptBtn.setVisibility(View.INVISIBLE);
                holder.refuseBtn.setVisibility(View.INVISIBLE);
            }
        });

        holder.refuseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InquirySummary curdata = thedata.get(holder.getAdapterPosition());
                Common.setInquiryAttrFeedback( thecontext,  curData.getId(),"status","refuse");
                holder.acceptBtn.setVisibility(View.INVISIBLE);
                holder.refuseBtn.setVisibility(View.INVISIBLE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theOrder;
        public TextView theTitle;
        public TextView theDescription;
        public Button acceptBtn;
        public Button refuseBtn;
        public CircleImageView snailview;

        public ViewHolder(View view) {
            super(view);
            theOrder = (TextView) view.findViewById(R.id.inquiry_order);
            theTitle = (TextView) view.findViewById(R.id.inquiry_user_title);
            theDescription = (TextView) view.findViewById(R.id.inquiry_user_description);
            acceptBtn = (Button) view.findViewById(R.id.id_inquiry_accept);
            refuseBtn = (Button) view.findViewById(R.id.id_inquiry_refuse);
            snailview  = (CircleImageView) view.findViewById(R.id.inquiry_user_snailview);
        }

    }

}
