package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.woyao.core.model.GetMyCommentResponse;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;

public class MyCommentActivity extends AppCompatActivity {
    ArrayList<HashMap<String,String>> mapList;

    private CommentAdapter allAdapter;
    private RecyclerView alllist = null;
    LinearLayoutManager allLayoutManager = null;

    ProgressDialog progressDialog;

    private Integer GET_CODE = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycomments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout

        alllist = (RecyclerView)findViewById(R.id.comment_items);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));


        loadData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        finish();
        return true;
    }


    private AsyncTask<Void,Void,GetMyCommentResponse> task = null;
    private void loadData(){
        if(task != null)return;

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("正在获取数据······");
        progressDialog.show();

        task =new AsyncTask<Void, Void, GetMyCommentResponse>() {
            @Override
            protected GetMyCommentResponse doInBackground(Void... params) {
                AccountService svc = ServiceFactory.get(AccountService.class);
                Call<GetMyCommentResponse> responseCall = svc.getMyComment(WoyaoooApplication.userId,0);
                try {
                    GetMyCommentResponse response = responseCall.execute().body();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(final GetMyCommentResponse response) {
                progressDialog.dismiss();
                render(response);
                task = null;
            }
            @Override
            protected void onCancelled() {
                progressDialog.dismiss();
            }
        };
        task.execute((Void)null);
    }
    private void render(GetMyCommentResponse response){

//            this.setTitle("我的评论(" + response.getContent().size() + ")");
//
//            allAdapter = new CommentAdapter(this,  response.getContent() );
//
//            allAdapter.setChangedHandler(new CommentAdapter.Changed() {
//                @Override
//                public void view(Integer id) {
//                    getIt(id);
//                }
//
//
//            });
//
//            alllist.setAdapter(allAdapter);


    }

    private void getIt( final Integer curid ){

            Intent intent = new Intent();
            intent.setClass( MyCommentActivity.this, CommentActivity.class);
            intent.putExtra("id",  curid );
            startActivityForResult(intent,GET_CODE);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_CODE && resultCode == 666  ) {
            Boolean ischanged = data.getBooleanExtra("changed", true);
            if (ischanged) {
                loadData();
            }
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
