package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.woyao.core.util.Common;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class TreeAdapter extends RecyclerView.Adapter<TreeAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private ArrayList<Map<String,String>> thedata = new ArrayList<Map<String,String>>();
    private Changed onChanged;
    Context thecontext;
    private boolean loading = false;

    public TreeAdapter(Context context, ArrayList<Map<String,String>> data  ) {

        this.mInflater = LayoutInflater.from(context);
        thecontext = context;
        this.thedata = data;

    }


    public void AddNewOne( Map<String,String> onedata ) {
        for (Map<String,String> one : thedata){
            if ( one.get("name")  == onedata.get("name") ){
                one.put("selected","true");
                return;
            }
        }
        thedata.add(onedata);
    }

    public interface Changed{
        void itemSelected( Map<String,String> one );
        void itemGetChildren( Map<String,String> one);
    }

    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    public String getSelection() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                it.add((String) one.get("no"));
            }
        }

        return  Common.listToString(it );
    }

    public String getSelectionName() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                it.add((String) one.get("name"));
            }
        }

        return  Common.listToString(it );
    }

    public int getSelectionCount(){
        int count = 0;
        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                count += 1;
            }
        }
        return count;
    }


    @Override
    public TreeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.tree_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TreeAdapter.ViewHolder holder, final int position) {
        loading = true;
        holder.thecheck.setOnCheckedChangeListener(null);
        Boolean falseString = false;

        if (  thedata.get(position).get("selectable") != null && thedata.get(position).get("selectable").equals(falseString.toString())){
            holder.thecheck.setVisibility(View.GONE);
        }else{
            holder.thecheck.setVisibility(View.VISIBLE);
        }

        holder.thetitle.setText( thedata.get(position).get("name") );

        holder.thedescription.setText( thedata.get(position).get("description") );

        if (thedata.get(position).containsKey("image") && StringUtil.notNullOrEmpty(thedata.get(position).get("image")) ) {
            holder.theimage.setVisibility(View.VISIBLE);
            holder.theimage.setImageResource(R.drawable.no_avartar);
                Picasso.with(thecontext)
                        .load(thedata.get(position).get("image"))
                        .into(holder.theimage);

        }else{
            holder.theimage.setVisibility(View.GONE);
        }

        if (thedata.get(position).get("childs").equals("0")  || thedata.get(position).get("childs").equals("1")  ) {
            holder.thenext.setVisibility(View.GONE);
        }else{
            holder.thenext.setVisibility(View.VISIBLE);
        }


        if (thedata.get(position).get("description") != null && thedata.get(position).get("description").length()==0 ) {
            holder.thedescription.setVisibility(View.GONE);
        }

        if (thedata.get(position).get("selected") == "true") {
            holder.thecheck.setChecked( true);
        }else{
            holder.thecheck.setChecked( false);
        }

        holder.thecheck.setTag( position);
        holder.thecheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (loading) return;
                final int thepos = (int) buttonView.getTag();
                if( isChecked ) {
                    thedata.get(thepos).put("selected", "true");
                    HashMap<String,String> it = new HashMap<String, String>();

                    for(Map.Entry<String, String> entry:thedata.get(thepos).entrySet()){
                        it.put(entry.getKey(),entry.getValue());
                    }

                    onChanged.itemSelected(it );
                }else{
                    thedata.get(thepos).put("selected", "false");
                }

            }
        });

        final CheckBox cb = holder.thecheck;
        holder.thepanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( ! thedata.get(position).get("childs").equals("0")   && ! thedata.get(position).get("childs").equals("1")  ) {
                    onChanged.itemGetChildren(  thedata.get(position) );
                }
            }
        });

        loading = false;
    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public TextView thedescription;
        public CheckBox thecheck;
        public LinearLayout thepanel;
        public ImageView thenext;
        public ImageView theimage;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.id_text);
            thedescription = (TextView) view.findViewById(R.id.id_description);
            thecheck = (CheckBox) view.findViewById(R.id.id_checkbox);
            thenext = (ImageView) view.findViewById(R.id.id_next);
            theimage = (ImageView) view.findViewById(R.id.id_image);
            thepanel = (LinearLayout) view.findViewById(R.id.id_panel);
        }

    }
}
