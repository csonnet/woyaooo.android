package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.woyao.core.model.KeyValue;
import com.woyao.core.util.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/19 0019.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder>
{

    private LayoutInflater mInflater;

    private ArrayList<Map<String,String>> thedata = new ArrayList<Map<String,String>>();
    private Changed onChanged;

    private String Selection = null ;

    public FilterAdapter(Context context, List<KeyValue> data, List<KeyValue> chosen  ) {

        this.mInflater = LayoutInflater.from(context);
        ArrayList<String> thechosen = new ArrayList<String>();

        for(KeyValue one: chosen){
            thechosen.add(one.getNo());
        }

        for(KeyValue one: data){
            Map<String,String> cur = new HashMap<String, String>() ;
            cur.put("no",one.getNo());
            cur.put("name",one.getName());

            if (  thechosen.contains(one.getNo() ) )  {
                cur.put("selected","true");
            }else{
                cur.put("selected","false");
            }
            thedata.add(cur );
        }

    }

    public String getSelection() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
             it.add((String) one.get("no"));
        }
        Selection = Common.listToString(it );
        return Selection;
    }

    public interface Changed{
        void dataChanged();
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.prefer_toggle_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    /**
     * 数据的绑定显示
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.thetitle.setText( thedata.get(position).get("name") );

        if (thedata.get(position).get("selected") == "true") {
            holder.thecheck.setChecked( true);
        }else{
            holder.thecheck.setChecked( false);
        }

        holder.thecheck.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onChanged.dataChanged();
                Log.i("haiming", "touch" + thedata.get(holder.getAdapterPosition()).get("name"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public CheckBox thecheck;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.id_text);
            thecheck = (CheckBox) view.findViewById(R.id.id_checkbox);
        }

    }

}
