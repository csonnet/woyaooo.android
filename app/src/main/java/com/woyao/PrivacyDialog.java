package com.woyao;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Eugenia Gao
 * Describe:自定义隐私页的dialog
 * 1.标题和内容
 * 2.确定和取消按钮及点击事件
 *
 */
public class PrivacyDialog extends Dialog {
    Context context;
    String title;
    SpannableStringBuilder content;
    String confirmBtnText;
    SpannableStringBuilder cancelBtnText;
    private TextView btnNo;
    private TextView tvContent;
    private TextView tvTltle;
    private TextView tvWelcom;
    private Button btnYes;
    private ClickInterface clickInterface;

    /**
     * 点击事件的监听接口
     */
    public interface ClickInterface{
        void  doCofirm();
        void doCancel();
    }

    /**
     * 自定义控件需要重写构造函数,传入需要要的值
     */
    public PrivacyDialog(Context context, String title, SpannableStringBuilder content, String confirmBtnText, SpannableStringBuilder cancelBtnText){
        super(context, R.style.MyDialog);
        this.context=context;
        this.title=title;
        this.content=content;
        this.confirmBtnText=confirmBtnText;
        this.cancelBtnText=cancelBtnText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    /**
     * 初始化控件
     * 1.加载布局
     * 2.指定窗口的大小
     * 3.文本的设置
     */
    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.privacy_dialog,null);
        setContentView(view);

        btnNo = view.findViewById(R.id.privacy_no);
        tvTltle = view.findViewById(R.id.privacy_title);
        tvWelcom = view.findViewById(R.id.privacy_welcom);
        tvContent = view.findViewById(R.id.privacy_content);
        btnYes = view.findViewById(R.id.privacy_yes);

        tvTltle.setText(title);

        tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        tvContent.setText(content);

        btnYes.setText(confirmBtnText);
        btnNo.setText(cancelBtnText);

        btnNo.setOnClickListener(new ClickListener());
        btnYes.setOnClickListener(new ClickListener());

        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.8); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);
    }

    /**
     * 点击事件的设置-----接口回调
     * 设置点击事件,需要重写按钮的确定和取消要执行的操作
     */
    public void setClickListener(ClickInterface clickInterface){
        this.clickInterface=clickInterface;
    }

    /**
     * 系统的点击事件的自动监听
     */
    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.privacy_no:
                    if (clickInterface != null) {
                        clickInterface.doCancel();
                    }
                    break;
                case R.id.privacy_yes:
                    if (clickInterface != null) {
                        clickInterface.doCofirm();
                    }
                    break;
            }
        }
    }
}


