package com.woyao;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.woyao.core.model.GetMyMemberResponse;
import com.woyao.core.model.MemberSummary;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.ServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

import static com.woyao.WoyaoooApplication.userId;

public class MyMemberActivity extends AppCompatActivity {
    ArrayList<HashMap<String,String>> mapList;

    private MemberAdapter allAdapter;
    private RecyclerView alllist = null;
    LinearLayoutManager allLayoutManager = null;
    FrameLayout emptyArea ;

    private boolean changed = false;

    List<MemberSummary> myMembers = new ArrayList<MemberSummary>();

    ProgressDialog progressDialog;

    private Integer ADDMEMBER_CODE = 400;
    private Integer SETMEMBER_CODE = 500;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mymember);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        emptyArea = (FrameLayout) findViewById(R.id.empty_area);

        alllist = (RecyclerView)findViewById(R.id.member_items);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));



        Button addBtn = (Button) findViewById(R.id.mymember_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("from","mymember");
                intent.putExtra("id",0);

                intent.setClass(MyMemberActivity.this, ConfirmOrgActivity.class);

                startActivityForResult(intent, ADDMEMBER_CODE);
            }
        });


        loadMembers();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        finish();
        return true;
    }



    private void getIt( final Integer curid ){
        Intent intent = new Intent();
        intent.setClass(MyMemberActivity.this, MemberActivity.class);
        intent.putExtra("id", curid  );
        startActivityForResult(intent,SETMEMBER_CODE);

    }
    private AsyncTask<Void,Void, GetMyMemberResponse> task = null;
    private void loadMembers(){
        if(task != null)return;

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("正在获取数据······");
        progressDialog.show();

        task =new AsyncTask<Void, Void, GetMyMemberResponse>() {
            @Override
            protected GetMyMemberResponse doInBackground(Void... params) {
                AccountService svc = ServiceFactory.get(AccountService.class);
                Call<GetMyMemberResponse> responseCall = svc.getMyMember(userId,"","mgmt");
                try {
                    GetMyMemberResponse response = responseCall.execute().body();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(final GetMyMemberResponse response) {
                progressDialog.dismiss();
                renderMember(response);
                task = null;
            }
            @Override
            protected void onCancelled() {
                progressDialog.dismiss();
            }
        };
        task.execute((Void)null);
    }
    private void renderMember(GetMyMemberResponse response){

        myMembers = response.getContent();

        if (myMembers.size() > 0 ){
            emptyArea.setVisibility(View.GONE);
            alllist.setVisibility(View.VISIBLE);
        }else{
            alllist.setVisibility(View.GONE);
            emptyArea.setVisibility(View.VISIBLE);
        }

        allAdapter = new MemberAdapter(this,  response.getContent() );

        allAdapter.setChangedHandler(new MemberAdapter.Changed() {
            @Override
            public void view(MemberSummary ms) {
                getIt(ms.getId() );
            }


        });

        alllist.setAdapter(allAdapter);


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == ADDMEMBER_CODE  || requestCode ==SETMEMBER_CODE) {
            Boolean changed = data.getBooleanExtra("changed", true);
            if (changed) {
                loadMembers();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
