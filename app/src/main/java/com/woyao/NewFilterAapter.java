package com.woyao;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.woyao.core.model.KeyValue;
import com.woyao.core.model.KeyValueList;
import com.woyao.core.util.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class NewFilterAapter extends RecyclerView.Adapter<NewFilterAapter.ViewHolder>{

    private LayoutInflater mInflater;

    private ArrayList<Map<String,String>> thedata = new ArrayList<Map<String,String>>();
    private Changed onChanged;

    private String Selection = null ;
    private boolean loading = false;

    public NewFilterAapter(Context context, List<KeyValue> data, List<KeyValue> chosen  ) {

        this.mInflater = LayoutInflater.from(context);
        ArrayList<String> thechosen = new ArrayList<String>();

        for(KeyValue one: chosen){
            thechosen.add(one.getNo());
        }

        for(KeyValue one: data){
            if ( one.getNo().equals("")){
                continue;
            }
            Map<String,String> cur = new HashMap<String, String>() ;
            cur.put("no",one.getNo());
            cur.put("name",one.getName());

            if (  thechosen.contains(one.getNo() ) )  {
                cur.put("selected","true");
            }else{
                cur.put("selected","false");
            }
            thedata.add(cur );
        }

    }


    public void AddNewOne( Map<String,String> onedata ) {
        if ( onedata.get("selected").equals("true") ) {
            for (Map<String, String> one : thedata) {
                if (one.get("name").equals(onedata.get("name"))) {
                    one.put("selected", onedata.get("selected"));
                    return;
                }
            }

            thedata.add(onedata);
        }
    }
    public interface Changed{
        void dataChanged();
    }
    public void setChangedHandler(Changed changed){
        this.onChanged = changed;
    }

    public KeyValueList getSelectionKeyValue() {
        KeyValueList ret = new KeyValueList();
        ArrayList<KeyValue> it = new ArrayList<KeyValue>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                KeyValue kv = new KeyValue();
                kv.setNo((String) one.get("no"));
                kv.setName((String) one.get("name"));
                it.add(kv);
            }
        }
        ret.setContent( it);
        return ret;
    }


    public ArrayList<KeyValue> getSelectedKeyValue(){
        ArrayList<KeyValue> thelist = new ArrayList<KeyValue>();

        for (Map one: thedata ){
            if (one.get("selected") == "true" ){
                KeyValue kv = new KeyValue();
                kv.setName((String) one.get("name"));
                kv.setNo( (String) one.get("no"));
                thelist.add(kv);
            }

        }
        return thelist;
    }

    public String getSelection() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                it.add((String) one.get("no"));
            }
        }

        return  Common.listToString(it );
    }

    public String getSelectionName() {

        ArrayList<String> it = new ArrayList<String>();

        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                it.add((String) one.get("name"));
            }
        }

        return  Common.listToString(it );
    }

    public int getSelectionCount(){
        int count = 0;
        for(Map one: thedata){
            if (one.get("selected") == "true" ){
                count += 1;
            }
        }
        return count;
    }

    @Override
    public NewFilterAapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.prefer_toggle_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(NewFilterAapter.ViewHolder holder, final int position) {
        loading = true;
        holder.thecheck.setOnCheckedChangeListener(null);

        holder.thetitle.setText( thedata.get(position).get("name") );

        if (thedata.get(position).get("selected") == "true") {
            holder.thecheck.setChecked( true);
        }else{
            holder.thecheck.setChecked( false);
        }
        holder.thecheck.setTag(position);

        holder.thecheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (loading) return;
                final int thepos = (int) buttonView.getTag();
                if( isChecked ) {
                    thedata.get(thepos).put("selected", "true");
                }else{
                    thedata.get(thepos).put("selected", "false");
                }
                onChanged.dataChanged();

                NewFilterAapter.this.notifyDataSetChanged();
            }
        });

        final CheckBox cb = holder.thecheck;
        holder.thepanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    cb.setChecked( ! cb.isChecked() );
            }
        });
        loading = false;
    }

    @Override
    public int getItemCount() {
        return thedata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;
        public CheckBox thecheck;
        public LinearLayout thepanel;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.id_text);
            thecheck = (CheckBox) view.findViewById(R.id.id_checkbox);
            thepanel = (LinearLayout) view.findViewById(R.id.id_panel);
        }

    }
}
