package com.woyao;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Resources;

import org.afinal.simplecache.ACache;

/**
 * Created by summerwind on 2016-05-11.
 */
public class WoyaoooApplication extends Application {
    public static Resources resources;

    public static String key  ="";
    public static Integer userId =0 ;
    public static Integer member_id =0 ;
    public static Integer demand_id =0 ;
    public static String displayname="";
    public static String location="";
    public static String title="";
    public static String snailview="";
    public static String mobile="";
    public static Integer message_num =0;
    public static boolean hasLogin = false;
    @Override
    public void onCreate(){
        super.onCreate();
        resources = getResources();

        SharedPreferences shared = getSharedPreferences("login", MODE_PRIVATE);
        WoyaoooApplication.hasLogin = shared.getBoolean("logged", false);

        if (hasLogin ) {
            try {
                WoyaoooApplication.userId = shared.getInt("userId", 0);
                WoyaoooApplication.displayname = shared.getString("displayname", "");
                WoyaoooApplication.mobile = shared.getString("mobile", "");
                WoyaoooApplication.location = shared.getString("location", "");
                WoyaoooApplication.title = shared.getString("title", "");
                WoyaoooApplication.snailview = shared.getString("snailview", "");
                WoyaoooApplication.demand_id = shared.getInt("demand_id", 0);
                WoyaoooApplication.member_id = shared.getInt("member_id", 0);
                WoyaoooApplication.message_num = shared.getInt("message_num", 0);
            } catch (Exception e) {

            }
        }

    }

    public static void increase(Activity activity){
        ACache cache = ACache.get(activity);
        Integer count = (Integer)cache.getAsObject("NEXT_COUNT_"+userId);
        if(count == null){
            count = 1;
        }else{
            count = count+1;
        }
        cache.put("NEXT_COUNT_"+userId, count, ACache.TIME_DAY*365);
    }

    public static int getNextCount(Activity activity){
        ACache cache = ACache.get(activity);
        Integer count = (Integer)cache.getAsObject("NEXT_COUNT_"+userId);
        if(count == null){
            return 0;
        }
        return count;
    }
}
