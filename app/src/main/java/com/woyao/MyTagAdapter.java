package com.woyao;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.woyao.core.model.MyTag;
import com.woyao.core.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/20 0020.
 */
public class MyTagAdapter extends RecyclerView.Adapter<MyTagAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private List<MyTag> thedata = new ArrayList<MyTag>();

    Context thecontext;
    Resources resources;


    public MyTagAdapter(Context context, Resources res, List<MyTag> data  ) {

        this.mInflater = LayoutInflater.from(context);
        thecontext = context;
        resources  = res;
        thedata = data;

    }




    @Override
    public MyTagAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= mInflater.inflate(R.layout.mytag_item,parent,false);
        //view.setBackgroundColor(Color.RED);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyTagAdapter.ViewHolder holder, final int position) {


        holder.thetitle.setText( thedata.get(position).getSubject() );

        if (thedata.get(position).getSelected()){
            holder.thetitle.setTextColor(resources.getColor(R.color.colorWhite));
            holder.thetitle.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark));
        }else{
            holder.thetitle.setTextColor(resources.getColor(R.color.colorTextPrimary));
            holder.thetitle.setBackgroundColor(resources.getColor(R.color.colorLight));
        }


        holder.thetitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyTag tag = thedata.get(position);
                tag.setSelected(!tag.getSelected());
//                thedata.set(position,tag);
                TextView theText = (TextView) view;
                if (tag.getSelected()) {
                    theText.setTextColor(resources.getColor(R.color.colorWhite));
                    theText.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark));
                }else{
                    theText.setTextColor(resources.getColor(R.color.colorTextPrimary));
                    theText.setBackgroundColor(resources.getColor(R.color.colorLight));
                }

            }
        });

    }
    @Override
    public int getItemCount() {
//        ArrayList<String> temp = new ArrayList<String>();
//        for ( MyTag tg : thedata){
//            if (tg.getSelected()) {
//                temp.add(tg.getId());
//            }
//        }
        return thedata.size();
    }

    public String getSelected() {
        ArrayList<String> temp = new ArrayList<String>();
        for ( MyTag tg : thedata){
            if (tg.getSelected()) {
                temp.add(tg.getId());
            }
        }
        return StringUtil.join(temp,",");
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView thetitle;

        public ViewHolder(View view) {
            super(view);
            thetitle = (TextView) view.findViewById(R.id.tag_title);

        }

    }
}
