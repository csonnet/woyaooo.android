package com.woyao;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.squareup.picasso.Picasso;
import com.woyao.core.FileUtil;
import com.woyao.core.model.GetMemberTypeResponse;
import com.woyao.core.model.GetOrgResponse;
import com.woyao.core.model.KeyValue;
import com.woyao.core.model.KeyValueList;
import com.woyao.core.model.MemberMgmtSummary;
import com.woyao.core.model.Org;
import com.woyao.core.service.AccountService;
import com.woyao.core.util.Common;
import com.woyao.core.util.ServiceFactory;
import com.woyao.core.util.StringUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.woyao.WoyaoooApplication.userId;

public class OrgActivity extends AppCompatActivity {

    private MemberMgmtAdapter allAdapter;
    private RecyclerView alllist = null;
    LinearLayoutManager allLayoutManager = null;

    private TextView type;
    private TextView buzz;
    private TextView title;
    private TextView description;
    private TextView address;
    private TextView website;
    private TextView member_summary;
    private EditText fee;
    private ImageView image;

    int typeChoice = 0 ;

    private List<KeyValue> types = new ArrayList<KeyValue>();


    private  Button completeBtn;

    private Button viewBtn;

    boolean changed = false;


    ProgressDialog progressDialog;
    Org org =   new Org();

    Integer id = 0;

    Integer TITLE_CODE =  66;
    Integer ADDRESS_CODE =  77;
    Integer WEBSITE_CODE =  88;
    Integer DESC_CODE =  55;
    Integer LOGO_CODE =  100;
    Integer BUZZ_CODE = 99;
    Integer MGMT_MEMBER_CODE = 111;


    private  boolean loading = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_org_edit);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout


        alllist = (RecyclerView)findViewById(R.id.member_items);
        allLayoutManager = new LinearLayoutManager(this);
        allLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        alllist.setLayoutManager(allLayoutManager);
        alllist.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        Intent intent = getIntent();

        id = intent.getIntExtra( "id",0);

        this.setTitle("完善组织信息");

        viewBtn = (Button)findViewById(R.id.orgedit_view) ;

        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("id", id);
                intent.setClass(OrgActivity.this, OrgViewActivity.class);
                startActivity(intent);
            }
        });

        type = (TextView)findViewById(R.id.business_type);
        buzz = (TextView)findViewById(R.id.id_org_business);
        title = (TextView)findViewById(R.id.orgedit_title);
        description = (TextView)findViewById(R.id.orgedit_desc);
        address = (TextView)findViewById(R.id.orgedit_address);
        website = (TextView)findViewById(R.id.orgedit_website);
        member_summary = (TextView)findViewById(R.id.id_member_summary);
        fee = (EditText) findViewById(R.id.id_org_fee);
        image = (ImageView) findViewById(R.id.orgedit_image);



        completeBtn = ( Button) findViewById(R.id.orgedit_finish);


        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            if( org.getBusiness().size() ==0 ) {

                Dialog alertDialog = new AlertDialog.Builder(OrgActivity.this).
                        setTitle("信息").
                        setMessage("请选择所在行业").
                        setIcon(R.drawable.ic_launcher).
                        setPositiveButton("确定", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               completeBusiness();
                            }
                        }).
                        create();
                alertDialog.show();

                return;
            }
            Intent intent = new Intent();
            intent.putExtra("changed", changed );
            setResult(666, intent);
            finish();
            }
        });

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmMemberType();
            }
        });


        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("type","orgtitle");
                intent.putExtra("content", org.getTitle());

                intent.setClass(OrgActivity.this,  InputActivity.class);
                startActivityForResult(intent,TITLE_CODE);
            }
        });

        buzz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeBusiness();
            }
        });

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("type","orgdesc");
                intent.putExtra( "content", org.getDescription() );
                intent.setClass(OrgActivity.this,  InputActivity.class);
                startActivityForResult(intent,DESC_CODE);
            }
        });

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("type","orgaddress");
                intent.putExtra("content", org.getAddress());

                intent.setClass(OrgActivity.this,  InputActivity.class);
                startActivityForResult(intent,ADDRESS_CODE);
            }
        });

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("type","orgwebsite");
                intent.putExtra("content", org.getWebsite());

                intent.setClass(OrgActivity.this,  InputActivity.class);
                startActivityForResult(intent,WEBSITE_CODE);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open default camera
                checkPerm();
            }
        });

        fee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if ( loading ) return;
                try{
                    if (fee.getText().toString().trim().equals("")){
                        return;
                    }

                    Float itnow = Float.parseFloat(fee.getText().toString());

                    org.setFee( itnow);

                    fee.setTextColor(ContextCompat.getColor(OrgActivity.this,R.color.colorPrimaryDark));
                    Common.setOrgAttr(id,"fee",org.getFee() +"");
                }catch(Exception e) {
                    if (!fee.getText().toString().equals("")) {
                        Common.alert(OrgActivity.this, "请输入合理的数字");
                    }
                    fee.requestFocus();
                    fee.selectAll();

                }

            }
        });


        loadData();
    }


    public  void completeBusiness() {
        Intent intent = new Intent();
        intent.putExtra("from", "org");
        KeyValueList kvl = new KeyValueList();
        kvl.setContent( org.getBusiness() );
        intent.putExtra("business", kvl  );
        intent.setClass(OrgActivity.this, FilterBusiness.class);
        startActivityForResult(intent, BUZZ_CODE);
    }

    private void confirmMemberType( ){


        AsyncTask<Void, Void, GetMemberTypeResponse> task =new AsyncTask<Void, Void, GetMemberTypeResponse>() {
            @Override
            protected GetMemberTypeResponse doInBackground(Void... params) {
                AccountService svc = ServiceFactory.get(AccountService.class);
                Call<GetMemberTypeResponse> responseCall = svc.getMemberType(WoyaoooApplication.userId);
                try {
                    GetMemberTypeResponse response = responseCall.execute().body();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(final GetMemberTypeResponse response) {

                types = response.getContent();

                ArrayList<String> list = new ArrayList<String>();
                for (int i = 0; i < types.size(); i++) {
                    KeyValue ds = types.get(i);
                    list.add(ds.getName());
                }


                String[] items =  (String[])list.toArray(new String[0]);

                AlertDialog.Builder singleChoiceDialog =
                        new AlertDialog.Builder(OrgActivity.this);
                singleChoiceDialog.setTitle("选择组织类型");
                // 第二个参数是默认选项，此处设置为0
                singleChoiceDialog.setSingleChoiceItems(items, typeChoice,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                typeChoice = which;
                            }
                        });
                singleChoiceDialog.setPositiveButton("确 定",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (typeChoice != -1) {
                                    org.setType( types.get(typeChoice).getNo());
                                    type.setText( types.get(typeChoice).getName());
                                    Common.setOrgAttr(id,"type", org.getType());
                                }
                            }
                        });
                singleChoiceDialog.setNegativeButton("取 消",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                singleChoiceDialog.show();
            }
            @Override
            protected void onCancelled() {

            }
        };
        task.execute((Void)null);

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == 520)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {

                //权限被授予
                choosePhoto();

            } else
            {
                // Permission Denied
                Toast.makeText(OrgActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void checkPerm() {
        /**1.在AndroidManifest文件中添加需要的权限。
         *
         * 2.检查权限
         *这里涉及到一个API，ContextCompat.checkSelfPermission，
         * 主要用于检测某个权限是否已经被授予，方法返回值为PackageManager.PERMISSION_DENIED
         * 或者PackageManager.PERMISSION_GRANTED。当返回DENIED就需要进行申请授权了。
         * */
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){ //权限没有被授予

            /**3.申请授权
             * @param
             *  @param activity The target activity.（Activity|Fragment、）
             * @param permissions The requested permissions.（权限字符串数组）
             * @param requestCode Application specific request code to match with a result（int型申请码）
             *    reported to {@link OnRequestPermissionsResultCallback#onRequestPermissionsResult(
             *    int, String[], int[])}.
             * */
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    520);


        }else{//权限被授予
            choosePhoto();

            //直接操作
        }

    }

    void choosePhoto(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/");
        // start the image capture Intent
        startActivityForResult(intent, LOGO_CODE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        intent.putExtra("changed", changed );
        setResult(666, intent);
        finish();
        return true;
    }


    private void loadData(){

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("正在获取数据······");
        progressDialog.show();

        AsyncTask<Void, Void, GetOrgResponse> needTask = new AsyncTask<Void, Void, GetOrgResponse>() {
            @Override
            protected GetOrgResponse doInBackground(Void... params) {
                AccountService svc = ServiceFactory.get(AccountService.class);
                Call<GetOrgResponse> responseCall= svc.getOrg(userId,id );
                try {
                    GetOrgResponse response = responseCall.execute().body();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(final GetOrgResponse response) {
                if(response != null &&response.getContent() !=null){
                    org = response.getContent();
                    
                    renderIt();
                }
                progressDialog.dismiss();
            }
        };
        needTask.execute((Void)null);
    }

    private void renderIt(){

        if (org.getVerified() == 0 ){
            this.setTitle( org.getTitle() +  "(未认证)");
        }else{
            this.setTitle(org.getTitle() +  "(已认证)");
        }


        if (StringUtil.notNullOrEmpty(org.getLogo())) {
            Picasso.with(this)
                    .load(org.getLogo())
                    .into(image);
        } else {
            image.setImageResource(R.drawable.no_avartar);
        }

        type.setText(org.getType_name());
        title.setText(org.getTitle());
        description.setText(org.getDescription());
        address.setText(org.getAddress());
        website.setText(org.getWebsite());

        if (org.getBusiness().size()>0){
            buzz.setText(Common.KeyValueToNames( org.getBusiness()));
        }



        member_summary.setText( org.getMembers_summary());

        if (org.getMembers().size() > 0 ) {

            allAdapter = new MemberMgmtAdapter(this, org.getMembers());
            allAdapter.setChangedHandler(new MemberMgmtAdapter.Changed() {
                @Override
                public void view(MemberMgmtSummary ms) {
                    Intent intent = new Intent();
                    intent.putExtra("id", ms.getUser_id());
                    intent.setClass(OrgActivity.this, PersonViewActivity.class);
                    startActivity(intent);
                }

                public void edit(MemberMgmtSummary ms) {
                    Intent intent = new Intent();
                    intent.putExtra("id", ms.getId());
                    intent.setClass(OrgActivity.this, MemberActivity.class);
                    startActivityForResult(intent, MGMT_MEMBER_CODE);
                }


            });

//        allAdapter.setChangedHandler(new MemberMgmtAdapter.Changed() {
//            @Override
//            public void view(MemberSummary ms) {
//                Intent intent = new Intent();
//                intent.putExtra("id", userId);
//                intent.setClass(AccountActivity.this, PersonViewActivity.class);
//                startActivity(intent);
//            }
//
//
//        });

            alllist.setAdapter(allAdapter);
        }else{
            alllist.setVisibility(View.GONE);
        }

    }

    private void uploadLogo( String picturePath){
        final String filename = FileUtil.getPicObjectKey();
        OSS oss = FileUtil.getOss(getApplicationContext());
        PutObjectRequest put = new PutObjectRequest(FileUtil.bucketName, filename, picturePath);

// 异步上传时可以设置进度回调
        put.setProgressCallback(new OSSProgressCallback<PutObjectRequest>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {

            }
        });

        OSSAsyncTask task = oss.asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                Common.showSnack(OrgActivity.this, address, "上传成功" );
                Common.setOrgAttr(id,"logo","aliyun"+filename);
                Common.setOrgAttr(id,"verified","2");
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常

                }
                Common.showSnack(OrgActivity.this, image, "上传失败" );
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == TITLE_CODE && resultCode == 666 ) {
            CharSequence con = data.getCharSequenceExtra("content");
            if (con == null) return;
            org.setTitle(con.toString() );
            title.setText(con);
            Common.setOrgAttr(id,"title", con.toString());
            changed = true;
        } else if (requestCode == MGMT_MEMBER_CODE  && resultCode == 666  ) {
            loadData();
        }else if  (requestCode == BUZZ_CODE && resultCode ==666){
            KeyValueList thelist = (KeyValueList ) data.getExtras().get("result");
            org.setBusiness(thelist.getContent());
            buzz.setText(Common.KeyValueToNames(org.getBusiness()));

            Common.setOrgAttr(id,"business", Common.KeyValueToNos(org.getBusiness()));
            changed = true;
        }else if(requestCode == ADDRESS_CODE && resultCode == 666 ) {
            CharSequence con = data.getCharSequenceExtra("content");
            if (con == null) return;
            org.setAddress(con.toString() );
            address.setText(con);
            Common.setOrgAttr(id,"address", con.toString());
            changed = true;
        }else if(requestCode == WEBSITE_CODE && resultCode == 666 ) {
            CharSequence con = data.getCharSequenceExtra("content");
            if (con == null) return;
            org.setWebsite(con.toString() );
            website.setText(con);
            Common.setOrgAttr(id,"website", con.toString());
            changed = true;
        }else if(requestCode == DESC_CODE && resultCode == 666 ) {
            CharSequence con = data.getCharSequenceExtra("content");
            if (con == null) return;
            org.setDescription( con.toString());
            changed = true;
            Common.setOrgAttr(id,"description", con.toString());
            description.setText( con.toString());
        }else if(requestCode == LOGO_CODE &&resultCode==RESULT_OK){
            try {
                Uri selectedImage = data.getData();
                Bitmap preview = null;
                try {
                    preview = FileUtil.decodeUri(selectedImage, OrgActivity.this);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                Cursor cursor = getContentResolver().query(
//                        selectedImage, filePathColumn, null, null, null);
//                if(cursor!=null) {
//                    cursor.moveToFirst();
//                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                    picturePath = cursor.getString(columnIndex);
//                    cursor.close();
//                }
//                else
//                {
//                    picturePath = selectedImage.getPath();
//                }
                String picturePath = Common.getPath(OrgActivity.this,selectedImage);
                uploadLogo( picturePath );
                image.setImageBitmap(preview);
                changed  = true;

             }catch(Exception e){
                 e.printStackTrace();
             }
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            intent.putExtra("changed", changed );
            setResult(666, intent);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


}
